/**
 * 
 */
package com.xingfei.blog.common.vo;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * 数据返回结果视图
 * @author: chenxingfei
 * @time: 2017/2/7  15:39
 * To change this template use File | Settings | File Templates.
 */
public class JsonResult<T> implements Serializable{
	
	private static final long serialVersionUID = -5256130172935647108L;

	private boolean isEncrypt; //是否加密
    
    private T data;//数据
    
    private String errorCode;//返回给调用者的错误码（如：100001）

    private String errorMsg; // 返回给调用者的错误信息描述，操作成功该字段为空(如：服务器异常)
    
	/**
	 * @return the isEncrypt
	 */
	public boolean isEncrypt() {
		return isEncrypt;
	}

	/**
	 * @param isEncrypt the isEncrypt to set
	 */
	public void setEncrypt(boolean isEncrypt) {
		this.isEncrypt = isEncrypt;
	}

	/**
	 * @return the data
	 */
	public T getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}

	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
}