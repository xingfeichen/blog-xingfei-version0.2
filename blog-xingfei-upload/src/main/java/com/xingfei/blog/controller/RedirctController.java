package com.xingfei.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: chenxingfei
 * @time: 2017/4/9  9:34
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class RedirctController {

    @RequestMapping("/index")
    public String toUploadPage(){
        return "index";
    }
}
