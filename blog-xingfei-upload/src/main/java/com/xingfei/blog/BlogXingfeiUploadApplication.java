package com.xingfei.blog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class BlogXingfeiUploadApplication {
	/**
	 * 日志
	 */
	public static final Logger LOGGER = LoggerFactory.getLogger(BlogXingfeiUploadApplication.class);

	/**
	 * 文件上传项目启动main
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(BlogXingfeiUploadApplication.class, args);
		LOGGER.info("====================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>file-web启动成功<<<<<<<<<<<<<<<<<<<<<<<<<<<<=================");
	}
}
