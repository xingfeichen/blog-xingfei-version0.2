package com.xingfei.blog.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 评论vo
 */
public class UserCommentVO implements Serializable {
    /**
     * 评论id
     */
    private Long id;
    /**
     * 收到评论的用户id
     */
    private Long userId;
    /**
     * 事项id，如（评论文章，则保存文章id，如恢复评论，则保存被评论记录id）
     */
    private Long relativeId;
    /**
     * 评论类型（评论文章：1，评论回复：2）
     */
    private Integer type;
    /**
     * 评论者ID
     */
    private Long commitUserId;
    /**
     * 评论时间
     */
    private Date commitTime;
    /**
     * 评论时的IP地址
     */
    private String commitIp;
    /**
     * 评论内容
     */
    private String commitContent;

    /**
     * 封装评论用户数据
     */
    private UserInfoVO userInfoVO;

    /**
     * 封装被评论或被回复用户数据
     */
    private UserInfoVO userInfoVOReplay;

    /**
     * 评论回复列表
     */
    private List<UserCommentVO> commentReplays;

    public List<UserCommentVO> getCommentReplays() {
        return commentReplays;
    }

    public void setCommentReplays(List<UserCommentVO> commentReplays) {
        this.commentReplays = commentReplays;
    }

    public UserInfoVO getUserInfoVOReplay() {
        return userInfoVOReplay;
    }

    public void setUserInfoVOReplay(UserInfoVO userInfoVOReplay) {
        this.userInfoVOReplay = userInfoVOReplay;
    }

    public UserInfoVO getUserInfoVO() {
        return userInfoVO;
    }

    public void setUserInfoVO(UserInfoVO userInfoVO) {
        this.userInfoVO = userInfoVO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRelativeId() {
        return relativeId;
    }

    public void setRelativeId(Long relativeId) {
        this.relativeId = relativeId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getCommitUserId() {
        return commitUserId;
    }

    public void setCommitUserId(Long commitUserId) {
        this.commitUserId = commitUserId;
    }

    public Date getCommitTime() {
        return commitTime;
    }

    public void setCommitTime(Date commitTime) {
        this.commitTime = commitTime;
    }

    public String getCommitIp() {
        return commitIp;
    }

    public void setCommitIp(String commitIp) {
        this.commitIp = commitIp;
    }

    public String getCommitContent() {
        return commitContent;
    }

    public void setCommitContent(String commitContent) {
        this.commitContent = commitContent;
    }

    @Override
    public String toString() {
        return "UserCommentDTO{" +
                "id=" + id +
                ", userId=" + userId +
                ", relativeId=" + relativeId +
                ", type=" + type +
                ", commitUserId=" + commitUserId +
                ", commitTime=" + commitTime +
                ", commitIp='" + commitIp + '\'' +
                ", commitContent='" + commitContent + '\'' +
                '}';
    }
}