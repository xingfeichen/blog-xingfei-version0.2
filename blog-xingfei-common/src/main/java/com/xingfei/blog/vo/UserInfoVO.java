package com.xingfei.blog.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * 用户数据展示对象（可扩展字段）
 *
 * @author: chenxingfei
 * @time: 2017/1/22  11:40
 * To change this template use File | Settings | File Templates.
 */
public class UserInfoVO implements Serializable {
    //用户id
    private Long id;
    //用户权限
    private Long powerId;
    //用户名
    private String userName;
    //用户手机
    private String phone;
    //用户性别
    private String sex;
    //用户qq
    private String qqOpenId;
    //用户邮箱地址
    private String email;
    //地址
    private String address;
    //用户积分
    private Integer mark;
    //积分账户id
    private Long rankId;
    //最后登录ip
    private String lastLoginIp;
    //描述
    private String description;
    //生日
    private Date birthday;
    //最后更新博客时间
    private Date lastUpdateBlogTime;
    //头像路径
    private String avatarUrl;
    //注册时间
    private Date registerTime;
    //毕业学校
    private String graduateSchool;
    //注册时ip
    private String registerIp;
    //微博账号
    private String weiboUid;
    //血型
    private String bloodType;
    //用户个性签名
    private String says;
    //是否锁定账户
    private Byte isLock;
    //是否冻结
    private Byte isFreeze;
    /**
     * 用户id
     */
    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPowerId() {
        return powerId;
    }

    public void setPowerId(Long powerId) {
        this.powerId = powerId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getQqOpenId() {
        return qqOpenId;
    }

    public void setQqOpenId(String qqOpenId) {
        this.qqOpenId = qqOpenId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    public Long getRankId() {
        return rankId;
    }

    public void setRankId(Long rankId) {
        this.rankId = rankId;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getLastUpdateBlogTime() {
        return lastUpdateBlogTime;
    }

    public void setLastUpdateBlogTime(Date lastUpdateBlogTime) {
        this.lastUpdateBlogTime = lastUpdateBlogTime;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public String getGraduateSchool() {
        return graduateSchool;
    }

    public void setGraduateSchool(String graduateSchool) {
        this.graduateSchool = graduateSchool;
    }

    public String getRegisterIp() {
        return registerIp;
    }

    public void setRegisterIp(String registerIp) {
        this.registerIp = registerIp;
    }

    public String getWeiboUid() {
        return weiboUid;
    }

    public void setWeiboUid(String weiboUid) {
        this.weiboUid = weiboUid;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getSays() {
        return says;
    }

    public void setSays(String says) {
        this.says = says;
    }

    public Byte getIsLock() {
        return isLock;
    }

    public void setIsLock(Byte isLock) {
        this.isLock = isLock;
    }

    public Byte getIsFreeze() {
        return isFreeze;
    }

    public void setIsFreeze(Byte isFreeze) {
        this.isFreeze = isFreeze;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}