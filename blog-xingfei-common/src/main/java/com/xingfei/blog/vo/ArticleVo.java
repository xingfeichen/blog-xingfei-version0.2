package com.xingfei.blog.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/2/18.
 * 文章数据展示对象（可扩展字段）
 */
public class ArticleVo implements Serializable {
    /**
     * 文章唯一标识
     */
    private Long id;
    /**
     * 所属用户ID
     */
    private Long userId;
    /**
     * 文章名称
     */
    private String articleName;
    /**
     * 发布时间
     */
    private Date articleTime;
    /**
     * 发布IP
     */
    private String articleIp;
    /**
     * 查看人数
     */
    private Integer articleClick;
    /**
     * 所属分类
     */
    private Long categoryArticleId;
    /**
     * 栏目ID
     */
    private Long typeId;
    /**
     * 文章的模式:0为私有，1为公开，2为仅好友查看
     */
    private Long articleType;
    /**
     * 是否置顶:0为否，1为是
     */
    private byte isTop;
    /**
     * 是否置顶:0为否，1为是
     */
    private byte isSupported;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date modifyTime;
    /**
     * 评论数
     */
    private Integer commentCount = 0;
    /**
     * 查看数
     */
    private Integer viewCount = 0;
    /**
     * 收藏数
     */
    private Integer collectCount = 0;
    /**
     * 点赞数
     */
    private Integer parisedCount = 0;
    /**
     * 文章图片
     */
    private String imgUrl;
    /**
     * 文章关键字
     */
    private String keyWord;
    /**
     * 文章描述信息
     */
    private String description;
    /**
     * 封装用户数据
     */
    private UserVO userVO;

    /**
     * 当前登录用户是否已经关注文章发布者
     */
    private Boolean attention;

    /**
     * 当前登录用户是否收藏当前文章
     */
    private Boolean collected = false;
    /**
     * 当前登录用户是否对当前文章点过赞
     */
    private Boolean praised = false;
    /**
     * 赞id
     */
    private Long parisedId;

    /**
     * 用户评论列表
     */
    private List<UserCommentVO> userCommentVOS = new ArrayList<>(16);

    public List<UserCommentVO> getUserCommentVOS() {
        return userCommentVOS;
    }

    public void setUserCommentVOS(List<UserCommentVO> userCommentVOS) {
        this.userCommentVOS = userCommentVOS;
    }

    public Integer getParisedCount() {
        return parisedCount;
    }

    public void setParisedCount(Integer parisedCount) {
        this.parisedCount = parisedCount;
    }

    public Boolean getCollected() {
        return collected;
    }

    public void setCollected(Boolean collected) {
        this.collected = collected;
    }

    public Boolean getPraised() {
        return praised;
    }

    public void setPraised(Boolean praised) {
        this.praised = praised;
    }

    public Long getParisedId() {
        return parisedId;
    }

    public void setParisedId(Long parisedId) {
        this.parisedId = parisedId;
    }

    public Boolean getAttention() {
        return attention;
    }

    public void setAttention(Boolean attention) {
        this.attention = attention;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Integer getCollectCount() {
        return collectCount;
    }

    public void setCollectCount(Integer collectCount) {
        this.collectCount = collectCount;
    }

    private ArticleContentVO articleContentVO;

    public ArticleContentVO getArticleContentVO() {
        return articleContentVO;
    }

    public void setArticleContentVO(ArticleContentVO articleContentVO) {
        this.articleContentVO = articleContentVO;
    }

    public UserVO getUserVO() {
        return userVO;
    }

    public void setUserVO(UserVO userVO) {
        this.userVO = userVO;
    }

    public Long getId() {
        return id;
    }

    public Long getUserId() {
        return userId;
    }

    public String getArticleName() {
        return articleName;
    }

    public Date getArticleTime() {
        return articleTime;
    }

    public String getArticleIp() {
        return articleIp;
    }

    public Integer getArticleClick() {
        return articleClick;
    }

    public Long getCategoryArticleId() {
        return categoryArticleId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public Long getArticleType() {
        return articleType;
    }

    public byte getIsTop() {
        return isTop;
    }

    public byte getIsSupported() {
        return isSupported;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public void setArticleTime(Date articleTime) {
        this.articleTime = articleTime;
    }

    public void setArticleIp(String articleIp) {
        this.articleIp = articleIp;
    }

    public void setArticleClick(Integer articleClick) {
        this.articleClick = articleClick;
    }

    public void setCategoryArticleId(Long categoryArticleId) {
        this.categoryArticleId = categoryArticleId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public void setArticleType(Long articleType) {
        this.articleType = articleType;
    }

    public void setIsTop(byte isTop) {
        this.isTop = isTop;
    }

    public void setIsSupported(byte isSupported) {
        this.isSupported = isSupported;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
