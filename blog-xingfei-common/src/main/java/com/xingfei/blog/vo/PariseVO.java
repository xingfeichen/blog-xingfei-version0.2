package com.xingfei.blog.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * 赞vo
 * Created by chenxingfei on 2017/6/15.
 */
public class PariseVO implements Serializable {

    private Integer id;
    private Integer userId;
    private Date createTime;
    private Integer relativeId;
    private Integer type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getRelativeId() {
        return relativeId;
    }

    public void setRelativeId(Integer relativeId) {
        this.relativeId = relativeId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
