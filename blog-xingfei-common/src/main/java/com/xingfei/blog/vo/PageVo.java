package com.xingfei.blog.vo;

/**
 * Created with IntelliJ IDEA.
 * 分页vo
 * @author: chenxingfei
 * @time: 2017/1/19  11:57
 * To change this template use File | Settings | File Templates.
 */
public class PageVo<T> {

    /**
     * 起始页
     */
    private int start;

    /**
     * 页面大小
     */
    private int limit;

    /**
     * 实体
     */
    private T domain;

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public T getDomain() {
        return domain;
    }

    public void setDomain(T domain) {
        this.domain = domain;
    }

}
