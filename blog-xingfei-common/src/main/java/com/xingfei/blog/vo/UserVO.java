package com.xingfei.blog.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * 用户数据展示对象（可扩展字段）
 *
 * @author: chenxingfei
 * @time: 2017/1/19  11:11
 * To change this template use File | Settings | File Templates.
 */
public class UserVO implements Serializable {
    //用户id
    private Long id;
    //用户手机号
    private String phone;
    //用户名称
    private String username;
    //用户邮箱
    private String email;
    //用户密码
    private String password;
    //创建时间
    private Date createTime;
    //我关注的用户个数
    private Integer attentions;
    //关注我的人数
    private Integer beAttentions;
    /**
     * 用户昵称
     */
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    //封装用户详细信息
    private UserInfoVO userInfoVO = new UserInfoVO();
    public UserInfoVO getUserInfoVO() {
        return userInfoVO;
    }
    public void setUserInfoVO(UserInfoVO userInfoVO) {
        this.userInfoVO = userInfoVO;
    }
    public Integer getAttentions() {
        return attentions;
    }
    public void setAttentions(Integer attentions) {
        this.attentions = attentions;
    }

    public Integer getBeAttentions() {
        return beAttentions;
    }

    public void setBeAttentions(Integer beAttentions) {
        this.beAttentions = beAttentions;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "UserVO{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", createTime=" + createTime +
                ", attentions=" + attentions +
                ", beAttentions=" + beAttentions +
                ", userName='" + userName + '\'' +
                ", userInfoVO=" + userInfoVO +
                '}';
    }
}