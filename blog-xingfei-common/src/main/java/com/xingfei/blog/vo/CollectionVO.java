package com.xingfei.blog.vo;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * 收藏DO
 *
 * @author chenxingfei
 * @create 2017-07-02 14:51
 **/
public class CollectionVO implements Serializable {

    /**
     * 唯一主键
     */
    private Long id;
    /**
     * 收藏用户id
     */
    private Long collectorId;
    /**
     * 收藏类型
     */
    private String collectionType;
    /**
     * 对象id
     */
    private Long relativeId;
    /**
     * 创建时间
     */
    private Date creatTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCollectorId() {
        return collectorId;
    }

    public void setCollectorId(Long collectorId) {
        this.collectorId = collectorId;
    }

    public String getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(String collectionType) {
        this.collectionType = collectionType;
    }

    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    public Long getRelativeId() {
        return relativeId;
    }

    public void setRelativeId(Long relativeId) {
        this.relativeId = relativeId;
    }

    @Override
    public String toString() {
        return "CollectionDTO{" +
                "id=" + id +
                ", collectorId=" + collectorId +
                ", collectionType='" + collectionType + '\'' +
                ", relativeId=" + relativeId +
                ", creatTime=" + creatTime +
                '}';
    }
}
