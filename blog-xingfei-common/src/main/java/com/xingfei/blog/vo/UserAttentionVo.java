package com.xingfei.blog.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * 封装用户关注数据vo
 */
public class UserAttentionVo implements Serializable {
    //自增ID
    private Long id;
    //被关注用户ID
    private Long byAttentionId;
    //关注用户ID
    private Long attentionId;
    //创建时间
    private Date createTime;
    //被关注者读取状态
    private String isRead;
    //修改时间
    private Date modifyTime;
    //是否关注（true:关注，false:取消关注）
    private boolean attention;

    public boolean isAttention() {
        return attention;
    }

    public void setAttention(boolean attention) {
        this.attention = attention;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getByAttentionId() {
        return byAttentionId;
    }

    public void setByAttentionId(Long byAttentionId) {
        this.byAttentionId = byAttentionId;
    }

    public Long getAttentionId() {
        return attentionId;
    }

    public void setAttentionId(Long attentionId) {
        this.attentionId = attentionId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public String toString() {
        return "UserAttentionDTO{" +
                "id=" + id +
                ", byAttentionId=" + byAttentionId +
                ", attentionId=" + attentionId +
                ", createTime=" + createTime +
                ", modifyTime=" + modifyTime +
                '}';
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }
}