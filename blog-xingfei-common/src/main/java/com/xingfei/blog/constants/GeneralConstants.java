package com.xingfei.blog.constants;

/**
 * Created with IntelliJ IDEA.
 * 一般常量
 * @author: chenxingfei
 * @time: 2017/1/22  17:25
 * To change this template use File | Settings | File Templates.
 */
public class GeneralConstants {

    /**
     * 对称解密key
     */
    public  static  final  String DES_DECODE_KEY = "BLOG-XINGFEI-==";

    /**
     * 博客对称解密key
     */
    public  static  final  String DES_DECODE_BOLG_PUBLISH_KEY = "JIANYIBLOG-ARTICLE-PUB===";
    /**
     * PBE加密解密密码
     */
    public  static  final  String PEB_DECODE_PASSWORD = "BLOG-XINGFEI-PEB";

    /**
     * 图片验证码存放在session中的key值
     */
    public static final String JIANYI_BLOG_IMGCODE_SESSION_KEY = "jianyiblog-imgcode";
    /**
     * 邮箱是否激活
     */
    public static final String EMAIL_ACTIVE_YES = "1";//是


    /**
     * 首页数据每次加载条数
     */
    public static final Integer PAGE_SIZE = 2;





}
