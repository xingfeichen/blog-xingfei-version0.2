package com.xingfei.blog.constants;

/**
 * Created with IntelliJ IDEA.
 * 返回错误码常量
 *
 * @author: chenxingfei
 * @time: 2017/1/23  14:55
 * To change this template use File | Settings | File Templates.
 */
public class ErrorConstants {

    /**
     * 验证码错误状态码
     */
    public static final String CHECK_CODE_ERROR = "000";

    /**
     * 邮箱错误状态码
     */
    public static final String EMAIL_ERROR = "001";
    /**
     * 用户名或密码错误状态码
     */
    public static final String LOGIN_ERROR = "002";
    /**
     * 手机号错误状态码
     */
    public static final String PHONE_ERROR = "003";
    /**
     * 服务器错误状态码
     */
    public static final String SERVER_ERROR = "500";
    /**
     * 服务器处理成功状态码
     */
    public static final String SERVER_SUCCESS = "200";
    /**
     * 返回成功状态码
     */
    public static final int RES_SUCCESS = 1;

    /**
     * 返回失败状态码，应用错误，包含runtime异常，网络，数据库异常
     */
    public static final int RES_RUNTIME = -2;
    /**
     * 返回失败状态码,业务错误，业务逻辑错误，包含参数错误
     */
    public static final int RES_SERVICE = -1;
    /**
     * 返回失败状态码,业务错误，业务逻辑错误，包含参数错误
     */
    public static final String RES_SERVICE_STRING = "-1";
    /**
     * 用户名重复状态码
     */
    public static final String RESULT_USERNAME_REPET = "E001";
    /**
     * 手机号重复状态码
     */
    public static final String RESULT_PHONE_REPET = "E002";
    /**
     * 手机验证码错误状态码
     */
    public static final String RESULT_PHONE_CODE_REPET = "E003";
    /**
     * 用户未登陆
     */
    public static final String RESULT_USER_NOT_LOGIN = "E004";


}
