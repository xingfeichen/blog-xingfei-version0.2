package com.xingfei.blog.constants;

/**
 * Created with IntelliJ IDEA.
 * 用于管理redis相关常量
 * @author: chenxingfei
 * @time: 2017/1/22  17:25
 * To change this template use File | Settings | File Templates.
 */
public class RedisConstants {
    /**
     * 用户token相关的key
     */
    public static final String USER_TOKEN_CODE_KEY = "user:token:userId:{0}";
    /**
     * 邮箱激活key
     */
    public static final String EMAIL_ACTIVE_CODE_KEY = "email:active:{0}";
    /**
     * 用户登录token前缀
     */
    public static final String PROFIX_USER = "user:";
    /**
     * 文章收藏数量
     */
    public static final String ARTICLE_COLLECT_COUNT = "article:collect:count:{0}";
    /**
     * 文章评论数量
     */
    public static final String ARTICLE_COMMENT_COUNT = "article:comment:count:{0}";
    /**
     * 文章查看论数量
     */
    public static final String ARTICLE_VIEW_COUNT = "article:view:count:{0}";
    /**
     * 文章赞数量
     */
    public static final String ARTICLE_PARISE_COUNT = "article:parise:count:{0}";
    /**
     * 用户是否收藏指定文章标识
     */
    public static final String ARTICLE_IS_COLLECTED_FOR_USER= "article:{0}:collect:user:{1}";
    /**
     * 用户是否赞过指定文章标识
     */
    public static final String ARTICLE_IS_PARISED_FOR_USER = "article:{0}:parise:user:{1}";
}
