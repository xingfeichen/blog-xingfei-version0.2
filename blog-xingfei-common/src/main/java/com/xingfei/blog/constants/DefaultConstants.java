package com.xingfei.blog.constants;

/**
 * 用于存放默认简易blog相关的默认数据
 * Created by xingfei on 2017/6/11.
 */
public class DefaultConstants {

    /**
     * 默认头像
     */
    public static final String DEFAULT_AVATAR = "files/avatar/default/default-avatar.png";

    /**
     * 默认文章图片
     */
    public static final String DEFAULT_ARTICLE = "files/avatar/default/default-article.png";


}
