package com.xingfei.blog.utils;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by yhang on 2017/5/16.
 */
public class HttpRequestUtil {

    public void sendGet(String remoteUrl) {

        try {
            URL realUrl = new URL(remoteUrl);
            // 打开和URL之间的连接
            HttpURLConnection http = (HttpURLConnection) realUrl
                    .openConnection();
            // 设置通用的请求属性
            http.setRequestMethod("GET");
            http.setRequestProperty("accept", "*/*");
            http.setRequestProperty("connection", "Keep-Alive");
            http.setRequestProperty("user-agent",
                    "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
            http.setConnectTimeout(10000);
            // 建立实际的连接
            http.connect();
            //File file = new File(result);
            //FileUtils.copyURLToFile(realUrl, file);
            System.out.println(http.getResponseMessage());
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
    }
}
