package com.xingfei.blog.utils;

import java.util.List;
import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by hang on 2017/2/11.
 * 阿里短信jar引入本地仓库
 * http://blog.csdn.net/tianjiliu121/article/details/52238379
 */
public class SMSUtil {

    private static Logger logger = LoggerFactory.getLogger(SMSUtil.class);

    //正式环境url
    private static String Production_Url_Http = "http://gw.api.taobao.com/router/rest";
    private static String Production_Url_Https = "https://eco.taobao.com/router/rest";
    //沙箱环境url
//    private static String Test_Url_Http = "http://gw.api.tbsandbox.com/router/rest";
//    private static String Test_Url_Https="https://gw.api.tbsandbox.com/router/rest";
    //申请的 key
    private static String APPKEY = "23631150";
    private static String SECRET = "8accfdedf065206edaa8bde43c55d3a3";
//    private static String APPKEY = "23631541";
//    private static String SECRET = "c2f910efb4d098007af2aecbe98ae8b1";
    private static TaobaoClient client = new DefaultTaobaoClient(Production_Url_Http, APPKEY, SECRET);

    /**
     * 短信及通知发送 <br/>
     * 短息模板ID:SMS_50120156 <br/>
     *
     * @param code 验证码
     * @param extend 用户id
     * @param recNum 用户手机号
     * @throws ApiException
     */
    public static boolean sendMsg(String code, String extend, String recNum) throws ApiException {

        AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
        boolean result = false;
        req.setExtend(extend);
        req.setSmsType("normal");
        req.setSmsFreeSignName("简易BLOG");
//        req.setSmsFreeSignName("简易博客");
        req.setSmsParamString("{\"code\":\"" + code + "\",\"product\":\"简易BLOG\"}");
        req.setRecNum(recNum);
//        req.setSmsTemplateCode("SMS_46245028");
        req.setSmsTemplateCode("SMS_50120156");
        AlibabaAliqinFcSmsNumSendResponse rsp = client.execute(req);
        if(rsp.getErrorCode()==null){
            result =  true;
        }
        logger.error(recNum+"短信发送失败："+rsp.getSubMsg());
        return result;
    }

    /**
     * 向多个用户发送通知短信
     * 通知模板ID:SMS_50115146
     * @param username
     * @param extend
     * @param recNums
     * @return
     * @throws ApiException
     */
    public static boolean sendNotification(String username, String extend, List<String> recNums) throws ApiException {
        AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
        req.setExtend(extend);
        req.setSmsType("normal");
        req.setSmsFreeSignName("简易BLOG");
        req.setSmsParamString("{\"username\":\"" + username + "\"}");
        String phone = "";
        boolean result = false;
        for (String recNum : recNums ) {
            phone += recNum + ",";
        }
        if(StringUtils.isNotBlank(phone)){
            phone = phone.substring(0,phone.length()-1);
            req.setRecNum(phone);
            req.setSmsTemplateCode("SMS_50115146");
            AlibabaAliqinFcSmsNumSendResponse rsp = client.execute(req);
            System.out.println(rsp.getBody());
            if("0".equals(rsp.getErrorCode())){
                result =  true;
            }
        }
        return result;

    }

    public static void main(String[] args) {
        try {
            SMSUtil.sendMsg("666666","0001","18613892254");
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }
}
