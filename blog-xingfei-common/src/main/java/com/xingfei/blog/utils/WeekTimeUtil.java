package com.xingfei.blog.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class WeekTimeUtil {

	     public static String gainCulWeekMondayTime(){
	    	 SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss"); //设置时间格式 
	    	 Date date=gainCulWeekMonday();
	    	 return sdf.format(date);
	     }
	
	     
	     public static String gainCulMonthFirstTime(){
	    	 SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss"); //设置时间格式 
	    	 Date date=gainCulMonthFirst();
	    	 return sdf.format(date);
	     }
	
	     //当前周的周一
	public static Date gainCulWeekMonday(){
			 Calendar cal = Calendar.getInstance(); 
			return gainWeekMonday(cal.getTime());
		 }
		
		 /**
		  * 当前月的第一天
		  * @return
		  */
		public static Date gainCulMonthFirst(){
			Calendar cal_1=Calendar.getInstance();
			//cal_1.add(Calendar.MONTH, -1);
			//设置为1号,当前日期既为本月第一天 
			cal_1.set(Calendar.DAY_OF_MONTH,1);
			return cal_1.getTime();
		}
		
		   
		   public static Date gainWeekMonday(Date time){ 
			   Calendar cal = Calendar.getInstance();  
			   cal.setTime(time);
			   
			   int dayWeek = cal.get(Calendar.DAY_OF_WEEK);//获得当前日期是一个星期的第几天  
			   if(1 == dayWeek) {  
			       cal.add(Calendar.DAY_OF_MONTH, -1);  
			    }
			   cal.setFirstDayOfWeek(Calendar.MONDAY);//设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一  
			   int day = cal.get(Calendar.DAY_OF_WEEK);//获得当前日期是一个星期的第几天  
			   cal.add(Calendar.DATE, cal.getFirstDayOfWeek()-day);//根据日历的规则，给当前日期减去星期几与一个星期第一天的差值   
			   return cal.getTime();
		   }
		
		   
//		   public static void main(String[] args){
//			   SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd000000"); //设置时间格式
//			   Calendar cal = Calendar.getInstance();
//			   cal.add(Calendar.DATE, 16);
//
//			   String weekMonday=sdf.format(gainCulWeekMonday());
//			   String weekMonday2=sdf.format(gainWeekMonday(cal.getTime()));
//			   String monthFirst=sdf.format(gainCulMonthFirst());
//
//			   System.out.println("当前周第一天:"+weekMonday);
//			   System.out.println("一周第一天:"+weekMonday2);
//			   System.out.println("当前月第一天:"+monthFirst);
//		   }
		   
}
