package com.xingfei.blog.utils;

import com.xingfei.blog.vo.UserVO;

/**
 * Created by yhang on 2016/12/6.
 */
public class UserThreadLocal {

    // 声明一个ThreadLocal
    private static ThreadLocal<UserVO> userThreadLocal = new ThreadLocal<UserVO>();

    // 把用户信息放到ThreadLocal
    public static void set(UserVO o) {
        userThreadLocal.set(o);
    }

    // 从ThreadLocal中获取用户信息
    public static UserVO get() {
        return userThreadLocal.get();
    }

}
