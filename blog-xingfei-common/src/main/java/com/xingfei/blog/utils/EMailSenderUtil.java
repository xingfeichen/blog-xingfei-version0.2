package com.xingfei.blog.utils;

import com.xingfei.blog.constants.RedisConstants;
import org.apache.commons.lang3.StringUtils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Properties;

public class EMailSenderUtil {

    /**
     * 以文本格式发送邮件
     *
     * @param mailInfo 待发送的邮件的信息
     */
    public boolean sendTextMail(MailSenderInfo mailInfo) {

        // 判断是否需要身份认证
        MyAuthenticator authenticator = null;
        Properties pro = mailInfo.getProperties();
        if (mailInfo.isValidate()) {
            // 如果需要身份认证，则创建一个密码验证器
            authenticator = new MyAuthenticator(mailInfo.getUserName(), mailInfo.getPassword());
        }
        // 根据邮件会话属性和密码验证器构造一个发送邮件的session
        Session sendMailSession = Session.getDefaultInstance(pro, authenticator);
        try {
            // 根据session创建一个邮件消息
            Message mailMessage = new MimeMessage(sendMailSession);

            //设置中文发件人
            String name = "";
            name = javax.mail.internet.MimeUtility.encodeText("系统管理员");

            // 创建邮件发送者地址
            Address from = new InternetAddress(mailInfo.getFromAddress());
            // 设置邮件消息的发送者

            mailMessage.setFrom(new InternetAddress(name + " <" + from + ">"));
            //mailMessage.setFrom(from);
            // 创建邮件的接收者地址，并设置到邮件消息中
            Address to = new InternetAddress(mailInfo.getToAddress());
            mailMessage.setRecipient(Message.RecipientType.TO, to);
            // 设置邮件消息的主题
            mailMessage.setSubject(mailInfo.getSubject());
            // 设置邮件消息发送的时间
            mailMessage.setSentDate(new Date());
            // 设置邮件消息的主要内容
            String mailContent = mailInfo.getContent();
            mailMessage.setText(mailContent);
            // 发送邮件
            Transport.send(mailMessage);
            return true;
        } catch (MessagingException ex) {
            ex.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return false;
    }

    /** */
    /**
     * 以HTML格式发送邮件
     *
     * @param mailInfo 待发送的邮件信息
     */
    public boolean sendHtmlMail(MailSenderInfo mailInfo) {
        // 判断是否需要身份认证
        MyAuthenticator authenticator = null;
        Properties pro = mailInfo.getProperties();
        //如果需要身份认证，则创建一个密码验证器
        if (mailInfo.isValidate()) {
            authenticator = new MyAuthenticator(mailInfo.getUserName(), mailInfo.getPassword());
        }
        // 根据邮件会话属性和密码验证器构造一个发送邮件的session
        Session sendMailSession = Session.getDefaultInstance(pro, authenticator);
        try {
            // 根据session创建一个邮件消息
            Message mailMessage = new MimeMessage(sendMailSession);
            // 创建邮件发送者地址
            Address from = null;
            if (mailInfo.getFromAddressShow() != null && !mailInfo.getFromAddressShow().equals("")) {
                try {
                    from = new InternetAddress(mailInfo.getFromAddress(), mailInfo.getFromAddressShow());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                from = new InternetAddress(mailInfo.getFromAddress());
            }
            // 设置邮件消息的发送者
            mailMessage.setFrom(from);
            // 创建邮件的接收者地址，并设置到邮件消息中
            Address to = new InternetAddress(mailInfo.getToAddress());
            // Message.RecipientType.TO属性表示接收者的类型为TO
            mailMessage.setRecipient(Message.RecipientType.TO, to);
            // 设置邮件消息的主题
            mailMessage.setSubject(mailInfo.getSubject());
            // 设置邮件消息发送的时间
            mailMessage.setSentDate(new Date());
            // MiniMultipart类是一个容器类，包含MimeBodyPart类型的对象
            Multipart mainPart = new MimeMultipart();
            // 创建一个包含HTML内容的MimeBodyPart
            BodyPart html = new MimeBodyPart();
            // 设置HTML内容
            html.setContent(mailInfo.getContent(), "text/html; charset=utf-8");
            mainPart.addBodyPart(html);
            // 将MiniMultipart对象设置为邮件内容
            mailMessage.setContent(mainPart);
            // 发送邮件
            Transport.send(mailMessage);
            return true;
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    /**
     * 向用户发送邮件
     *
     * @param userID    用户id，必须，用于生成用户token
     * @param userEmail 用户邮箱，必须
     * @param username  用户名，可选（有就传）
     * @return
     */
    public static boolean sendActiveEmail(Long userID, String userEmail, String username) {
        if (userID==null || StringUtils.isBlank(userEmail)) return false;
        //校验条件
        if (StringUtils.isBlank(username)) {
            username = userEmail;
        }
        MailSenderInfo mailInfo = new MailSenderInfo();
        String key = MessageFormat.format(RedisConstants.USER_TOKEN_CODE_KEY, userID);
        //缓存用户激活信息，缓存48小时
        RedisUtil.set(key,userID+":"+userEmail,60 * 60 * 48,1);
        String token = TokenCodeUtil.setTokenCode(userID.toString());
        mailInfo.setMailServerHost("smtp.jianyiblog.com");
        mailInfo.setMailServerPort("25");
        mailInfo.setValidate(true);
        mailInfo.setUserName("jianyiblog@jianyiblog.com");
        mailInfo.setPassword("XINGFEIjy123");// 您的邮箱密码
        mailInfo.setFromAddress("jianyiblog@jianyiblog.com");
        mailInfo.setToAddress(userEmail);
        // 主题
        mailInfo.setSubject("简易BLOG邮箱激活邮件");
        // 发送邮件
        StringBuilder builder = new StringBuilder();
        builder.append(username + "<br/>欢迎！请确认此邮件地址以激活您的账号。<br/>");
        builder.append("<font color='red'><a href='http://localhost:9999/jianyi-blog/active/emailActive?ak="
                + token + "' target='_blank'");
        builder.append(">立即激活</a></font><br/>");
        builder.append("或者点击下面链接:<br/>");
        builder.append("http://localhost:9999/jianyi-blog/jianyi-blog/active/emailActive?ak="
                + token + "<br/>");
        builder.append("<h5>请48小时内激活</h5><br/>");
        builder.append("这是一封自动发送的邮件；如果您并未要求但收到这封信件，您不需要进行任何操作。");
        String htmlContent = builder.toString();
        mailInfo.setContent(htmlContent);
        // 这个类主要来发送邮件
        EMailSenderUtil sms = new EMailSenderUtil();
//        sms.sendTextMail(mailInfo);// 发送文体格式
        return sms.sendHtmlMail(mailInfo);// 发送html格式
    }

    /**
     * 重置密码
     * @param userEmail
     * @param token
     * @return
     */
    public static boolean sendResetPasswordEmail(String userEmail,String token) {

        MailSenderInfo mailInfo = new MailSenderInfo();

        mailInfo.setMailServerHost("smtp.jianyiblog.com");
        mailInfo.setMailServerPort("25");
        mailInfo.setValidate(true);
        mailInfo.setUserName("jianyiblog@jianyiblog.com");
        mailInfo.setPassword("XINGFEIjy123");// 您的邮箱密码
        mailInfo.setFromAddress("jianyiblog@jianyiblog.com");
        mailInfo.setToAddress(userEmail);
        // 主题
        mailInfo.setSubject("简易BLOG密码重置邮件");
        // 发送邮件
        StringBuilder builder = new StringBuilder();
        builder.append(userEmail + "<br/>欢迎！请确认此邮件地址以重置您的账号。<br/>");
        builder.append("<font color='red'><a href='http://localhost:9999/jianyi-blog/login/resetEmailPassword?ak="
                + token + "' target='_blank'");
        builder.append(">立即确认</a></font><br/>");
        builder.append("或者点击下面链接:<br/><a href='http://localhost:9999/jianyi-blog/login/resetEmailPassword?ak="
                + token + "' target='_blank'");
        builder.append(">http://localhost:9999/jianyi-blog/login/resetEmailPassword?ak="
                + token + "</a><br/>");
        builder.append("<h5>请三十分钟内确认</h5><br/>");
        builder.append("这是一封自动发送的邮件；如果您并未要求但收到这封信件，您不需要进行任何操作。");
        String htmlContent = builder.toString();
        mailInfo.setContent(htmlContent);
        // 这个类主要来发送邮件
        EMailSenderUtil sms = new EMailSenderUtil();
        return sms.sendHtmlMail(mailInfo);// 发送html格式
    }

    /**
     * 个人中心修改邮箱
     */
    public static boolean sendEmailToUpdateEmail(String userEmail,String token) {

        MailSenderInfo mailInfo = new MailSenderInfo();

        mailInfo.setMailServerHost("smtp.jianyiblog.com");
        mailInfo.setMailServerPort("25");
        mailInfo.setValidate(true);
        mailInfo.setUserName("jianyiblog@jianyiblog.com");
        mailInfo.setPassword("XINGFEIjy123");// 您的邮箱密码
        mailInfo.setFromAddress("jianyiblog@jianyiblog.com");
        mailInfo.setToAddress(userEmail);
        // 主题
        mailInfo.setSubject("简易BLOG绑定邮箱邮件");
        // 发送邮件
        StringBuilder builder = new StringBuilder();
        builder.append(userEmail + "<br/>欢迎！请确认此邮件地址。<br/>");
        builder.append("<font color='red'><a href='http://127.0.0.1:9999/email/personalConfirm?ak="
                + token + "' target='_blank'");
        builder.append(">立即确认</a></font><br/>");
        builder.append("或者点击下面链接:<br/><a href='http://127.0.0.1:9999/email/personalConfirm?ak="
                + token + "' target='_blank'");
        builder.append(">http://127.0.0.1:9999/email/personalConfirm?ak="
                + token + "</a><br/>");
        builder.append("<h5>请三十分钟内确认</h5><br/>");
        builder.append("这是一封自动发送的邮件；如果您并未要求但收到这封信件，您不需要进行任何操作。");
        String htmlContent = builder.toString();
        mailInfo.setContent(htmlContent);
        // 这个类主要来发送邮件
        EMailSenderUtil sms = new EMailSenderUtil();
        return sms.sendHtmlMail(mailInfo);// 发送html格式
    }

//    public static void main(String[] args) {
//        sendActiveEmail(35l,"xingfeichen@163.com","陈兴飞");
//    }    public static void main(String[] args) {
//        sendActiveEmail(35l,"xingfeichen@163.com","陈兴飞");
//    }
}