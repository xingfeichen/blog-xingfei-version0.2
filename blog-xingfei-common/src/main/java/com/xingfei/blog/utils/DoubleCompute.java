package com.xingfei.blog.utils;

import java.math.BigDecimal;

/**
 * double 四则运算
 * Created by yhang on 2017/4/25.
 */
public class DoubleCompute {
    /**
     * double 减法  value1-value2
     * @return
     */
    public static double sub(Number value1, Number value2) {
        BigDecimal b1 = new BigDecimal(Double.toString(value1.doubleValue()));
        BigDecimal b2 = new BigDecimal(Double.toString(value2.doubleValue()));
        return b1.subtract(b2).doubleValue();
    }
    /**
     * double 加法
     * @return
     */
    public static double add(Number value1, Number value2) {
        BigDecimal b1 = new BigDecimal(Double.toString(value1.doubleValue()));
        BigDecimal b2 = new BigDecimal(Double.toString(value2.doubleValue()));
        return b1.add(b2).doubleValue();
    }
    /**
     * double 乘法
     * @return
     */
    public static double mul(Number value1, Number value2) {
        BigDecimal b1 = new BigDecimal(Double.toString(value1.doubleValue()));
        BigDecimal b2 = new BigDecimal(Double.toString(value2.doubleValue()));
        return b1.multiply(b2).doubleValue();
    }
}
