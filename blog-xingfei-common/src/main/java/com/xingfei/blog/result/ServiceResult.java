package com.xingfei.blog.result;

/**
 * Created with IntelliJ IDEA.
 * 用于dubbo服务返回结果集
 *
 * @author: chenxingfei
 * @time: 2017/1/22  15:00
 * To change this template use File | Settings | File Templates.
 */
public class ServiceResult<T> implements java.io.Serializable {
    /**
     * 操作结果
     */
    private int code;

    /**
     * 返回给前台的错误代码
     */
    @Deprecated
    private String resultCode;

    /**
     * 错误信息,如果code是出错，则错误具体提示存在此处
     */
    private String errMsg;

    /**
     * 返回的结果
     */
    private T result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }
}