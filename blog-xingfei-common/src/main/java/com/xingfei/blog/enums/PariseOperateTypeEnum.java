package com.xingfei.blog.enums;

/**
 * 赞操作类型
 * Created by xingfei on 2017/7/5.
 */
public enum PariseOperateTypeEnum {
    PARISE_CANCEL("取消"),PARISE_SURE("赞");

    private String info;

    PariseOperateTypeEnum(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public static void main(String[] args) {
        System.out.println(PariseOperateTypeEnum.PARISE_CANCEL.toString());
    }
}
