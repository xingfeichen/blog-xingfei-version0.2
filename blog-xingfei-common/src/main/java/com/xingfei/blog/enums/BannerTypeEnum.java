package com.xingfei.blog.enums;

/**
 * @Desc 轮播图类型
 * ClassName com.xingfei.blog.enums
 * @Author xingfei
 * @Date 2017/7/9 21:57
 */
public enum BannerTypeEnum {

    BANNER_FOR_MAIN("首页主轮播图"), BANNER_FOR_SECOND("首页小轮播图");

    private String typeInfo;

    BannerTypeEnum(String typeInfo) {
        this.typeInfo = typeInfo;
    }

    public String getTypeInfo() {
        return typeInfo;
    }

    public void setTypeInfo(String typeInfo) {
        this.typeInfo = typeInfo;
    }
}
