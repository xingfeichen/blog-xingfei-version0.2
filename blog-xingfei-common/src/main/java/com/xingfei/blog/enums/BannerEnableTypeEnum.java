package com.xingfei.blog.enums;

/**
 * @Desc 轮播图可用类型
 * ClassName com.xingfei.blog.enums
 * @Author xingfei
 * @Date 2017/7/9 22:03
 */
public enum BannerEnableTypeEnum {
    BANNER_DISABLE("轮播图禁用"), BANNER_ENABLE("轮播图可用");

    private String typeInfo;

    BannerEnableTypeEnum(String typeInfo) {
        this.typeInfo = typeInfo;
    }

    public String getTypeInfo() {
        return typeInfo;
    }

    public void setTypeInfo(String typeInfo) {
        this.typeInfo = typeInfo;
    }
}
