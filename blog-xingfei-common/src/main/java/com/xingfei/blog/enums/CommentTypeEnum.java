package com.xingfei.blog.enums;

/**
 * 文章评论类型
 * Created by chenxingfei on 2017/6/10.
 */
public enum CommentTypeEnum {
    COMMENT_ARTICLE("文章评论"), COMMENT_REPLAY("评论回复");

    private String typeInfo;

    CommentTypeEnum(String typeInfo) {
        this.typeInfo = typeInfo;
    }

    public String getTypeInfo() {
        return typeInfo;
    }

    public void setTypeInfo(String typeInfo) {
        this.typeInfo = typeInfo;
    }
}
