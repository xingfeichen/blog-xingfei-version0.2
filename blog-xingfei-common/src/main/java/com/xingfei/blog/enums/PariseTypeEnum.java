package com.xingfei.blog.enums;

/**
 * 赞类型
 * Created by xingfei on 2017/7/5.
 */
public enum PariseTypeEnum {
    PARISE_USER("赞用户"),PARISE_ARTICLE("赞文章");

    private String info;

    PariseTypeEnum(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
