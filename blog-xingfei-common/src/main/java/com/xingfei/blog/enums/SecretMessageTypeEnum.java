package com.xingfei.blog.enums;

/**
 * @Desc TODO
 * ClassName com.xingfei.blog.enums
 * @Author xingfei
 * @Date 2017/7/8 16:36
 */
public enum SecretMessageTypeEnum {

    SECRET_MESSAGE("发送私信");
    private String info;

    SecretMessageTypeEnum(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
