package com.xingfei.blog.enums;

/**
 * 收藏类型
 *
 * @author chenxingfei
 * @create 2017-07-02 14:53
 **/
public enum CollentionTypeEnum {
    ARTICLE("文章收藏"), COMMENT("评论收藏"), USER("用户收藏");

    private String collectionType;
    CollentionTypeEnum(String collectionType) {
        this.collectionType = collectionType;
    }

    public String getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(String collectionType) {
        this.collectionType = collectionType;
    }
}
