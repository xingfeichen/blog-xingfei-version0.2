package com.xingfei.blog.dto;

import java.io.Serializable;

/**
 * @Desc 轮播图DTO
 * ClassName com.xingfei.blog.dto
 * @Author xingfei
 * @Date 2017/7/9 20:27
 */
public class BannerDTO implements Serializable {

    /**
     * 图片id
     */
    private String id;

    /**
     * 图片标题
     */
    private String picTitle;
    /**
     * 图片名字
     */
    private String picName;
    /**
     * 图片路径
     */
    private String picPath;
    /**
     * 图片类型（1：主轮播图（首页大轮播图），2：小轮播图）
     */
    private String type;
    /**
     * 图片添加时间
     */
    private String createTime;
    /**
     * 图片链接地址
     */
    private String picLinkedUrl;
    /**
     * 图片备注
     */
    private String remark;
    /**
     * 是否有效
     */
    private Integer enable;

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public String getPicTitle() {
        return picTitle;
    }

    public void setPicTitle(String picTitle) {
        this.picTitle = picTitle;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicName() {
        return picName;
    }

    public void setPicName(String picName) {
        this.picName = picName;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getPicLinkedUrl() {
        return picLinkedUrl;
    }

    public void setPicLinkedUrl(String picLinkedUrl) {
        this.picLinkedUrl = picLinkedUrl;
    }

    @Override
    public String toString() {
        return "BannerDTO{" +
                "id='" + id + '\'' +
                ", picTitle='" + picTitle + '\'' +
                ", picName='" + picName + '\'' +
                ", picPath='" + picPath + '\'' +
                ", type='" + type + '\'' +
                ", createTime='" + createTime + '\'' +
                ", picLinkedUrl='" + picLinkedUrl + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
