package com.xingfei.blog.dto;

import javax.persistence.Transient;
import java.util.UUID;

public class AdminUserDTO {

    private String userId = UUID.randomUUID().toString();      //用户id
    private String username;    //用户名
    private String password;    //密码
    private String name;        //姓名
    private String rights;        //权限
    private String roleId;        //角色id
    private String lastLogin;    //最后登录时间
    private String ip;            //用户登录ip地址
    private String status;        //状态
    @Transient
    private AdminRoleDTO role;            //角色对象
    private String skin;        //皮肤
    private String number;
    private String email; //邮箱
    private String phone;//手机号

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRights() {
        return rights;
    }

    public void setRights(String rights) {
        this.rights = rights;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public AdminRoleDTO getRole() {
        return role;
    }

    public void setRole(AdminRoleDTO role) {
        this.role = role;
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
