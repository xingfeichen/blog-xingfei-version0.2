package com.xingfei.blog.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章内容表
 */
public class ArticleContentDTO implements Serializable {
    /**
     * 文章内容id
     */
    private Long id;
    /**
     * 文章id
     */
    private Long articleId;
    /**
     * 文章内从创建时间按
     */
    private Date createTime;
    /**
     * 文章内容最后修改时间
     */
    private Date modifyTime;
    /**
     * 文章内从
     */
    private String acticleContent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getActicleContent() {
        return acticleContent;
    }

    public void setActicleContent(String acticleContent) {
        this.acticleContent = acticleContent;
    }
}