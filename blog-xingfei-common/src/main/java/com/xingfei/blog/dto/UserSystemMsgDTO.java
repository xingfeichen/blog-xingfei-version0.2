package com.xingfei.blog.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by yhang on 2017/5/8.
 */
public class UserSystemMsgDTO implements Serializable {

    private static final long serialVersionUID = 8102467293959470267L;

    private Long id;

    private Long receiveId;

    private Long sysMsgId;

    private String systemContent;

    private String isRead;

    private Date readTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReceiveId() {
        return receiveId;
    }

    public void setReceiveId(Long receiveId) {
        this.receiveId = receiveId;
    }

    public Long getSysMsgId() {
        return sysMsgId;
    }

    public void setSysMsgId(Long sysMsgId) {
        this.sysMsgId = sysMsgId;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public Date getReadTime() {
        return readTime;
    }

    public void setReadTime(Date readTime) {
        this.readTime = readTime;
    }

    public String getSystemContent() {
        return systemContent;
    }

    public void setSystemContent(String systemContent) {
        this.systemContent = systemContent;
    }
}
