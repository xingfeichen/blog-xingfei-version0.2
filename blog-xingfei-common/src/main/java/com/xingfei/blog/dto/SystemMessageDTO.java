package com.xingfei.blog.dto;

import java.io.Serializable;
import java.util.Date;
public class SystemMessageDTO implements Serializable {
    private Long id;
    private Long receiveId;
    private Long groupId;
    private Byte sendDefault;
    private String systemTopic;
    private String systemContent;
    private Date sendTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReceiveId() {
        return receiveId;
    }

    public void setReceiveId(Long receiveId) {
        this.receiveId = receiveId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Byte getSendDefault() {
        return sendDefault;
    }

    public void setSendDefault(Byte sendDefault) {
        this.sendDefault = sendDefault;
    }

    public String getSystemTopic() {
        return systemTopic;
    }

    public void setSystemTopic(String systemTopic) {
        this.systemTopic = systemTopic;
    }

    public String getSystemContent() {
        return systemContent;
    }

    public void setSystemContent(String systemContent) {
        this.systemContent = systemContent;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }
}