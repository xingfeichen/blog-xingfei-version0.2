package com.xingfei.blog.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2017/2/18.
 * 文章数据展示对象（可扩展字段）
 */
public class ArticleDTO implements Serializable {
    //文章唯一标识
    private Long id;
    //所属用户ID
    private Long userId;
    //文章名称
    private String articleName;
    //发布时间
    private Date articleTime;
    //发布IP
    private String articleIp;
    //查看人数
    private Integer articleClick;
    //所属分类
    private Long categoryArticleId;
    //栏目ID
    private Long typeId;
    //文章的模式:0为私有，1为公开，2为仅好友查看
    private Long articleType;
    //是否置顶:0为否，1为是
    private byte isTop;
    //是否置顶:0为否，1为是
    private byte isSupported;
    //创建时间
    private Date createTime;
    //修改时间
    private Date modifyTime;

    /**
     * 文章描述信息
     */
    private String description;
    /**
     * 文章图片
     */
    private String imgUrl;
    /**
     * 文章关键字
     */
    private String keyWord;
    /**
     * 文章内容dto
     */
    private ArticleContentDTO articleContentDTO;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArticleContentDTO getArticleContentDTO() {
        return articleContentDTO;
    }

    public void setArticleContentDTO(ArticleContentDTO articleContentDTO) {
        this.articleContentDTO = articleContentDTO;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public Long getId() {
        return id;
    }

    public Long getUserId() {
        return userId;
    }

    public String getArticleName() {
        return articleName;
    }

    public Date getArticleTime() {
        return articleTime;
    }

    public String getArticleIp() {
        return articleIp;
    }

    public Integer getArticleClick() {
        return articleClick;
    }

    public Long getCategoryArticleId() {
        return categoryArticleId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public Long getArticleType() {
        return articleType;
    }

    public byte getIsTop() {
        return isTop;
    }

    public byte getIsSupported() {
        return isSupported;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public void setArticleTime(Date articleTime) {
        this.articleTime = articleTime;
    }

    public void setArticleIp(String articleIp) {
        this.articleIp = articleIp;
    }

    public void setArticleClick(Integer articleClick) {
        this.articleClick = articleClick;
    }

    public void setCategoryArticleId(Long categoryArticleId) {
        this.categoryArticleId = categoryArticleId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public void setArticleType(Long articleType) {
        this.articleType = articleType;
    }

    public void setIsTop(byte isTop) {
        this.isTop = isTop;
    }

    public void setIsSupported(byte isSupported) {
        this.isSupported = isSupported;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
