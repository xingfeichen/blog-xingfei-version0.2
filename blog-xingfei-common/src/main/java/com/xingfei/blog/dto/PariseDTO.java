package com.xingfei.blog.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 赞dto
 * Created by chenxingfei on 2017/6/15.
 */
public class PariseDTO implements Serializable {

    /**
     * 唯一标识
     */
    private Long id;
    /**
     * 当前用户id
     */
    private Long userId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 对象id，赞文章时，保存文章id，赞用户时候，保存用户id
     */
    private Long relativeId;
    /**
     * 点赞类型（1：赞文章，0：赞用户）
     */
    private Integer type;
    /**
     * 操作类型（1：点赞，0：取消点赞）
     */
    private Integer operatorType;

    public Integer getOperatorType() {
        return operatorType;
    }

    public void setOperatorType(Integer operatorType) {
        this.operatorType = operatorType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getRelativeId() {
        return relativeId;
    }

    public void setRelativeId(Long relativeId) {
        this.relativeId = relativeId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "PariseDTO{" +
                "id=" + id +
                ", userId=" + userId +
                ", createTime=" + createTime +
                ", relativeId=" + relativeId +
                ", type=" + type +
                '}';
    }
}
