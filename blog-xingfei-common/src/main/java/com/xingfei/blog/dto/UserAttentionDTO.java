package com.xingfei.blog.dto;

import java.io.Serializable;
import java.util.Date;

public class UserAttentionDTO implements Serializable {
    //自增ID
    private Long id;
    //被关注用户ID
    private Long byAttentionId;
    //关注用户ID
    private Long attentionId;
    //被关注者读取zhuangt
    private String isRead;
    //创建时间
    private Date createTime;
    //修改时间
    private Date modifyTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getByAttentionId() {
        return byAttentionId;
    }

    public void setByAttentionId(Long byAttentionId) {
        this.byAttentionId = byAttentionId;
    }

    public Long getAttentionId() {
        return attentionId;
    }

    public void setAttentionId(Long attentionId) {
        this.attentionId = attentionId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public String toString() {
        return "UserAttentionDTO{" +
                "id=" + id +
                ", byAttentionId=" + byAttentionId +
                ", attentionId=" + attentionId +
                ", createTime=" + createTime +
                ", modifyTime=" + modifyTime +
                '}';
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }
}