package com.xingfei.blog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@EnableCaching
@ImportResource(locations = {"classpath:dubbo-comsumer.xml"})
@ComponentScan(basePackages = {"com.xingfei.blog","com.baidu"})
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class BlogXingfeiWebApplication {

	/**
	 * 日志
	 */
	public static final Logger LOGGER = LoggerFactory.getLogger(BlogXingfeiWebApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(BlogXingfeiWebApplication.class, args);
		LOGGER.info("====================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>web启动成功<<<<<<<<<<<<<<<<<<<<<<<<<<<<=================");
	}
}
