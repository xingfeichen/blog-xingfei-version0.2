package com.xingfei.blog.controller.message;

import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;
import javax.websocket.server.ServerEndpointConfig.Configurator;
import java.util.List;

/**
 * 获取token
 * Created by yhang on 2017/5/2.
 * http://www.cnblogs.com/zhaoww/p/5119706.html?utm_source=tuicool&utm_medium=referral
 */
public class GetHttpSessionConfigurator extends Configurator {

    @Override
    public void modifyHandshake(ServerEndpointConfig sec,
                                HandshakeRequest request, HandshakeResponse response) {

        List list = request.getHeaders().get("cookie");
        String token = null;
        if(list!=null){
            String values = (String)list.get(0);
            String[] array = values.split(";");

            for (String value:array) {
                if(value.contains("token")){
                    token = value;
                    break;
                }
            }
            //token=1B8DE3884B6D7348B9D9E15C716E58C1
            token = token.substring(token.indexOf("=")+1);
        }
        // TODO Auto-generated method stub
        sec.getUserProperties().put(String.class.getName(),token);
    }
}
