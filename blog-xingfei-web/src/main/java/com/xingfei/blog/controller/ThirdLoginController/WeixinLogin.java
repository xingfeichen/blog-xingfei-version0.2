package com.xingfei.blog.controller.ThirdLoginController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 第三方登陆接口 weixin
 * 需要企业认证公众号上线之后再做吧
 * Created by yhang on 2017/4/18.
 */
@Controller
@RequestMapping("/login")
public class WeixinLogin {

    /**
     * qq_login
     */
    @RequestMapping(value = "/weixin",method = RequestMethod.GET)
    public String getCode(HttpServletRequest request, HttpServletResponse response) {
        return null;
    }
}
