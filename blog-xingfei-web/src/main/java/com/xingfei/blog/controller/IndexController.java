package com.xingfei.blog.controller;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.xingfei.blog.activemq.producer.ActiveMQProducer;
import com.xingfei.blog.controller.base.BaseController;
import com.xingfei.blog.enums.BannerEnableTypeEnum;
import com.xingfei.blog.enums.BannerTypeEnum;
import com.xingfei.blog.redisCluster.RedisClusterUtil;
import com.xingfei.blog.service.BlogArticleService;
import com.xingfei.blog.service.WebBannerService;
import com.xingfei.blog.service.WebIndexService;
import com.xingfei.blog.service.WebUserService;
import com.xingfei.blog.vo.BannerVo;
import com.xingfei.blog.vo.UserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * 首页控制器
 *
 * @author: chenxingfei
 * @time: 2017/3/27  14:42
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/index")
public class IndexController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(IndexController.class);
    @Autowired
    private WebUserService webUserService;

    @Autowired
    private BlogArticleService blogArticleService;

    @Autowired
    private WebIndexService webIndexService;

    @Autowired
    private RedisClusterUtil redisCluster;

    @Autowired
    private ActiveMQProducer activeMQProducer;

    @Autowired
    private WebBannerService webBannerService;

    /**
     * 首页
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("")
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
        UserVO userVO = super.getUserVoByCookie(request);
        //跳转首页
        ModelAndView modelAndView = new ModelAndView("/index");
        if(userVO!=null){
            modelAndView.addObject("user", userVO);
        }
        this.webIndexService.getIndexViewData(modelAndView,userVO);
        //加载首页轮播图
        BannerVo bannerVo = new BannerVo();
        // 查询可用轮播图
        bannerVo.setEnable(BannerEnableTypeEnum.BANNER_ENABLE.ordinal());
        List<BannerVo> bannerVos = webBannerService.listMainBanners(bannerVo);
        if (CollectionUtils.isNotEmpty(bannerVos)){
            List<BannerVo> mainBanners = new ArrayList<>();
            List<BannerVo> secondBanners = new ArrayList<>();
            bannerVos.forEach(action ->{
                if(Objects.equals(action.getType(), BannerTypeEnum.BANNER_FOR_MAIN.toString())){
                    mainBanners.add(action);
                }
                else if(Objects.equals(action.getType(),BannerTypeEnum.BANNER_FOR_SECOND.toString())){
                    secondBanners.add(action);
                }
            });
            modelAndView.addObject("mainBanners",mainBanners);
            modelAndView.addObject("secondBanners",secondBanners);
        }
        return modelAndView;
    }

}
