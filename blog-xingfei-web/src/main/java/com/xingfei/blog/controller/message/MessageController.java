package com.xingfei.blog.controller.message;

import com.xingfei.blog.service.WebMessageService;
import com.xingfei.blog.service.WebUserService;
import com.xingfei.blog.vo.UserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * web 推送消息相关
 * Created by yhang on 2017/4/25.
 */
@RequestMapping("/message")
@Controller
public class MessageController {

    private static Logger logger = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    private WebUserService webUserService;

    @Autowired
    private WebMessageService webMessageService;

    @RequestMapping("")
    public ModelAndView toMessage(HttpServletRequest request) {
        UserVO userVO = webUserService.getUser4Redis(request);
        ModelAndView mv = new ModelAndView("/webSocket");
        mv.addObject("userVO", userVO);
        return mv;
    }

    /**
     * 消息
     * @param request
     * @return
     */
    @RequestMapping("/messages")
    public ModelAndView messages(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView("/personal/jianyi-personal-setting");
        return modelAndView;
    }

    /**
     * 获取未读消息
     * @param request
     * @return
     */
    @RequestMapping("/getUnreadmessages")
    public @ResponseBody Map<String,Object> getUnreadSecretMessage(HttpServletRequest request){
        UserVO userVO = webUserService.getUser4Redis(request);
        //私信未读集合+系统消息+未读关注
        Map<String,Object> map = webMessageService.getUnreadSecretMessage(userVO.getId());
        //map.put("secret_msg",lists);
        //map.put("sys_msg",lists2);
        return map;
    }
}
