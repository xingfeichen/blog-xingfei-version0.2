package com.xingfei.blog.controller;

import com.xingfei.blog.service.WebEmailService;
import com.xingfei.blog.service.WebUserService;
import com.xingfei.blog.utils.UserThreadLocal;
import com.xingfei.blog.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * 通过邮件访问网址（不同浏览器直接不能获取cookie）
 * Created by yhang on 2017/6/15.
 */
@Controller
@RequestMapping("/email")
public class EmailController {

    @Autowired
    private WebEmailService webEmailService;
    @Autowired
    private WebUserService webUserService;

    @RequestMapping(value = "personalConfirm",method = RequestMethod.GET)
    public String confireEmail(HttpServletRequest request,@RequestParam String ak){
        boolean b = webEmailService.checkEmailCode(ak);
        // FIXME: 2017/7/2 chenxingfei 获取用户信息用户拼接文章数据
        UserVO userVO = UserThreadLocal.get();
        if(b){
            //更改完成 加载数据
            this.webUserService.firstToLoad(request,userVO);
            request.setAttribute("emailPoint","邮箱绑定成功！");
            return "index";
        }else{
            return "/error/jianyi-500";
        }
    }
}
