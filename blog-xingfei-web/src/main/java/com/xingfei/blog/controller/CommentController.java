package com.xingfei.blog.controller;

import com.xingfei.blog.controller.base.BaseController;
import com.xingfei.blog.service.WebCommentService;
import com.xingfei.blog.utils.IPUtil;
import com.xingfei.blog.utils.UserThreadLocal;
import com.xingfei.blog.vo.UserCommentVO;
import com.xingfei.blog.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 评论Controller
 * Created by chenxingfei on 2017/5/21.
 */
@Controller
@RequestMapping("/comment")
public class CommentController extends BaseController {

    @Autowired
    private WebCommentService webCommentService;

    /**
     * 保存评论
     *
     * @param userCommentVO
     * @return
     */
    @PostMapping("saveComment")
    @ResponseBody
    public Boolean saveComment(HttpServletRequest request, UserCommentVO userCommentVO) {
        UserVO user = UserThreadLocal.get();
        //组装数据
        userCommentVO.setCommitIp(IPUtil.getIpAddr(request));
        if (user != null) {
            userCommentVO.setCommitUserId(user.getId());
        }
        return webCommentService.saveComment(userCommentVO);
    }

    /**
     * 分页获取评论列表
     *
     * @param userCommentVO 获取评论查询条件
     * @param page          查询页
     * @param size          查询条数
     * @return
     */
    @RequestMapping("/listComments")
    @ResponseBody
    public List<UserCommentVO> userCommentVOList(UserCommentVO userCommentVO, Integer page, Integer size) {
        List<UserCommentVO> voList = webCommentService.listUserComments(userCommentVO, page, size);
        return voList;
    }
}
