package com.xingfei.blog.controller;

import com.xingfei.blog.controller.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 投诉举报相关controller
 * Created by xingfei on 2017/6/9.
 */
@Controller
@RequestMapping("/praise")
public class ComplaintsController extends BaseController {
}
