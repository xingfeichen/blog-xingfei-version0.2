package com.xingfei.blog.controller;

import com.xingfei.blog.service.WebBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Description 轮播图相关数据
 * @Author chenxingfei
 * @Date 2017/4/26 18:58
 */
@Controller
@RequestMapping("/banner")
public class BannerController {

    @Autowired
    private WebBannerService webBannerService;
    /**
     * 获取轮播图列表
     * @return
     */
    @RequestMapping(value = "/listBanner",method = RequestMethod.GET)
    public ModelAndView listBanner(HttpServletRequest request){
//        List<BannerVO> mainBanners = this.webBannerService.listMainBanner();
//        List<BannerVO> secondBanners = this.webBannerService.listSecondBanner();
        return null;
    }

}
