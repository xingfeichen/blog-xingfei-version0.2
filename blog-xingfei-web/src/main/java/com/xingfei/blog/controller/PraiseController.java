package com.xingfei.blog.controller;

import com.xingfei.blog.activemq.producer.ActiveMQProducer;
import com.xingfei.blog.controller.base.BaseController;
import com.xingfei.blog.dto.PariseDTO;
import com.xingfei.blog.enums.PariseOperateTypeEnum;
import com.xingfei.blog.enums.PariseTypeEnum;
import com.xingfei.blog.redisCluster.dao.RedisClusterDao;
import com.xingfei.blog.service.WebPariseService;
import com.xingfei.blog.utils.UserThreadLocal;
import com.xingfei.blog.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.NotNull;

/**
 * 赞功能controller
 * Created by xingfei on 2017/6/9.
 */
@Controller
@RequestMapping("/praise")
public class PraiseController extends BaseController {

    @Autowired
    private WebPariseService webPariseService;
    @Autowired
    private ActiveMQProducer activeMQProducer;
    @Autowired
    private RedisClusterDao redisClusterDao;
    /**
     * 点赞或取消点赞
     * @param relativeId 点赞对象id（用户id或者文章id）
     * @param type 点赞类型（文章点赞1，用户点赞2）
     * @return
     */
    @RequestMapping("/addOrDelete")
    @ResponseBody
    public boolean praise(@NotNull Long relativeId,@NotNull Integer type,Integer operatorType){
        UserVO userVO = UserThreadLocal.get();
        if(userVO!=null){
            PariseDTO pariseDTO = new PariseDTO();
            pariseDTO.setRelativeId(relativeId);
            pariseDTO.setType(type);
            pariseDTO.setUserId(userVO.getId());
            pariseDTO.setOperatorType(operatorType);
            activeMQProducer.sendQueueMessage(PariseOperateTypeEnum.PARISE_SURE.toString(),pariseDTO);
            return true;
        }
        return  false;
    }

}
