package com.xingfei.blog.controller;

import com.xingfei.blog.common.JsonResult;
import com.xingfei.blog.controller.base.BaseController;
import com.xingfei.blog.service.WebCollectionService;
import com.xingfei.blog.utils.UserThreadLocal;
import com.xingfei.blog.vo.CollectionVO;
import com.xingfei.blog.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.xingfei.blog.constants.ErrorConstants.RESULT_USER_NOT_LOGIN;
import static com.xingfei.blog.constants.ErrorConstants.SERVER_ERROR;
import static com.xingfei.blog.constants.ErrorConstants.SERVER_SUCCESS;

/**
 * 收藏controller
 *
 * @author chenxingfei
 * @create 2017-07-02 13:49
 **/
@Controller
@RequestMapping("/collection")
public class CollectionController extends BaseController {

    @Autowired
    private WebCollectionService collectionService;

    /**
     * 收藏或取消收藏
     *
     * @param collectionVO 文章id
     * @return
     */
    @PostMapping("/collect")
    @ResponseBody
    public JsonResult collect(CollectionVO collectionVO) {
        UserVO userVO = UserThreadLocal.get();
        if (userVO != null) {
            collectionVO.setCollectorId(userVO.getId());
            Boolean result = collectionService.collect(collectionVO);
            if(result){
                return new JsonResult(SERVER_SUCCESS,"用户未登陆");
            }
        }else{
            return new JsonResult(RESULT_USER_NOT_LOGIN,"用户未登陆");
        }
        return new JsonResult(SERVER_ERROR,"用户未登陆");
    }
}
