package com.xingfei.blog.controller;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.xingfei.blog.common.JsonResult;
import com.xingfei.blog.constants.GeneralConstants;
import com.xingfei.blog.controller.base.BaseController;
import com.xingfei.blog.dto.ArticleDTO;
import com.xingfei.blog.service.WebArticleService;
import com.xingfei.blog.utils.UserThreadLocal;
import com.xingfei.blog.vo.ArticleVo;
import com.xingfei.blog.vo.CollectionVO;
import com.xingfei.blog.vo.UserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 文章相关接口
 *
 * @author: chenxingfei
 * @time: 2017/5/7  21:22
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/article")
public class ArticleController extends BaseController {

    @Autowired
    private WebArticleService webArticleService;

    /**
     * 日志
     */
    public static final Logger logger = LoggerFactory.getLogger(ArticleController.class);

    /**
     * 跳转至写文章页面
     *
     * @param request
     * @param response
     * @return
     */
    @GetMapping("/toWriteArticle")
    public String toWriteArticle(HttpServletRequest request, HttpServletResponse response) {
        logger.info("=============跳转至文章编辑页面===============");
        return "/article/write-article";
    }

    /**
     * 写文章
     *
     * @param request
     * @param response
     * @param articleInfo ArticleVo json字符串加密数据
     * @return
     */
    @PostMapping("/writeArticle")
    @ResponseBody
    public JsonResult writeArticle(HttpServletRequest request, HttpServletResponse response, String articleInfo) {
        UserVO userVO = UserThreadLocal.get();
        if (userVO == null) {
            new JsonResult("/toWriteArticle", "500", "保存失败");
        }
        if (StringUtils.isNotEmpty(articleInfo)) {
            boolean result = false;
            if (StringUtils.isNotEmpty(articleInfo)) {
                result = this.webArticleService.writeArticle(articleInfo, request, userVO);
                if (result) {
                    return new JsonResult("/index", "200", "保存成功");
                }
            }

        }
        return new JsonResult("/toWriteArticle", "500", "保存失败");
    }

    /**
     * 获取文章详情
     *
     * @param articleId
     * @return
     */
    @RequestMapping(value = "/findArticleById/{articleId}", method = RequestMethod.GET)
    public ModelAndView findArticleById(@PathVariable Long articleId, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("/article/article-details");
        if (articleId == null) {
            return new ModelAndView("/error/jianyi-404");

        }
        UserVO userVO = UserThreadLocal.get();
        Long userId = 0L;
        if (userVO != null) {
            modelAndView.addObject("user", userVO);
            userId = userVO.getId();
        }
        ArticleVo articleVo = this.webArticleService.findArticleById(articleId, userId);
        if (articleVo == null) {
            articleVo = new ArticleVo();
        }
        modelAndView.addObject("article", articleVo);
        return modelAndView;
    }

    /**
     * 首页加载更多数据调用接口
     *
     * @param page
     * @return
     */
    @RequestMapping("/nextPage")
    @ResponseBody
    public List<ArticleVo> listArticles(Integer page,HttpServletRequest request) {
        UserVO userVO = super.getUserVoByCookie(request);
        ArticleDTO articleDto = new ArticleDTO();
        List<ArticleVo> articleVos = this.webArticleService.listByPage(articleDto, page, GeneralConstants.PAGE_SIZE);
        if (CollectionUtils.isNotEmpty(articleVos)) {
            for (ArticleVo articleVo : articleVos) {
                webArticleService.appendArticleOtherData(articleVo,userVO);
                webArticleService.appendUserInfo(articleVo);
            }
        }
        return articleVos;
    }

    /**
     * 我收藏的文章
     *
     * @param page
     * @return
     */
    @RequestMapping("/myArticeByCollection")
    @ResponseBody
    public List<ArticleVo> listColletionArticleFormeByPage(CollectionVO collectionVO ,Integer page,HttpServletRequest request) {
        UserVO userVO = super.getUserVoByCookie(request);
        List<ArticleVo> articleVos = this.webArticleService.listColletionArticleFormeByPage(collectionVO, page, GeneralConstants.PAGE_SIZE);
        if (CollectionUtils.isNotEmpty(articleVos)) {
            for (ArticleVo articleVo : articleVos) {
                webArticleService.appendArticleOtherData(articleVo,userVO);
                webArticleService.appendUserInfo(articleVo);
            }
        }
        return articleVos;
    }
}
