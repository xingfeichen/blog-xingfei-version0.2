package com.xingfei.blog.common;

/**
 * url枚举类
 * Created by yhang on 2017/5/24.
 */
public enum URLEnum {

    REMOTEIMG_URL("http://jianyiblog.com:8808/uploadFile/uploadImgStr"),

    DELETEIMG_URL("http://jianyiblog.com:8808/uploadFile/delImgByPath");

    /*REMOTEIMG_URL("http://127.0.0.1:8808/uploadFile/uploadImgStr"),

    DELETEIMG_URL("http://127.0.0.1:8808/uploadFile/delImgByPath");*/

    private final String value;

    URLEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
