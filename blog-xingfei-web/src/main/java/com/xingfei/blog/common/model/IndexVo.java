package com.xingfei.blog.common.model;

import com.xingfei.blog.vo.ArticleVo;
import com.xingfei.blog.vo.UserVO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 首页数据封装Vo
 * @author: chenxingfei
 * @time: 2017/3/27  16:25
 * To change this template use File | Settings | File Templates.
 */
public class IndexVo {

    UserVO userVO = new UserVO();
    //封装文章列表数据
    private List<ArticleVo> articleVos = new ArrayList<ArticleVo>();

    public UserVO getUserVO() {
        return userVO;
    }

    public void setUserVO(UserVO userVO) {
        this.userVO = userVO;
    }

    public List<ArticleVo> getArticleVos() {
        return articleVos;
    }

    public void setArticleVos(List<ArticleVo> articleVos) {
        this.articleVos = articleVos;
    }
}
