package com.xingfei.blog.common.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * 用于封装注册数据的vo对象
 *
 * @author: chenxingfei
 * @time: 2017/3/18  14:44
 * To change this template use File | Settings | File Templates.
 */
public class RegisterVo implements Serializable {

    private String phone;
    private String userName;//用户名
    private String email;//邮箱
    private String password;//密码
    private String token;//用户的token值
    private String passwordagain;//确认密码
    private String authcode;//验证码

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordagain() {
        return passwordagain;
    }

    public void setPasswordagain(String passwordagain) {
        this.passwordagain = passwordagain;
    }

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

    @Override
    public String toString() {
        return "RegisterVo{" +
                "phone='" + phone + '\'' +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", passwordagain='" + passwordagain + '\'' +
                ", authcode='" + authcode + '\'' +
                '}';
    }
}
