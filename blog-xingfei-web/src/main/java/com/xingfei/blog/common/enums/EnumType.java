package com.xingfei.blog.common.enums;

/**
 * Created by chenxingfei on 2017/6/10.
 */
public enum EnumType {

    COMMENT_ARTICLE("文章评论"),COMMENT_REPLAY("评论回复");

    private String typeInfo;
    EnumType(String typeInfo) {
        this.typeInfo = typeInfo;
    }

    public String getTypeInfo() {
        return typeInfo;
    }

    public void setTypeInfo(String typeInfo) {
        this.typeInfo = typeInfo;
    }

}
