package com.xingfei.blog.service;

import com.xingfei.blog.redisCluster.dao.RedisClusterDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 赞相关功能Service
 * Created by xingfei on 2017/6/9.
 */
@Service
public class WebPraiseService {

    /**
     * 注入redisDao
     */
    @Autowired
    private RedisClusterDao redisClusterDao;
}
