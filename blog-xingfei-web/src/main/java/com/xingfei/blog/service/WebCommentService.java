package com.xingfei.blog.service;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.xingfei.blog.constants.ErrorConstants;
import com.xingfei.blog.constants.RedisConstants;
import com.xingfei.blog.dto.UserCommentDTO;
import com.xingfei.blog.dto.UserDTO;
import com.xingfei.blog.dto.UserInfoDTO;
import com.xingfei.blog.enums.CommentTypeEnum;
import com.xingfei.blog.redisCluster.RedisClusterUtil;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.vo.UserCommentVO;
import com.xingfei.blog.vo.UserInfoVO;
import com.xingfei.blog.vo.UserVO;
import java.text.MessageFormat;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 品论服务类
 * Created by chenxingfei on 2017/5/21.
 */
@Service("webCommentService")
public class WebCommentService {

    @Autowired
    private BlogUserCommentService blogUserCommentService;

    @Autowired
    private BlogArticleService blogArticleService;

    @Autowired
    private BlogUserService blogBlogUserService;

    @Autowired
    private RedisClusterUtil redisClusterUtil;
    // 图片域名获取
    @Value("${jianyi.imageDomainUrl}")
    private String imageDomainUrl;

    /**
     * 保存评论数据
     *
     * @param userCommentVO
     * @return
     */
    public Boolean saveComment(UserCommentVO userCommentVO) {
        Boolean result = false;
        if (userCommentVO != null) {
            UserCommentDTO userCommentDTO = getDTOfromVO(userCommentVO);
            if (userCommentDTO != null && userCommentDTO.getType() != CommentTypeEnum.COMMENT_REPLAY.ordinal()) {
                ServiceResult<Long> getPublisherIdResult = blogArticleService.getPublisherIdByArticleId(userCommentDTO.getRelativeId());
                if (getPublisherIdResult != null && getPublisherIdResult.getCode() == ErrorConstants.RES_SUCCESS) {
                    userCommentDTO.setUserId(getPublisherIdResult.getResult());
                }
                // 评论文章，增加文章评论数
                String articleCommenttKey = MessageFormat.format(RedisConstants.ARTICLE_COMMENT_COUNT, userCommentVO.getRelativeId());
                if (redisClusterUtil.isExisit(articleCommenttKey)) {
                    redisClusterUtil.incr(articleCommenttKey);
                }else{
                    redisClusterUtil.set(articleCommenttKey,"1",0);
                }
            }
            ServiceResult<UserCommentDTO> serviceResult = blogUserCommentService.saveComment(userCommentDTO);
            if (serviceResult != null && serviceResult.getCode() == ErrorConstants.RES_SUCCESS) {
                result = true;
            }

        }
        return result;
    }

    /**
     * 分页获取评论列表
     *
     * @param userCommentVO 评论列表查询条件
     * @param page          查询当前页码
     * @param size          查询条数
     * @return
     */
    public List<UserCommentVO> listUserComments(UserCommentVO userCommentVO, Integer page, Integer size) {
        if (userCommentVO != null && page != null && size != null) {
            UserCommentDTO userCommentDTO = getDTOfromVO(userCommentVO);
            if (userCommentDTO != null) {
                ServiceResult<List<UserCommentDTO>> listServiceResult = blogUserCommentService.listCommentsByPage(userCommentDTO, page, size);
                if (listServiceResult != null && listServiceResult.getCode() == ErrorConstants.RES_SUCCESS) {
                    List<UserCommentDTO> userCommentDTOList = listServiceResult.getResult();
                    return getListVOsFromlistDTOs(userCommentDTOList);
                }
            }
        }
        return null;
    }


    /**
     * VO转换为DTO
     *
     * @param userCommentVO
     * @return
     */
    private UserCommentDTO getDTOfromVO(UserCommentVO userCommentVO) {
        if (userCommentVO != null) {
            UserCommentDTO userCommentDTO = new UserCommentDTO();
            BeanUtils.copyProperties(userCommentVO, userCommentDTO);
            return userCommentDTO;
        }
        return null;
    }

    /**
     * DTO转换为VO
     *
     * @param userCommentDTO
     * @return
     */
    private UserCommentVO getVOfromDTO(UserCommentDTO userCommentDTO) {
        if (userCommentDTO != null) {
            UserCommentVO userCommentVO = new UserCommentVO();
            BeanUtils.copyProperties(userCommentDTO, userCommentVO);
            // 获取评论用户数据
            if (userCommentVO.getCommitUserId() != null) {
                ServiceResult<UserInfoDTO> serviceResult = this.blogBlogUserService.getUserInfoDTOByUserId(userCommentVO.getCommitUserId());
                if (serviceResult != null && serviceResult.getCode() == ErrorConstants.RES_SUCCESS) {
                    UserInfoDTO userInfoDTO = serviceResult.getResult();
                    if (userInfoDTO != null) {
                        UserInfoVO userInfoVO = new UserInfoVO();
                        userInfoVO.setUserId(userInfoDTO.getUserId());
                        userInfoVO.setAvatarUrl(userInfoDTO.getAvatarUrl());
                        userInfoVO.setUserName(userInfoDTO.getUserName());
                        userCommentVO.setUserInfoVO(userInfoVO);
                    }
                }
            }
            // 封装被评论或被回复用户数据
            if (userCommentVO.getUserId() != null) {
                ServiceResult<UserInfoDTO> serviceResult = this.blogBlogUserService.getUserInfoDTOByUserId(userCommentVO.getUserId());
                if (serviceResult != null && serviceResult.getCode() == ErrorConstants.RES_SUCCESS) {
                    UserInfoDTO userInfoDTO = serviceResult.getResult();
                    if (userInfoDTO != null) {
                        UserInfoVO userInfoVO = new UserInfoVO();
                        userInfoVO.setUserId(userInfoDTO.getUserId());
                        if(StringUtils.isNotEmpty(userInfoDTO.getAvatarUrl()) && !userInfoDTO.getAvatarUrl().contains("http") ){

                            userInfoVO.setAvatarUrl(imageDomainUrl+userInfoDTO.getAvatarUrl());
                        }
                        userInfoVO.setUserName(userInfoDTO.getUserName());
                        userCommentVO.setUserInfoVOReplay(userInfoVO);
                    }
                }
            }
            return userCommentVO;
        }
        return null;
    }

    /**
     * 根据list<DTO> 转换为list<VO>
     *
     * @param dtoList
     * @return
     */
    private List<UserCommentVO> getListVOsFromlistDTOs(List<UserCommentDTO> dtoList) {
        if (CollectionUtils.isNotEmpty(dtoList)) {
            List<UserCommentVO> voList = new ArrayList<>();
            dtoList.forEach(action -> {
                UserCommentVO commentVO = getVOfromDTO(action);
                // 递归封装评论回复列表
                if (CollectionUtils.isNotEmpty(action.getReviewDTOs())) {
                    List<UserCommentVO> userCommentVOS = getListVOsFromlistDTOs(action.getReviewDTOs());
                    if (CollectionUtils.isNotEmpty(userCommentVOS)) {
                        commentVO.setCommentReplays(userCommentVOS);
                    }
                }
                voList.add(commentVO);
            });
            return voList;
        }
        return null;
    }


}
