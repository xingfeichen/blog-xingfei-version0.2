package com.xingfei.blog.service;

import com.xingfei.blog.constants.ErrorConstants;
import com.xingfei.blog.dto.UserInfoDTO;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.vo.UserInfoVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 用户详细信息web端service
 *
 * @author: chenxingfei
 * @time: 2017/1/19  12:06
 * To change this template use File | Settings | File Templates.
 */
@Service("webUserInfoService")
public class WebUserInfoService {

    @Autowired
    private BlogUserInfoService blogUserInfoService;

    /**
     * 根据id查询用户详细信息
     *
     * @param userId
     * @return
     */
    public UserInfoVO getUserInfoByUserId(Long userId) {
        ServiceResult<UserInfoDTO> serviceResult = this.blogUserInfoService.getUserInfoById(userId);
        //转换数据
        if (serviceResult.getResult() != null) {
            UserInfoVO userInfoVO = new UserInfoVO();
            BeanUtils.copyProperties(serviceResult.getResult(), userInfoVO);
            return userInfoVO;
        }
        return null;
    }

    /**
     * 根据条件校验用户是否重复
     *
     * @param userInfoVO 封装参数
     * @return
     */
    public boolean getUserInfoByCondition(UserInfoVO userInfoVO) {
        //返回结果标识
        boolean result = false;
        // 数据转换
        UserInfoDTO userInfoDTO = new UserInfoDTO();
        BeanUtils.copyProperties(userInfoVO, userInfoDTO);
        ServiceResult<List<UserInfoDTO>> listServiceResult = this.blogUserInfoService.listUserInfoByDTO(userInfoDTO);
        if (listServiceResult != null) {
            if (listServiceResult.getResult() == null || listServiceResult.getResult().size() < 1) {
                result = true;
            }
        }
        return result;
    }

    /**
     * 获取推荐作者列表
     * @return
     */
    public List<UserInfoDTO> listRecommendAuthor() {
        ServiceResult<List<UserInfoDTO>> serviceResult = blogUserInfoService.listRecommendAuthor();
        if (serviceResult != null && serviceResult.getCode() == ErrorConstants.RES_SUCCESS) {
            return serviceResult.getResult();
        }
        return null ;

    }

    /**
     *更新userinfo字段
     * @return
     */
    public boolean updateUserInfoField(UserInfoDTO userInfo) {
        ServiceResult<Boolean> serviceResult = blogUserInfoService.updateUserInfoByDTO(userInfo);
        if (serviceResult != null && serviceResult.getCode() == ErrorConstants.RES_SUCCESS) {
            return serviceResult.getResult();
        }
        return false;
    }
    public ServiceResult<Boolean> updateUserInfoField2(UserInfoDTO userInfo) {
        ServiceResult<Boolean> serviceResult = blogUserInfoService.updateUserInfoByDTO(userInfo);
        return serviceResult;
    }

    /**
     * 证用户名是否存在
     * @param userName
     * @return
     */
    public boolean checkUserName(String userName) {
        ServiceResult<Integer> serviceResult = blogUserInfoService.checkUserName(userName);
        if (serviceResult != null && serviceResult.getCode() == ErrorConstants.RES_SUCCESS) {
            if(serviceResult.getResult()>0){
                return true;
            }
            return false;
        }
        return false;
    }
}
