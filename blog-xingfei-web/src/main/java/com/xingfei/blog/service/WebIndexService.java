package com.xingfei.blog.service;

import com.xingfei.blog.constants.GeneralConstants;
import com.xingfei.blog.dto.ArticleDTO;
import com.xingfei.blog.dto.UserInfoDTO;
import com.xingfei.blog.vo.ArticleVo;
import com.xingfei.blog.vo.UserVO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @Description
 * @Author chenxingfei
 * @Date 2017/4/29 18:40
 */
@Service("webIndexService")
public class WebIndexService {


    @Autowired
    private WebUserInfoService webUserInfoService;
    @Autowired
    private WebArticleService webArticleService;

    /**
     * 封装首页相关数据
     *
     * @param modelAndView
     * @param userVO
     * @return
     */
    public void getIndexViewData(ModelAndView modelAndView, UserVO userVO) {
        ArticleDTO articleDTO = new ArticleDTO();
        //查询公共文章类型
        articleDTO.setArticleType(1l);
        //获取文章列表(默认加载15条数据)
        List<ArticleVo> articleVos = webArticleService.listByPage(articleDTO, 1, GeneralConstants.PAGE_SIZE);
        if (CollectionUtils.isNotEmpty(articleVos)) {
            for (ArticleVo articleVo : articleVos) {
                //组装文章其他数据
                webArticleService.appendArticleOtherData(articleVo, userVO);
                //组装用户数据
                webArticleService.appendUserInfo(articleVo);
            }
            modelAndView.addObject("articles", articleVos);
        }
        //封装推荐作者数据
        List<UserInfoDTO> infoDTOS = this.webUserInfoService.listRecommendAuthor();
        if (CollectionUtils.isNotEmpty(infoDTOS)) {
            modelAndView.addObject("recommends", infoDTOS);
        }

    }
}
