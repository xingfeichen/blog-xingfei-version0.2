package com.xingfei.blog.service;

import com.xingfei.blog.constants.ErrorConstants;
import com.xingfei.blog.dto.CollectionDTO;
import com.xingfei.blog.redisCluster.dao.RedisClusterDao;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.vo.CollectionVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import static com.xingfei.blog.constants.RedisConstants.ARTICLE_IS_COLLECTED_FOR_USER;

/**
 * 收藏服务
 *
 * @author chenxingfei
 * @create 2017-07-02 15:22
 **/
@Service
public class WebCollectionService {

    @Autowired
    private BlogCollectionService blogCollectionService;
    @Autowired
    private RedisClusterDao redisClusterDao;

    /**
     * @param collectionVO
     * @return 收藏结果
     * @Desc 收藏
     * @author chenxingfei
     * @methodName collect
     * @date 2017/7/2 15:28
     */
    public Boolean collect(CollectionVO collectionVO) {
        CollectionDTO collectionDTO = getDTOFromVO(collectionVO);
        ServiceResult<CollectionDTO> serviceResult = this.blogCollectionService.saveCollection(collectionDTO);
        if (serviceResult != null && serviceResult.getCode() == ErrorConstants.RES_SUCCESS) {
            String key = MessageFormat.format(ARTICLE_IS_COLLECTED_FOR_USER, collectionVO.getRelativeId(), collectionVO.getCollectorId());
            // 收藏成功，将收藏标识保存至缓存
            redisClusterDao.set(key, "COLLECTED", 0L);
            return true;
        }
        return false;
    }

    /**
     * @param collectionVO
     * @return list
     * @Desc 根据条件获取收藏列表
     * @author chenxingfei
     * @methodName listCollectionVos
     * @date 2017/7/15 10:39
     */
    public List<CollectionVO> listCollectionVos(CollectionVO collectionVO) {
        CollectionDTO collectionDTO = getDTOFromVO(collectionVO);
        ServiceResult<List<CollectionDTO>> listServiceResult = this.blogCollectionService.listCollectionByCondition(collectionDTO);
        if (listServiceResult != null && listServiceResult.getCode() == ErrorConstants.RES_SUCCESS) {
            List<CollectionDTO> collectionDTOS = listServiceResult.getResult();
            if (!CollectionUtils.isEmpty(collectionDTOS)) {
                List<CollectionVO> collectionVOS = new ArrayList<>();
                collectionDTOS.forEach(action -> collectionVOS.add(getVOFromDTO(action)));
                return collectionVOS;
            }
        }
        return null;

    }

    /**
     * 根据vo获取dto
     *
     * @param collectionVO
     * @return
     */
    private CollectionDTO getDTOFromVO(CollectionVO collectionVO) {
        if (collectionVO != null) {
            CollectionDTO collectionDTO = new CollectionDTO();
            BeanUtils.copyProperties(collectionVO, collectionDTO);
            return collectionDTO;
        }
        return null;
    }

    /**
     * 根据dto获取vo
     *
     * @param collectionDTO
     * @return
     */
    private CollectionVO getVOFromDTO(CollectionDTO collectionDTO) {
        if (collectionDTO != null) {
            CollectionVO collectionVO = new CollectionVO();
            BeanUtils.copyProperties(collectionDTO, collectionVO);
            return collectionVO;
        }
        return null;
    }
}
