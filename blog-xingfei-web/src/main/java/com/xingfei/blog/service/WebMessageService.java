package com.xingfei.blog.service;

import com.xingfei.blog.result.ServiceResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by yhang on 2017/5/4.
 */
@Service("webMessageService")
public class WebMessageService {

    @Autowired
    private BlogMessageService blogMessageService;

    public Map<String, Object> getUnreadSecretMessage(Long id) {
        ServiceResult<Map<String, Object>> serviceResult = blogMessageService.listUnreadMessage(id,0+"");
        return serviceResult.getResult();
    }
}
