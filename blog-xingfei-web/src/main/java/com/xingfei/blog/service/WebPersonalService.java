package com.xingfei.blog.service;

import com.alibaba.fastjson.JSON;
import com.xingfei.blog.dto.UserDTO;
import com.xingfei.blog.redisCluster.RedisClusterUtil;
import com.xingfei.blog.vo.UserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * Created by yhang on 2017/5/25.
 */
@Service("personalService")
public class WebPersonalService {

    private final static Logger logger = LoggerFactory.getLogger(WebPersonalService.class);

    private static String Profix_User = "user:";

    @Autowired
    private RedisClusterUtil redisCluster;
    @Autowired
    private WebUserService webUserService;
    @Autowired
    private WebUserInfoService webUserInfoService;

    /**
     * 更新用户缓存
     * @param token
     * @param userVO
     * @return
     */
    public boolean updateRedis(String token, UserVO userVO){
        try {
            String value = JSON.toJSONString(userVO);
            //TODO 更新用户缓存，之前是默认记住密码的时间也改成30分钟
            //RedisUtil.set(Profix_User+token,value,Integer.valueOf(60 * 30),1);
            redisCluster.set(Profix_User+token,value,Integer.valueOf(60 * 30));
        } catch (Exception e) {
            logger.error(e.toString());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 执行修改用户邮箱
     */
    public boolean updateUserEmail(UserDTO userDTO){
        boolean b = webUserService.updateUserInfoField(userDTO);
        return b;
    }

    /**
     * 修改用户手机
     * @param id
     * @param phone
     * @return
     */
    public boolean updateUserPhone(Long id, String phone) {
        UserDTO user = new UserDTO();
        user.setId(id);
        user.setPhone(phone);
        Integer integer = webUserService.updateUserAndUserInfo(user);
        if(integer>0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 验证用户名是否存在
     * @param userName
     * @return
     */
    public boolean checkUserName(String userName) {
        return  webUserInfoService.checkUserName(userName);
    }
}
