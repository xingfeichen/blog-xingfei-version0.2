package com.xingfei.blog.service;

import com.xingfei.blog.constants.ErrorConstants;
import com.xingfei.blog.dto.PariseDTO;
import com.xingfei.blog.redisCluster.dao.RedisClusterDao;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.utils.UserThreadLocal;
import com.xingfei.blog.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 赞相关service
 * Created by chenxingfei on 2017/6/15.
 */
@Service
public class WebPariseService {

    @Autowired
    private BlogPariseService blogPariseService;

    /**
     * 点赞
     * @param relativeId
     * @param type
     * @return
     */
    public boolean parise(Long relativeId, Integer type,Long userId) {
        PariseDTO pariseDTO = new PariseDTO();
        pariseDTO.setRelativeId(relativeId);
        pariseDTO.setType(type);
        pariseDTO.setUserId(userId);
        ServiceResult<PariseDTO> serviceResult = blogPariseService.saveParise(pariseDTO);
        if (serviceResult != null && serviceResult.getCode() == ErrorConstants.RES_SUCCESS) {
            return true;
        }
        return false;
    }
}
