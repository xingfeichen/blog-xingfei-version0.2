package com.xingfei.blog.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by yhang on 2017/1/22.
 */
@Configuration
public class RegistInterceptor extends WebMvcConfigurerAdapter {

    /**
     * 加载拦截器
     *
     * @param registry
     */
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new GlobalInterceptor()).excludePathPatterns("/**/imgCode", "/**/toRegister", "/**/redirectWeiboUrl", "/**/register/**", "/**/login", "/**/toLogin",
                "/**/register", "/**/toLogin", "/**/findArticleById", "/**/index", "/**/article/**", "/**/redirectQqUrl",
                "/**/weibo", "/**/qq", "/**/logout", "/**/nextPage").addPathPatterns("/**");
        super.addInterceptors(registry);
    }
}
