package com.xingfei.blog.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


@Configuration
public class FreemarkerConfig {

    private static final String CONTEXT_PATH = "basePath";

    @Bean
    protected ServletContextListener listener() {
        return new ServletContextListener() {
            public void contextInitialized(ServletContextEvent sce) {
                // 获取web应用对象
                ServletContext application = sce.getServletContext();

                // 获取web应用路径
                String basePath = application.getContextPath();

                // 保存在web应用范围内
                application.setAttribute(CONTEXT_PATH, basePath);
            }

            public void contextDestroyed(ServletContextEvent sce) {
                //必须覆盖的方法
            }
        };
    }
}
