/**
 * layer校验工具类
 * Created by chenxingfei on 2017/4/23.
 */

var LayerUtils = {
    /**
     * 需要在input右侧提示tips的校验方法
     * @param obj 必须是jquery对象
     */
    checkForTips: function (message, obj) {
        if (!CommonValidate.isNull(obj)) {
            //弹出提示框
            layer.tips(message, obj, {
                tips: [2, '#cc1a42'],
                time: 4000
            });
            //聚焦到错误input
            obj.focus();
        }
    },
    /*
     *http://www.layui.com/doc/modules/layer.html
     *右下角实时消息提示
     */
    moduleMessages:function(message){
        layer.open({
            title: '消息通知',
            content: message,
            offset: 'rb',
            btn:'我知道啦',
            time:2000,
            anim:6,
            move:'false',
        });
    },
}