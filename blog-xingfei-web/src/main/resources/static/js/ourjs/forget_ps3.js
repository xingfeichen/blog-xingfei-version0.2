/**
 * Created by yhang on 2017/4/6.
 */

/**
 * 监听回车键
 * @param event
 */
document.onkeydown=function(event){
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if(e && e.keyCode==13){ // enter 键
        login_ajax();
    }
};

/**
 * DEC加密
 */
function encryptByDES(message, key) {
    var keyHex = CryptoJS.enc.Utf8.parse(key);
    var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    });
    return encrypted.toString();
}
function GetJsonData() {
    var username = $("#username").val();
    var password = $("#password").val();
    var confirmPassword = $("#confirmPassword").val();
    //校验两次输入的密码
    if(password!=confirmPassword){
        alert("密码输入不一致！");
        return false;
    }
    password = encryptByDES(password,"BLOG-XINGFEI-==");

    var json={
        "username":username,
        "password":password,
    }
    return json;
}
function login_ajax(){
    var data = GetJsonData();
    $.ajax({
        type: "POST",
        url: "/login/forgetPassword3",
        dataType: "json",
        data: data,
        success: function(result){
            if(result.errorCode=="200"){
                window.location.href=result.errorMsg;
            }
            else{
                alert(result.errorMsg);
            }
        }
    });
}
