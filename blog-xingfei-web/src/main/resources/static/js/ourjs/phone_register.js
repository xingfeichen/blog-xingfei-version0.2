/*********注册代码进行模块化*********/
var PhoneRegister = {
    /*抽取公共常量*/
    constants: {
        PHONE: function () {
            return $("#phone").val();
        },
        AUTH_CODE: function(){
            return $("#authcode").val()
        },
        CUR_COUNT: 60
    },
    /*初始换函数*/
    init: function (params) {
        // //手机号
        // PhoneRegister.constants.PHONE =  params['phone'];
        // //验证码
        // PhoneRegister.constants.AUTH_CODE =  params['authcode'];
        //调用第一步注册
        // PhoneRegister.register.phone_register_step_one(PhoneRegister.constants.PHONE, PhoneRegister.constants.AUTH_CODE);

    },
    /*地址管理函数*/
    URL: {
        BASE_URL: $("#basePathId").val(), //获取页面的basePath
        /*跳转至第二步登录路径*/
        phone_register_to_page_register1: function(){
            return PhoneRegister.URL.BASE_URL+"/register/toRegisterStep2";
        },
        /*第一步登录*/
        phone_register_step_one_url: function (param) {
            return PhoneRegister.URL.BASE_URL + "/register/phoneRegisterStepOne";
        },
        /*第二步登录*/
        phone_register_step_two_url: function (param) {
            return PhoneRegister.URL.BASE_URL +"/register/registerStepTwo";
        },
        /*获取跳转到邮箱登录路径*/
        email_register_url: function (param) {
            return PhoneRegister.URL.BASE_URL +"/register/toEmailRegisterPage";
        },
        /*获取跳转到邮箱登录路径*/
        getObjUrl: function (model,method) {
            return PhoneRegister.URL.BASE_URL +"/"+model+"/"+method;
        }
    },
    toRegisterSuccessPage: function(){
        //第一步登录成功，跳转至第二步
        location.href = PhoneRegister.URL.getObjUrl('register', 'toRegisterSuccessPage')+"?tk_u="+$("#token").val();
    },
    /*注册函数*/
    register: {
        phone_register_step_one: function () {
            var password = PhoneRegister.encryptByDES($("#password").val(), "BLOG-XINGFEI-==");
            //通过post提交数据
            $.post(PhoneRegister.URL.getObjUrl('register', 'commonRegister'), {
                'phone': $("#phone").val(),
                'password': password
            }, function (result) {
                if (result['errorCode'] == '200') {
                    //第一步登录成功，跳转至第二步
                    //第一步登录成功，跳转至第三步
                    location.href = PhoneRegister.URL.getObjUrl('register', 'toRegisterStep2')+"?tk_u="+ result['data'];
                } else {
                    alert(result['errorMsg']);
                }

            })
        },
        phone_register_step_two: function () {
            //通过post提交数据
            $.post(PhoneRegister.URL.getObjUrl('register', 'phoneRegisterStepTwo'), {
                'email': $("#email").val(),
                'password': $("#password").val(),
            }, function (result) {
                if (result['errorCode'] == '200') {
                    //第一步登录成功，跳转至第三步
                    location.href = PhoneRegister.URL.getObjUrl('register', 'toRegisterSuccessPage')+"?ak="+$("#token").val();
                } else {
                    alert(result['errorMsg']);
                }

            })
        },

    },
    /**
     * DES加密
     * @param message
     * @param key
     * @returns {*|string}
     */
    encryptByDES:function(message, key) {
        var keyHex = CryptoJS.enc.Utf8.parse(key);
        var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypted.toString();
    },
    sendMessage: function () {
        var phone = $("#phone").val();//手机号码
        if (phone != null && phone != '') {
            //设置button效果，开始计时
            $("#messageCodeBtn").attr("disabled", "true");
            $("#messageCodeBtn").text(PhoneRegister.constants.CUR_COUNT + "秒后重新发送");
            var InterValObj = window.setInterval(PhoneRegister.SetRemainTime, 1000); //启动计时器，1秒执行一次
            //向后台发送处理数据
            $.ajax({
                type: "POST", //用POST方式传输
                dataType: "json", //数据格式:JSON
                url: PhoneRegister.URL.BASE_URL+'/code/sendMessageCode', //目标地址
                data: {'phone': phone},
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                success: function (msg) {
                    layer.msg('短信发送成功，请查收！', {icon: 1})
                }
            });
        } else if(phone=null) {
            alert("手机号码不能为空！");
        }

    },
    SetRemainTime: function (InterValObj) {
        if (PhoneRegister.constants.CUR_COUNT == 0) {
            window.clearInterval(InterValObj);//停止计时器
            $("#messageCodeBtn").removeAttr("disabled");//启用按钮
            $("#messageCodeBtn").text("重新发送验证码");
        }
        else {
            PhoneRegister.constants.CUR_COUNT--;
            $("#messageCodeBtn").text(PhoneRegister.constants.CUR_COUNT + "秒后重新发送");
        }
    },
    checkFormStep1: function () {
        $("#registerPhoneStepOneForm").validate({
            errorClass: "error",
            errorElement: "div",
            focusInvalid:false,
            submitHandler:function() {
                PhoneRegister.register.phone_register_step_one();
            },
            rules: {
                phone: {
                    required: true,
                    minlength: 11,
                    remote: {
                        url: PhoneRegister.URL.getObjUrl('validate','checkRepetition'),
                        type: "get",               //数据发送方式
                        dataType: "json",
                        data: {
                            phone: function(){
                                return $("#phone").val();
                            }
                        },
                        dataFilter: function (result) {
                            var json = $.parseJSON(result)
                            if(json['errorCode']=='500'){
                                return "\"" + json['errorMsg'] + "\"";
                            }else{
                                return true;
                            }
                        }
                    }
                },
                authcode: {
                    required: true,
                    minlength: 6,
                    maxlength: 6,
                    digits: true,
                    remote: {
                        url: PhoneRegister.URL.getObjUrl('validate','checkAuthCode'),
                        type: "get",               //数据发送方式
                        dataType: "json",
                        data: {
                            phone: function(){
                                return $("#phone").val();
                            },
                            authcode: function(){
                                return $("#authcode").val();
                            }
                        },
                        dataFilter: function (result) {
                            var json = $.parseJSON(result)
                            if(json['errorCode']=='500'){
                                return "\"" + json['errorMsg'] + "\"";
                            }else{
                                return true;
                            }
                        }
                    }
                },
               /* username: {
                    required: true,
                    minlength: 2,
                    maxlength: 6,
                    remote: {
                        url: PhoneRegister.URL.getObjUrl('validate','checkRepetition'),
                        type: "get",               //数据发送方式
                        dataType: "json",
                        data: {
                            userName: function(){
                                return $("#username").val();
                            }
                        },
                        dataFilter: function (result) {
                            var json = $.parseJSON(result)
                            if(json['errorCode']=='500'){
                                return "\"" + json['errorMsg'] + "\"";
                            }else{
                                return true;
                            }

                        }
                    }
                },*/
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 32
                },
                // passwordagain: {
                //     required: true,
                //     minlength: 6,
                //     equalTo: "#password"
                // },
                agree: "required",
            },
            messages: {
                phone: {
                    required: "请输入手机号",
                    minlength: "手机号长度为11位",
                    maxlength: "手机号长度为11位",
                    remote: "该手机号已存在!"
                },
                // username: {
                //     required: "请输入用户名",
                //     minlength: "用户名最少2个字符",
                //     maxlength: "用户名不可超过6个字符",
                //     remote: "该用户名已存在!"
                // },
                authcode: {
                    required: "请输入手机验证码",
                    minlength: "验证码为为 6 个数字",
                    maxlength: "验证码为为 6 个数字",
                    digits: "必须为数字",
                    remote: "验证码错误!"
                },
                password: {
                    required: "请输入密码",
                    minlength: "密码长度不足",
                    maxlength: "密码不能超过32字符"
                },
                // passwordagain: {
                //     required: "请再次输入密码",
                //     equalTo: "两次密码输入不一致",
                // },
                agree: "请接受我们的声明",
            }
        })
    },
    /*校验表单*/
    checkFormStep2: function(param){
        $("#registerPhoneStepTwoForm").validate({
            // debug:true,
            errorClass: "error",
            errorElement: "div",
            focusInvalid:false,
            submitHandler:function() {
                PhoneRegister.register.phone_register_step_two();
            },
            rules: {
                username: {
                    required: true,
                    minlength: 2,
                    maxlength: 6,
                    remote: {
                        url: PhoneRegister.URL.getObjUrl('validate','checkRepetition'),
                        type: "get",               //数据发送方式
                        dataType: "json",
                        data: {
                            userName: function(){
                                return $("#username").val();
                            }
                        },
                        dataFilter: function (result) {
                            var json = $.parseJSON(result)
                            if(json['errorCode']=='500'){
                                // return "\"" + json['errorMsg'] + "\"";
                                return false;
                            }else{
                                return true;
                            }

                        }
                    }
                },
                email: {
                    required: true,
                    email: true,
                    // remote: {
                    //     url: PhoneRegister.URL.getObjUrl('validate','checkRepetition'),
                    //     type: "get",               //数据发送方式
                    //     dataType: "json",
                    //     data: {
                    //         email: function(){
                    //             return $("#email").val();
                    //         }
                    //     },
                    //     dataFilter: function (result) {
                    //         var json = $.parseJSON(result)
                    //         if(json['errorCode']=='500'){
                    //             // return "\"" + json['errorMsg'] + "\"";
                    //             return false;
                    //         }else{
                    //             return true;
                    //         }
                    //
                    //     }
                    // }
                },
            },
            messages: {
                username: {
                    required: "请输用户名",
                    minlength: "用户名最少2个字符",
                    maxlength: "用户名不可超过6个字符",
                    remote: "该用户名已存在!"
                },
                email: {
                    required: "请输入邮箱",
                    email: "邮箱不合法"
                },
            }
        })
    },
    changeImg: function () {
        var imgSrc = $("#imgObj");
        var src = imgSrc.attr("src");
        imgSrc.attr("src", PhoneRegister.chgUrl(src));
    },
    /**
     * 为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
     */
    chgUrl: function (url) {
        var timestamp = (new Date()).valueOf();
        url = url.substring(0, 37);
        if ((url.indexOf("&") >= 0)) {
            url = url + "×tamp=" + timestamp;
        } else {
            url = url + "?timestamp=" + timestamp;
        }
        return url;
    },
    countDown: function (secs) {
        var jumpTo = document.getElementById('jumpto');
        jumpTo.innerHTML = secs;
        if (--secs > 0) {
            setTimeout("PhoneRegister.countDown(" + secs + ")", 1000);
        }
        else {
            location.href = PhoneRegister.URL.getObjUrl('login', 'loginByToken') + "?tk_u=" + $("#tk_u").val();
        }
    }

}