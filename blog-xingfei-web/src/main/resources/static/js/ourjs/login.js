/**
 * Created by yhang on 2017/2/3.
 */

function login_ajax(){

    var data = GetJsonData();
    $.ajax({
        type: "POST",
        url: "/login/toLogin",
        dataType: "json",
        data: data,
        success: function(result){
            if(result.errorCode=="200"){
                window.location.href=result.errorMsg;
            }
            else{
                alert(result.errorMsg);
            }
        }
    });
}
function GetJsonData() {
    var userName = $("#userName").val();
    var password = $("#password").val();
    //0未选中  1选中
    var remember = $("#remember").val();
    password = encryptByDES(password,"BLOG-XINGFEI-==");

    var json={
        "userName":userName,
        "password":password,
        "remember":remember
    }

    return json;
}
/**
 * DEC加密
 */
function encryptByDES(message, key) {
    var keyHex = CryptoJS.enc.Utf8.parse(key);
    var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    });
    return encrypted.toString();
}
/**
 * 请求weibo url
 */
function getWeiboUrl(){
    $.ajax({
        type: "GET",
        url: "login/redirectWeiboUrl",
        dataType: "json",
        success: function(result){
            if(result.errorCode=="200"){
                window.location.href=result.errorMsg;
            }
            else{
                alert(result.errorMsg);
            }
        }
    });
}
/**
 * 监听回车键
 * @param event
 */
document.onkeydown=function(event){
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if(e && e.keyCode==13){ // enter 键
        login_ajax();
    }
};
function qqRegist(){
    $.ajax({
        type: "GET",
        url: "/login/qqRegist",
        dataType: "json",
        success: function(result){
            if(result.errorCode=="200"){
                window.location.href=result.errorMsg;
            }
            else{
                alert(result.errorMsg);
            }
        }
    });
}
function weiboRegist(){
    $.ajax({
        type: "GET",
        url: "/login/weiboRegist",
        dataType: "json",
        success: function(result){
            if(result.errorCode=="200"){
                window.location.href=result.errorMsg;
            }
            else{
                alert(result.errorMsg);
            }
        }
    });
}
function changeImg() {
    var imgSrc = $("#imgObj");
    var src = imgSrc.attr("src");
    imgSrc.attr("src", chgUrl(src));
}
/**
 * 为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
 */
function chgUrl(url) {
    var timestamp = (new Date()).valueOf();
    url = url.substring(0, 37);
    if ((url.indexOf("&") >= 0)) {
        url = url + "×tamp=" + timestamp;
    } else {
        url = url + "?timestamp=" + timestamp;
    }
    return url;
}
//跳转忘记密码第二步
function forgetPassword2(){
    $("#forget_form").submit();
}
