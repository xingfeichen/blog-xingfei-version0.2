/**
 * 首页相关数据处理
 * Created by an on 2017/3/26.
 */
var INDEX = {
    /*首页中关于路径的处理*/
    URL:{
        /*根据模块和方法拼接参数*/
        GET_URK_BY_PARAM :function (model,method) {
            return "/"+model+"/"+method;
        }
    },
    /**
     * 加载更多数据
     * @param page 当前页
     */
    loadData : function (obj) {
        var page = $(obj).attr('data-page');
        page = parseInt(page)+1;
        //通过ajax获取数据动态加载
        $.get(this.URL.GET_URK_BY_PARAM("article","nextPage"),{'page' : page},function (data) {
            if (data != null && data.length > 0) {
                var elm = '';
                for (var i = 0; i < data.length; i++) {
                    var articleTime = INDEX.formatDate(data[i].articleTime);
                     elm += "<li><div><img src='"+data[i].userVO.userInfoVO.avatarUrl+"'/><a href=''>"+data[i].userVO.userInfoVO.userName+"</a><span>" +
                         articleTime+"</span></div><div>" +
                         "<a href=''>"+data[i].articleName+"</a><p>"+data[i].articleContentVO.acticleContent+"</p>" +
                         "<img src='"+data[i].imgUrl+"'/></div>" +
                         "<div class='clearfix'><a href=''>@产品</a><a href=''>"+data[i].collectCount+"</a><a href=''>"+data[i].commentCount+"</a><a href=''>"+data[i].viewCount+"</a></div></li>";
                }
                $(".show-area").append(elm);
                $(obj).attr('data-page',page);
                mui(obj).button('reset');
            }else{
                $("#load_more").text("没有更多数据了");
            }
        })
    },
    /*日期格式化*/
    formatDate: function(strTime) {
        var date = new Date(strTime);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        }
        var day = date.getDate();
        if (day < 10) {
            day = "0" + day;
        }
        var hour = date.getHours();
        if (hour < 10) {
            hour = "0" + hour;
        }
        var min = date.getMinutes();
        if (min < 10) {
            min = "0" + min;
        }
        var second = date.getSeconds();
        if (second < 10) {
            second = "0" + second;
        }
        return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + second;
    }

}