/**
 * 初始化注册页面相关数据和校验
 * Created by chenxingfei on 2017/4/23.
 */
$(function () {
    /*校验昵称*/
    $("#username").on("blur",function () {
        REGIST.checkV2.checkUserName($(this));
    })
    /*校验手机号*/
    $("#phone").on("blur",function () {
        REGIST.checkV2.checkPhone($(this));
    })
    /*校验密码*/
    $("#password").on("blur",function () {
        REGIST.checkV2.checkPassword($(this));
    })
    /*校验验证码*/
    $("#check_code").on("blur",function () {
        REGIST.checkV2.checkAuthCode($(this));
    })
    /*给表单绑定表单提交事件*/
    $("#register-id").on("click",function () {
        REGIST.regist()
    })
})