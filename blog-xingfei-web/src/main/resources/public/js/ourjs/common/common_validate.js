/**
 * 抽取公共校验js代码</br>
 * Created by chenxingfei on 2017/4/23.
 */
var CommonValidate = {

    /**
     * 判空
     * @param obj
     * @returns {boolean}
     */
    isNull: function (obj) {
        if (obj == null || obj == '' || obj == undefined) {
            return true;
        }
        return false;
    },
    /**
     * 判非空
     * @param obj
     * @returns {boolean}
     */
    isNotNull: function (obj) {
        if (obj == null || obj == '' || obj == undefined) {
            return false;
        }
        return true;
    },

    /**
     * 校验手机号
     * @param obj
     */
    isMobile: function (obj) {
        var mobile = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
        return obj.length == 11 && mobile.test(obj);
    },
    /**
     * 校验是否为数字
     * @param obj
     */
    isDigit: function (obj) {
        if (!isNaN(obj)) {
            return true;
        }
        return false;
    },

    /**
     * 校验邮箱格式
     */
    isEmail: function (obj) {
        var email = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
        return email.test(obj);
    }
}