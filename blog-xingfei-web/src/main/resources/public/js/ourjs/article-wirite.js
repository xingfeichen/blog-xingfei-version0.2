/**
 * 首页相关数据处理
 * Created by an on 2017/3/26.
 */
var ARTICLE_INDEX = {

    init: function () {
        var ue = UE.getEditor('editor', {
//				toolbars: [
//					['bold', 'italic', 'underline', 'strikethrough', 'insertimage', 'justifyleft', 'justifyright', 'justifycenter', 'justifyjustify', 'link', 'unlink']
//				],
            autoHeightEnabled: false,
            elementPathEnabled: false,
            wordCount: false
        });
        //一键清空
        $('#clearDoc').on('click', function() {
            ue.execCommand('cleardoc');
        });
        //发布文章
        $('#myButton').on('click', function() {
            var html = ue.getContent();//获取文章HTML内容
            var hc = '<div class="clearfix">' +
                '<div class="col-md-4" style="line-height:40px;text-align:right;padding-right:15px;">文章类型</div>' +
                '<div class="col-md-8">' +
                '<select class="form-control" id="articleTypeId">' +
                '<option value="1" selected="selected">公开</option>' +
                '<option value="0">私有</option>' +
                '<option value="2">仅朋友可见</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '<div class="clearfix" style="margin-top:15px">' +
                '<div class="col-md-4" style="line-height:40px;text-align:right;padding-right:15px;">文章关键字</div>' +
                '<div class="col-md-8">' +
                '<input type="email" class="form-control" id="addValue" placeholder="情感">' +
                '<a id="addTab" class="btn btn-default" style="margin-top:10px">+</a>' +
                '</div>';
            layer.open({
                type: 1,
                title: "确认相关设置", //显示标题栏
                closeBtn: false,
                area: '500px;',
                shade: 0.5,
                id: 'LAY_layuipro', //设定一个id，防止重复弹出
                resize: false,
                btn: ['发布', '取消'],
                btnAlign: 'c',
                moveType: 1, //拖拽模式，0或者1
                content: '<div style="padding: 15px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;">' + hc + '</div>',
                success: function(layero) {
                    $('#addTab').on('click', function() {
                        var tabVal = $('#addValue').val();
                        (tabVal != "") && $(this).after('<a class="btn btn-default remove-btn keywordClass" href="#" style="margin-top:10px;margin-left: 10px;">' + tabVal + '</a>');
                    });
                    var btn = layero.find('.layui-layer-btn');
                    btn.find('.layui-layer-btn0').on('click', function() {
                        var articleVO = {};
                        var title = $("#articleTitlehrefId").text();
                        if (title==null){
                            layer.msg("文章标题不能为空",{icon:5});
                            return;
                        }
                        if (title == '未定义标题'){
                            layer.msg("请输入文章标题",{icon:5});
                            return;
                        }
                        articleVO.articleType=$("#articleTypeId").val();
                        var arr = new Array(0);
                        var i = 0;
                        $(".keywordClass").each(function(){
                            arr[i++] = $(this).text();
                        });
                        if(arr.length > 0){
                            articleVO.keyWord = arr.join();
                        }else{
                            layer.msg("请至少输入一个关键字",{icon:5});
                            return;
                        }
                        articleVO.articleName = $("#articleTitlehrefId").text() ;
                        var articleContentVO = {};
                        articleContentVO.acticleContent = ue.getContent();
                        //var content = JSON.stringify(articleContentVO);
                        articleVO.articleContentVO =  articleContentVO;
                        var article = JSON.stringify(articleVO);
                        var articleInfo =ARTICLE_INDEX.encryptByDES(article,"JIANYIBLOG-ARTICLE-PUB===");
                        //发布操作
                        //code...
                        $.post("/article/writeArticle",{'articleInfo':articleInfo},function (result) {
                            // layer.msg('文章发布成功，正在跳转至首页', function(){
                            //     window.location.href = "/index";
                            // });
                            if(result.errorCode == '200'){
                                layer.msg('文章发布成功，正在跳转至首页', {
                                    title: '友情提示',
                                    icon: 1,
                                    time: 3000 //2秒关闭（如果不配置，默认是3秒）
                                }, function(){
                                    window.location.href=result.data;
                                });
                            }else{
                                layer.msg('文章发布失败，正在刷新当前页', {
                                    title: '友情提示',
                                    icon: 1,
                                    time: 3000 //2秒关闭（如果不配置，默认是3秒）
                                }, function(){
                                    window.location.href=result.data;
                                });
                            }


                        })
                    });
                    btn.find('.layui-layer-btn1').on('click', function() {
                        return;
                    });
                }
            });
        });
        $('body').on('click', '.remove-btn', function() {
            $(this).remove();
        });
        
        $("#articleTitleId").on("keyup",function () {
           var titleHref =  $("#articleTitlehrefId").text();
           var title = $("#articleTitleId").val();
           if(titleHref != title){
               $("#articleTitlehrefId").text(title);
           }

        })
    },
    /**
     * DES加密
     * @param message
     * @param key
     * @returns {*|string}
     */
    encryptByDES: function(message, key) {
        var keyHex = CryptoJS.enc.Utf8.parse(key);
        var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypted.toString();
    }

}