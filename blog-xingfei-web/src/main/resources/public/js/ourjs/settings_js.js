/**
 * Created by yhang on 2017/5/19.
 */
/*用户裁剪头像上传*/
function uploadAvatar() {
    //base64编码图片
    var avatarimg = document.getElementById("userAvatar").src;
    //原始图片url
    var oldAvatarimg = document.getElementById("odlUserAvatar").src;
    if(avatarimg!=null && oldAvatarimg!=null){
        $.ajax({
            type: "POST",
            url: "/personal/avatarImg",
            contentType: "application/json",
            data: JSON.stringify({imgSrc: avatarimg,oldImgSrc: oldAvatarimg}),
            success: function(result){
                if(result.errorCode=="200"){
                    layer.msg('上传，正在拼命上传...', {
                        title: '友情提示',
                        icon: 1,
                        time: 3000 //2秒关闭（如果不配置，默认是3秒）
                    }, function(){
                        window.location.href=result.errorMsg;
                    });
                }
                else{
                    layer.alert(result.errorMsg,{icon: 5});
                }
            }
        });
    }
}
/**
 *触发邮件发送
 */
 $("#send_email").click(function(){
     if(checkEmail()){
         $.ajax({
             type: "GET",
             url: "/personal/sendEmail/"+email,
             contentType: "application/json",
             success: function(result){
                 if(result.errorCode=="200"){
                     layer.msg('请查看您的邮箱完成验证', {
                         title: '友情提示',
                         icon: 1,
                         time: 3000 //2秒关闭（如果不配置，默认是3秒）
                     }, function(){
                         layer.alert(result.errorMsg,{icon: 3});
                     });
                 }
                 else{
                     layer.alert(result.errorMsg,{icon: 5});
                 }
             }
         });
     }
 });
/**
 *验证email是否存在
 */
function checkEmail(){
    var email = $("#email").val();
    //首行/尾行的空格去除
    email = email.replace(/^\s+|\s+$/g, '');
    if(email==null || email==""){
        LayerUtils.checkForTips("请输入邮箱", $("#email"));
        return false;
    }
    var flag = CommonValidate.isEmail(email);
    if(!flag){
        LayerUtils.checkForTips("请输入正确格式的邮箱", $("#email"));
        return false;
    }
    var x = document.getElementById("email").placeholder;
    if(x==email){
        LayerUtils.checkForTips("邮箱未做修改", $("#email"));
        return false;
    }
    $.ajax({
        type: "GET",
        url: "/validate/checkRepetition?email="+email,
        contentType: "application/json",
        success: function(result){
            if(result.errorCode=="E001"){
                layer.alert(result.errorMsg,{icon: 3});
                return true;
            }else{
                return false;
            }
        }
    });
};
/**
 *手机验证输入界面
 */
$("#mobile_button").click(function(obj){
    var user_mobile = $("#user_mobile").val();
    $("#mobile_button").after("<input type='text' class='form-control' id='mobile' placeholder="+user_mobile+" /><a href='javascript:;' class='file' id='send_mobile' onclick='checkMobile()'>发送");
    document.getElementById("mobile_div").removeChild(document.getElementById("mobile_button"));
    document.getElementById("mobile_div").setAttribute("class","basics basics-3")
});

/**
 *触发手机短信验证码发送
 */
function sendMobile(){
    var mobile = $("#mobile").val();
    $.ajax({
            type: "GET",
            url: "/code/sendMessageCode?phone="+mobile,
            contentType: "application/json",
            success: function(result){
                if(result.errorCode=="0"){
                    layer.msg('请查看您的手机验证码完成验证', {
                        title: '友情提示',
                        icon: 1,
                        time: 3000 //2秒关闭（如果不配置，默认是3秒）
                    }, function(){
                        layer.alert(result.errorMsg,{icon: 3});
                        document.getElementById("mobile").setAttribute("style","display:none");
                        document.getElementById("mobile_div").removeChild(document.getElementById("send_mobile"));
                        $("#mobile").after("<input type='text' class='form-control' id='code' placeholder='短信验证码' />");
                        $("#mobile").after("<button class='file' id='bind_mobile' onclick='bindMobile()'>确定</button>");
                    });
                }
                else{
                    layer.alert(result.errorMsg,{icon: 5});
                }
            }
        });
    /*document.getElementById("mobile").setAttribute("style","display:none");
    document.getElementById("mobile_div").removeChild(document.getElementById("send_mobile"));
    $("#mobile").after("<input type='text' class='form-control' id='code' placeholder='短信验证码' />");
    $("#mobile").after("<button class='file' id='bind_mobile' onclick='bindMobile()'>确定</button>");*/
};

/**
 *验证moble是否存在
 */
function checkMobile(){
    var mobile = $("#mobile").val();
    //首行/尾行的空格去除
    mobile = mobile.replace(/^\s+|\s+$/g, '');
    if(mobile==null || mobile==""){
        LayerUtils.checkForTips("请输入手机号", $("#mobile"));
        return false;
    }
    var flag = CommonValidate.isMobile(mobile);
    if(!flag){
        LayerUtils.checkForTips("请输入正确格式的手机号", $("#mobile"));
        return false;
    }
    var x = document.getElementById("mobile").placeholder;
    if(x==mobile){
        LayerUtils.checkForTips("手机号未做修改", $("#mobile"));
        return false;
    }
    $.ajax({
        type: "GET",
        url: "/validate/checkRepetition?phone="+mobile,
        contentType: "application/json",
        success: function(result){
            if(result.errorCode=="200"){
                sendMobile();
            }else{
                layer.alert(result.errorMsg,{icon: 3});
            }
    }});
};

/**
 *验证手机验证码
 */
function bindMobile(){
    var mobile = $("#mobile").val();
    var code = $("#code").val();
    //首行/尾行的空格去除
    code = code.replace(/^\s+|\s+$/g, '');
    if(code==""){
        LayerUtils.checkForTips("请填写验证码", $("#code"));
        return false;
    }
    $.ajax({
        type: "POST",
        url: "/personal/checkMobileCode",
        dataType: "json",
        data: JSON.stringify({phone: mobile,code: code}),
        contentType: "application/json",
        success: function(result){
            if(result.errorCode=="200"){
                layer.alert(result.errorMsg,{icon: 3});
                document.getElementById("mobile").removeAttribute("style");
                $("#mobile").val("");
                document.getElementById("mobile").setAttribute("placeholder",mobile);
                document.getElementById("mobile_div").removeChild(document.getElementById("code"));
                document.getElementById("mobile_div").removeChild(document.getElementById("bind_mobile"));
                $("#mobile").after("<a href='javascript:;' class='file' id='send_mobile' onclick='checkMobile()'>发送");
            }else{
                layer.alert(result.errorMsg,{icon: 3});
            }
        }
    });
}

/**
 *个人资料提交修改js
 */
    $("#info_upload").click(function(){
        //首行/尾行的空格去除
        var username = $("#username").val();
        username = username.replace(/^\s+|\s+$/g, '');
        var x = document.getElementById("username").placeholder;
        if(x==username || username==""){
            //昵称未做修改
            username = null;
        }
        var sex=$('input:radio[name="sex"]:checked').val();
        var birthday_year = $("#sel_year").val();
        var birthday_month = $("#sel_month").val();
        var birthday_day = $("#sel_day").val();
        var description = $("#description").val();
        description = description.replace(/^\s+|\s+$/g, '');
        var x = document.getElementById("description").placeholder;
        if(x==description || description==""){
            //个人接收未做修改
            description = null;
        }
        var json;
        if(username==null && description!=null){
            json={
                "sex":sex,
                "birthday":birthday_year+"-"+birthday_month+"-"+birthday_day,
                "description":description
            }
        }else if(description==null && username!=null){
            json={
                "sex":sex,
                "birthday":birthday_year+"-"+birthday_month+"-"+birthday_day,
                "userName":username
            }
        }else {
            json={
                "sex":sex,
                "birthday":birthday_year+"-"+birthday_month+"-"+birthday_day,
                "username":username,
                "description":description
            }
        }
        $.ajax({
            type: "POST",
            url: "/personal/updateUserInfo",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function(result){
                if(result.errorCode=="200"){
                    layer.alert(result.errorMsg,{icon: 3});
                    /*location.reload();*/
                }else{
                    layer.alert(result.errorMsg,{icon: 3});
                }
            }});
    })

$("#username").blur(function(){
    var username = $("#username").val();
    if(username){
        $.ajax({
            type: "GET",
            url: "/personal/checkUserName?userName="+username,
            contentType: "application/json",
            success: function(result){
                if(result.errorCode=="200"){

                }else if(result.errorCode=="E001"){
                    layer.alert(result.errorMsg,{icon: 3});
                    $("#username").val("");
                }else{
                    layer.alert(result.errorMsg,{icon: 3});
                }
            }});
    }
});