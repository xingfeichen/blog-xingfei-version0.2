/**
 * 邮箱注册
 * Created by 陈兴飞 on 2017/3/18.
 */
var EmailRegister = {

    /*地址管理函数*/
    URL: {
        BASE_URL: $("#basePathId").val(), //获取页面的basePath
        /*获取路径*/
        getURL: function(model,methodUrl){
            return EmailRegister.URL.BASE_URL+"/"+model+"/"+methodUrl;
        }
    },
    toRegisterSuccessPage: function(){
        //第一步登录成功，跳转至第二步
        location.href = PhoneRegister.URL.getURL('register', 'toEmailRegisterSuccessPage')+"?tk_u="+$("#tk_u").val();
    },
    register: {
        email_register_step_one: function () {
            var password = EmailRegister.encryptByDES($("#password").val(), "BLOG-XINGFEI-==");
            //通过post提交数据
            $.post(PhoneRegister.URL.getObjUrl('register', 'toEmailRegisterStepOne'), {
                'userName': $("#username").val(),
                'email': $("#email").val(),
                'authcode': $("#authcode").val(),
                'password': password
            }, function (result) {
                if (result['errorCode'] == '200') {
                    //第一步登录成功，跳转至第二步
                    //第一步登录成功，跳转至第三步
                    location.href = PhoneRegister.URL.getObjUrl('register', 'toEmailRegisterStep2') + "?tk_u=" + result['data'];
                } else {
                    alert(result['errorMsg']);
                }

            })
        },
        /*邮箱注册第四步*/
        email_register_step_four: function () {
            //通过post提交数据
            $.post(PhoneRegister.URL.getObjUrl('register', 'toEmailRegisterStepFour'), {
                'token':$("#tk_u").val(),
                'phone': $("#phone").val(),
                'authcode': $("#authcode").val(),
            }, function (result) {
                if (result['errorCode'] == '200') {
                    //第一步登录成功，跳转至最后一步
                    location.href = PhoneRegister.URL.getObjUrl('register', 'toEmailRegisterSuccessPage')+"?tk_u=" + result['data'];
                } else {
                    alert(result['errorMsg']);
                }

            })
        },
    },
    /*表单校验*/
    validateForm: function () {
        $("#registerEmailStepOneForm").validate({
            submitHandler:function() {
                EmailRegister.register.email_register_step_one();
            },
            rules: {
                username: {
                    required: true,
                    minlength: 2,
                    maxlength: 6,
                    remote: {
                        url: EmailRegister.URL.getURL('validate','checkRepetition'),
                        type: "get",               //数据发送方式
                        dataType: "json",
                        data: {
                            userName: function(){
                                return $("#username").val();
                            }
                        },
                        dataFilter: function (result) {
                            var json = $.parseJSON(result)
                            if(json['errorCode']=='500'){
                                // return "\"" + json['errorMsg'] + "\"";
                                return false;
                            }else{
                                return true;
                            }

                        }
                    }
                },
                email: {
                    required: true,
                    email: true,
                    // remote: {
                    //     url: EmailRegister.URL.getURL('validate','checkRepetition'),
                    //     type: "get",               //数据发送方式
                    //     dataType: "json",
                    //     data: {
                    //         email: function(){
                    //             return $("#email").val();
                    //         }
                    //     },
                    //     dataFilter: function (result) {
                    //         var json = $.parseJSON(result)
                    //         if(json['errorCode']=='500'){
                    //             // return "\"" + json['errorMsg'] + "\"";
                    //             return false;
                    //         }else{
                    //             return true;
                    //         }
                    //
                    //     }
                    // }
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 32
                },
                passwordagain: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                },
                authcode: {
                    required: true,
                },
                agree: "required",
            },
            messages: {
                username: {
                    required: "请输用户名",
                    minlength: "用户名最少2个字符",
                    maxlength: "用户名不可超过6个字符",
                    // remote: "该用户名已存在!"
                },
                email: {
                    required: "请输入邮箱",
                    email: "邮箱不合法"
                },
                password: {
                    required: "请输入密码",
                    minlength: "密码长度不足",
                    maxlength: "密码不能超过32字符"
                },
                passwordagain: {
                    required: "请再次输入密码",
                    equalTo: "两次密码输入不一致",
                },
                authcode: {
                    required: "请输入验证码",
                },
                agree: "请接受我们的声明",
            }
        })
    },
    /*邮箱注册第四步，完善手机号信息*/
    checkFormStep4: function () {
        $("#email-register-add-phone-id").validate({
            errorClass: "error",
            errorElement: "div",
            focusInvalid:false,
            /*表单提交*/
            submitHandler:function() {
                EmailRegister.register.email_register_step_four();
            },
            rules: {
                phone: {
                    required: true,
                    minlength: 11,
                    remote: {
                        url: PhoneRegister.URL.getObjUrl('validate','checkRepetition'),
                        type: "get",               //数据发送方式
                        dataType: "json",
                        data: {
                            phone: function(){
                                return $("#phone").val();
                            }
                        },
                        dataFilter: function (result) {
                            var json = $.parseJSON(result)
                            if(json['errorCode']=='500'){
                                return "\"" + json['errorMsg'] + "\"";
                            }else{
                                return true;
                            }
                        }
                    }
                },
                authcode: {
                    required: true,
                    minlength: 6,
                    maxlength: 6,
                    digits: true,
                    remote: {
                        url: EmailRegister.URL.getURL('validate','checkAuthCode'),
                        type: "get",               //数据发送方式
                        dataType: "json",
                        data: {
                            phone: function(){
                                return $("#phone").val();
                            },
                            authcode: function(){
                                return $("#authcode").val();
                            }
                        },
                        dataFilter: function (result) {
                            var json = $.parseJSON(result)
                            if(json['errorCode']=='500'){
                                return "\"" + json['errorMsg'] + "\"";
                            }else{
                                return true;
                            }
                        }
                    }
                },
            },
            messages: {
                phone: {
                    required: "请输入手机号",
                    minlength: "手机号长度为11位",
                    maxlength: "手机号长度为11位",
                    remote: "该手机号已存在!"
                },
                authcode: {
                    required: "请输入手机验证码",
                    minlength: "验证码为为 6 个数字",
                    maxlength: "验证码为为 6 个数字",
                    digits: "必须为数字",
                    remote: "验证码错误!"
                }
            }
        })
    },
    changeImg: function () {
        var imgSrc = $("#imgObj");
        var src = imgSrc.attr("src");
        imgSrc.attr("src", EmailRegister.chgUrl(src));
    },
    /**
     * 为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
     */
    chgUrl: function (url) {
        var timestamp = (new Date()).valueOf();
        url = url.substring(0, 37);
        if ((url.indexOf("&") >= 0)) {
            url = url + "×tamp=" + timestamp;
        } else {
            url = url + "?timestamp=" + timestamp;
        }
        return url;
    },
    /**
     * DES加密
     * @param message
     * @param key
     * @returns {*|string}
     */
    encryptByDES:function(message, key) {
        var keyHex = CryptoJS.enc.Utf8.parse(key);
        var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypted.toString();
    },
    countDown: function (secs) {
        var jumpTo = document.getElementById('jumpto');
        jumpTo.innerHTML = secs;
        if (--secs > 0) {
            setTimeout("EmailRegister.countDown(" + secs + ")", 1000);
        }
        else {
            location.href = EmailRegister.URL.getURL('login', 'loginByToken') + "?tk_u=" + $("#tk_u").val();
        }
    }
}

