/**
 * 用户注册
 * Created by chenxingfei on 2017/4/23.
 */
var REGIST = {

    /*常量*/
    constants: {
        CUR_COUNT: 60,
        AJAX_USERNAME_CHECK:false,
        AJAX_AUTHCODE_CHECK:false
    },
    /*初始化函数*/
    init: function (obj) {
        //初始化手机验证码按钮
        $(".check_code").css("style", "display:block");
    },
    URL: {
        /*根据参数拼接访问路径*/
        getObjUrl: function (model, method) {
            return "/" + model + "/" + method;
        }
    },
    /*点击注册按钮时的校验*/
    regist_validate: function () {
        var userName = $("#username").val().trim();
        if(CommonValidate.isNull(userName)){
            LayerUtils.checkForTips("用户名不能为空",$("#username"));
            return false;
        }
        var phone = $("#phone").val().trim();
        if(CommonValidate.isNull(phone)){
            LayerUtils.checkForTips("手机号不能为空",$("#phone"));
            return false;
        }
        var check_code = $("#check_code").val().trim();
        if(CommonValidate.isNull(check_code)){
            LayerUtils.checkForTips("验证码不能为空，如果没有收到验证码，请联系我们QQ：1151590116",$("#check_code"));
            return false;
        }
        var password = $("#password").val().trim();
        if(CommonValidate.isNull(password)){
            LayerUtils.checkForTips("密码不能为空",$("#password"));
            return false;
        }
        return true;

    },
    /*注册*/
    regist: function () {
        //校验条件
        if(!REGIST.regist_validate()){
            return;
        }
        /*禁用提交按钮*/
        $("#register-id").attr("disabled", "disabled");
        var password = REGIST.encryptByDES($("#password").val(), "BLOG-XINGFEI-==");
        //通过post提交数据
        $.post(REGIST.URL.getObjUrl('register', 'signUp'), {
            'phone': $("#phone").val(),
            'userName': $("#username").val(),
            'password': password
        }, function (result) {
            if (result['errorCode'] == '200') {

                // layer.confirm('恭喜，注册成功，前往登录？', {
                //     btn: ['好的','不登录了'], //按钮
                //     title: "注册成功",
                //     icon: 1
                // }, function () {
                //     location.href = "/login";
                // })
                //注册成功,自动登录
                location.href = "/login";
            } else {
                layer.msg('注册失败，请重试！', {icon: 5});
            }

        })
    },

    checkPhoneImpl: function (obj) {
        var value = obj.val().trim();
        var message = "";
        var check_flag = false;
        if (CommonValidate.isNull(value)) {
            message = "手机号不能为空";
            check_flag = true;
        }
        else if (!CommonValidate.isMobile(value)) {
            message = "手机号格式不正确";
            check_flag = true;
        }else{
            $(".check_code").show();
        }
        //校验是否重复，手机号不校验重复
        // else {
        //     $.get(REGIST.URL.getObjUrl('validate', 'checkRepetition'), {phone: obj.val()}, function (msg) {
        //         /*用户名重复校验码*/
        //         if (msg.errorCode != '200') {
        //             message = "手机号已经被使用啦";
        //             check_flag = true;
        //         }
        //     })
        // }
        //打印提示信息
        if (check_flag) {
            LayerUtils.checkForTips(message, obj);
        }
    },

    checkUserNameImpl: function (obj) {
        var value = obj.val().trim();
        var message = "";
        var check_flag = false;
        if (CommonValidate.isNull(value)) {
            message = "昵称不能为空";
            check_flag = true;
        }
        else if (value.length < 3) {
            message = "用户名长度不能小于3个字符";
            check_flag = true;
        }
        else if (value.length > 8) {
            message = "用户名长度不能小于8个字符";
            check_flag = true;
        }
        //校验是否重复
        else {
            //只远程校验一次
            if(!REGIST.constants.AJAX_USERNAME_CHECK){
                $.get(REGIST.URL.getObjUrl('validate', 'checkRepetition'), {userName: obj.val()}, function (msg) {
                    /*用户名重复校验码*/
                    if (msg.errorCode != '200') {
                        message = "昵称已经被使用啦";
                        check_flag = true;
                        REGIST.constants.AJAX_USERNAME_CHECK = true;
                        LayerUtils.checkForTips(message, obj);
                    }
                })
            }else{
                message = "昵称已经被使用啦";
                check_flag = true;
            }
        }
        //打印提示信息
        if (check_flag) {
            LayerUtils.checkForTips(message, obj);
        }
    },
    checkAuthCodeImpl: function (obj) {
        var value = obj.val().trim();
        var message = "";
        var check_flag = false;
        if (CommonValidate.isNull(value)) {
            message = "验证码不能为空，如果没有收到验证码，请联系我们QQ：1151590116!";
            check_flag = true;
        }
        else if (!CommonValidate.isDigit(value)) {
            message = "验证码是数字哦！";
            check_flag = true;
        }
        //校验是否重复
        else {
            //只远程校验一次
            if(!REGIST.constants.AJAX_AUTHCODE_CHECK){
                $.get(REGIST.URL.getObjUrl('validate', 'checkAuthCode'), {authcode: obj.val()}, function (msg) {
                    /*用户名重复校验码*/
                    if (msg.errorCode != '200') {
                        message = "验证码不正确！";
                        check_flag = true;
                        REGIST.constants.AJAX_AUTHCODE_CHECK = true;
                    }
                })
            }else{
                message = "验证码不正确";
                check_flag = true;
            }
        }
        //打印提示信息
        if (check_flag) {
            LayerUtils.checkForTips(message, obj);
        }
    },
    checkPasswordImpl: function (obj) {
        var value = obj.val().trim();
        var message = "";
        var check_flag = false;
        if (CommonValidate.isNull(value)) {
            message = "密码不能为空！";
            check_flag = true;
        }
        else if (value.length < 5) {
            message = "密码过于简单可能为丢哦！";
            check_flag = true;
        }
        //打印提示信息
        if (check_flag) {
            LayerUtils.checkForTips(message, obj);
        }
    },
    checkV2: {
        /**
         * 校验用户名
         * @param obj jquery对象
         */
        checkUserName: function (obj) {
            REGIST.checkUserNameImpl(obj);
        },
        /**
         * 校验手机号码
         * @param obj jquery对象
         */
        checkPhone: function (obj) {
            REGIST.checkPhoneImpl(obj);
        },
        /**
         * 校验验证码
         * @param obj jquery对象
         */
        checkAuthCode: function (obj) {
            REGIST.checkAuthCodeImpl(obj);
        },
        /**
         * 校验密码
         * @param obj jquery对象
         */
        checkPassword: function (obj) {
            REGIST.checkPasswordImpl(obj);
        }
    },
    /**
     * DES加密
     * @param message
     * @param key
     * @returns {*|string}
     */
    encryptByDES: function (message, key) {
        var keyHex = CryptoJS.enc.Utf8.parse(key);
        var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypted.toString();
    },
    /*发送短信*/
    sendMessage: function () {
        var phone = $("#phone").val();//手机号码
        if (phone != null && phone != '') {
            //设置button效果，开始计时
            $("#messageCodeBtn").attr("disabled", "true");
            $("#messageCodeBtn").text(REGIST.constants.CUR_COUNT + "秒后重新发送");
            var InterValObj = window.setInterval(REGIST.SetRemainTime, 1000); //启动计时器，1秒执行一次
            //向后台发送处理数据
            $.ajax({
                type: "POST", //用POST方式传输
                dataType: "json", //数据格式:JSON
                url: '/code/sendMessageCode', //目标地址
                data: {'phone': phone},
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
                success: function (msg) {
                    if (msg.errorCode == '1') {
                        layer.msg('抱歉，短信发送失败了！', {icon: 5});
                    } else {
                        layer.msg('短信发送成功，请查收！', {icon: 1})
                    }
                }
            });
        } else if (phone == null || phone == '') {
            layer.msg('手机号不能为空', function(){
                //关闭后的操作
                $("#phone").focus();
            });
        }

    },
    SetRemainTime: function (InterValObj) {
        if (REGIST.constants.CUR_COUNT == 0) {
            window.clearInterval(InterValObj);//停止计时器
            $("#messageCodeBtn").removeAttr("disabled");//启用按钮
            $("#messageCodeBtn").text("重新发送验证码");
        }
        else {
            REGIST.constants.CUR_COUNT--;
            $("#messageCodeBtn").text(REGIST.constants.CUR_COUNT + "秒后重新发送");
        }
    },

}