/**
 * Created by yhang on 2017/4/5.
 */

/**
 * 监听回车键
 * @param event
 */
document.onkeydown=function(event){
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if(e && e.keyCode==13){ // enter 键
        formsubmit();
    }
};

function changeImg() {
    var imgSrc = $("#imgObj");
    var src = imgSrc.attr("src");
    imgSrc.attr("src", chgUrl(src));
}
/**
 * 为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
 */
function chgUrl(url) {
    var timestamp = (new Date()).valueOf();
    url = url.substring(0, 37);
    if ((url.indexOf("&") >= 0)) {
        url = url + "×tamp=" + timestamp;
    } else {
        url = url + "?timestamp=" + timestamp;
    }
    return url;
}
//提交表单
function formsubmit(){
    var phone_code = $("#authcode_phone").val();
    if(phone_code!=null){
        $("#forget_form_phone").submit();
    }
    var email_code = $("#authcode_email").val();
    if(email_code!=null){
        $("#forget_form_email").submit();
    }
}

function sendMessageCode_email(){
    var email = $("#email").val();
    if(email==null || email==""){
        alert("请输入邮箱！");
        return false;
    }
    $.ajax({
        type: "GET",
        url: "/login/sendMessageCodeByEmail",
        dataType: "json",
        data:"email="+email,
        success: function(result){
            if(result.errorCode=="0"){
                //成功
                alert(result.errorMsg);
            }
            else{
                //失败
                alert(result.errorMsg);
            }
        }
    });
}