/**
 * 首页相关数据处理
 * Created by an on 2017/3/26.
 */
var INDEX = {
    /*首页中关于路径的处理*/
    URL:{
        /*根据模块和方法拼接参数*/
        GET_URK_BY_PARAM :function (model,method) {
            return "/"+model+"/"+method;
        }
    },
    /**
     * 加载更多数据
     * @param page 当前页
     */
    loadData : function (obj) {
        var page = $(obj).attr('data-page');
        page = parseInt(page)+1;
        $("#load_more").text("加载中...")
        //通过ajax获取数据动态加载
        $.get(this.URL.GET_URK_BY_PARAM("article", "nextPage"), {'page': page}, function (data) {
            if (data != null && data.length > 0) {
                var elm = '';
                for (var i = 0; i < data.length; i++) {
                    var articleTime = INDEX.formatDate(data[i].articleTime);
                    var collectedFlag = "<a href='#'  onclick='INDEX.iWantCollect("+data[i].id+",this)' >我要收藏</a>";
                    if (data[i].collected) {
                        collectedFlag = "<a href='#' disabled='disabled' >已收藏</a>";
                    }
                    var praisedFlag = '<a class="nice" onclick="INDEX.pariseOrCancelPrise(this)" data-n="nice" data-relativeId="'+data[i].id+'"><img src="img/parise.png"/><span>'+data[i].parisedCount+'</span></a>';
                    if (data[i].praised) {
                        praisedFlag = '<a class="nice" onclick="INDEX.pariseOrCancelPrise(this)" data-n="" data-relativeId="'+data[i].id+'"><img src="img/parised.png"/><span>'+data[i].parisedCount+'</span></a>';
                    }
                    elm += "<li><div><img src='" + data[i].userVO.userInfoVO.avatarUrl + "'/><a href=''>" + data[i].userVO.userInfoVO.userName + "</a><span>" +
                        articleTime + "</span></div><div>" +
                        "<a href='/article/findArticleById/" + data[i].id + "'>" + data[i].articleName + "</a><p>" + data[i].description + "</p>" +
                        "<img src='" + data[i].imgUrl + "'/></div>" +
                        "<div class='clearfix jy-static'>" + collectedFlag +"<a href=''>" + data[i].commentCount + "</a><a href=''>" + data[i].viewCount + "</a>"+praisedFlag+"</div></li>";
                }
                $(".show-area").append(elm);
                $(obj).attr('data-page', page);
                $("#load_more").text("加载更多");
            } else {
                $("#load_more").text("暂时没有更多数据了，请稍后试试");
            }
        })
    },
    /*日期格式化*/
    formatDate: function(strTime) {
        var date = new Date(strTime);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        }
        var day = date.getDate();
        if (day < 10) {
            day = "0" + day;
        }
        var hour = date.getHours();
        if (hour < 10) {
            hour = "0" + hour;
        }
        var min = date.getMinutes();
        if (min < 10) {
            min = "0" + min;
        }
        var second = date.getSeconds();
        if (second < 10) {
            second = "0" + second;
        }
        return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + second;
    },
    /**
     * 收藏或取消收藏
     * @param articleId
     * @param obj
     */
    iWantCollect: function(articleId,obj){
        if( $(obj).text() == '已收藏'){
            layer.msg("已收藏,无需重复操作");
            return;
        }else{
            $.post(INDEX.URL.GET_URK_BY_PARAM("collection", "collect"),{'relativeId': articleId,'collectionType':'ARTICLE'}, function (result) {
                if(result.errorCode=='200'){
                    //此处只设置已收藏并禁用按钮，需要到我收藏的文章列表处取消
                    $(obj).text("已收藏");
                    $(obj).attr("disabled",'disabled');
                    layer.msg("已收藏");
                }else{
                    layer.msg("收藏失败，请稍后重试");
                }
            });
        }
    },
    pariseOrCancelPrise: function(obj){
        var i = $(obj).attr('data-n');
        var relativeId = $(obj).attr('data-relativeId');
        var temObj = $(obj);
        var parseCount = parseInt(temObj.find('span').text());
        if (i == 'nice') {
            $.get("/praise/addOrDelete", {
                'operatorType': '1',
                'type': '1',
                'relativeId': relativeId
            }, function (result) {
                if (result) {
                    console.info("赞操作成功！");
                    temObj.find('img').attr('src', 'img/parised.png');
                    temObj.attr('data-n', '');
                    temObj.find('span').text(parseCount + 1);
                } else {
                    console.info("赞操作失败！");
                }
            })
        } else {
            // 取消点赞
            $.get("/praise/addOrDelete", {
                'operatorType': '0',
                'type': '1',
                'relativeId': relativeId
            }, function (result) {
                if (result) {
                    console.info("取消赞操作成功！");
                    temObj.find('img').attr('src', 'img/parise.png');
                    temObj.attr('data-n', 'nice');
                    if(parseCount - 1 < 0){
                        temObj.find('span').text(0);
                    }else{
                        temObj.find('span').text(parseCount - 1);
                    }
                } else {
                    console.info("取消赞操作失败！");
                }
            })
        }
    }

}
/**
 * 个人中心私有文章调用
 */
var CENTER = {
    /*首页中关于路径的处理*/
    URL:{
        /*根据模块和方法拼接参数*/
        GET_URK_BY_PARAM :function (model,method) {
            return "/"+model+"/"+method;
        }
    },
    /**
     * 加载更多数据
     * @param page 当前页
     */
    loadData : function (obj) {
        var page = $(obj).attr('data-page');
        page = parseInt(page)+1;
        $("#per_load_more").text("加载中...")
        //通过ajax获取数据动态加载
        $.get(this.URL.GET_URK_BY_PARAM("personal","listMyArticles"),{'page' : page},function (data) {
            if (data != null && data.length > 0) {
                var elm = '';
                for (var i = 0; i < data.length; i++) {
                    var articleTime = CENTER.formatDate(data[i].articleTime);
                    elm += "<li><div><img src='"+data[i].userVO.userInfoVO.avatarUrl+"'/><a href=''>"+data[i].userVO.userInfoVO.userName+"</a><span>" +
                        articleTime+"</span></div><div>" +
                        "<a href='/article/findArticleById/"+data[i].id+"'>"+data[i].articleName+"</a><p>"+data[i].description+"</p>" +
                        "<img src='"+data[i].imgUrl+"'/></div>" +
                        "<div class='clearfix'><a href=''>@产品</a><a href=''>"+data[i].collectCount+"</a><a href=''>"+data[i].commentCount+"</a><a href=''>"+data[i].viewCount+"</a></div></li>";
                }
                $(".show-area").append(elm);
                $(obj).attr('data-page',page);
                $("#per_load_more").text("加载更多");
            }else{
                $("#per_load_more").text("暂时没有更多数据了，请稍后试试");
            }
        })
    },
    /*日期格式化*/
    formatDate: function(strTime) {
        var date = new Date(strTime);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        }
        var day = date.getDate();
        if (day < 10) {
            day = "0" + day;
        }
        var hour = date.getHours();
        if (hour < 10) {
            hour = "0" + hour;
        }
        var min = date.getMinutes();
        if (min < 10) {
            min = "0" + min;
        }
        var second = date.getSeconds();
        if (second < 10) {
            second = "0" + second;
        }
        return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + second;
    },

}

$(function($) {
    //邮箱绑定成功提示
    var point= $("#emailPoint").val();
    if(point!=""){
        LayerUtils.moduleMessages(point);
    }
});
