/**
 * 评论相关js
 * Created by chenxingfei on 2017/5/26.
 */
ARTICLE_COMMENT = {

    /**
     * 访问路径拼接
     * @param module
     * @param method
     * @returns {string}
     * @constructor
     */
    URL : function (module,method) {
        return "/"+module+"/"+method;
    },

    /**
     * 发布评论
     */
    publishComment: function (articleId,isLogin) {
        if (!isLogin || isLogin == null || isLogin == undefined) {
            layer.msg("您还没有登录");
            window.location.href = "/login";
            return;
        }
        var commentContent = $("#article-comment-id").val();
        if (CommonValidate.isNotNull(commentContent)) {
            //通过post方法提交评论内容
            $.post(ARTICLE_COMMENT.URL('comment', 'saveComment'), {
                'commitContent': commentContent,
                'relativeId': articleId,
                'type': 0
            }, function (result) {
                if (result) {
                    layer.msg("评论成功");
                    window.location.reload();
                }else{
                    layer.msg("评论失败，请稍后重试");
                }
            })
        } else {
            layer.msg("请填写评论内容!", {shift: 6});
        }

    },
    /**
     * 发布评论回复
     * @param userId 被回复用户id
     * @param commentId 当前评论id
     */
    publishCommentReplay: function (userId, commentId,obj,isLogin) {

        if (!isLogin || isLogin == null || isLogin == undefined) {
            layer.msg("您还没有登录");
            window.location.href = "/login";
            return;
        }
        if (userId == null || userId == undefined || commentId == null || commentId == undefined) {
            layer.msg("评论失败，请尝试刷新页面后重试");
            return;
        }
        var commentContent = $(obj).prev().val();
        if (CommonValidate.isNotNull(commentContent)) {
            //通过post方法提交评论内容
            $.post(ARTICLE_COMMENT.URL('comment', 'saveComment'), {
                'commitContent': commentContent,
                'relativeId': commentId,
                'userId': userId,
                'type': 1
            }, function (result) {
                if (result) {
                    layer.msg("评论成功");
                    window.location.reload();
                } else {
                    layer.msg("评论失败，请稍后重试");
                }
            })
        } else {
            layer.msg("请填写评论内容!", {shift: 6});
        }

    },
    /**
     *  回复文本域控制
     */
    textAreaControl: function (obj,isLogin) {
        if (!isLogin || isLogin == null || isLogin == undefined) {
            layer.msg("您还没有登录");
            window.location.href = "/login";
            return;
        }
        var isActive = $(obj).next("div").attr('is-active');
        if(isActive == 'active'){
            $(obj).next("div").hide();
            $(obj).next("div").attr('is-active','un-active');
        }else{
            $(obj).next("div").show();
            $(obj).next("div").attr('is-active','active');
        }
    }

}
