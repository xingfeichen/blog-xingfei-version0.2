/**
 * Created by yhang on 2017/5/10.
 */

var Chat = {};

Chat.socket = null;

Chat.connect = (function(host) {
    if ('WebSocket' in window) {
        Chat.socket = new WebSocket(host);
    } else if ('MozWebSocket' in window) {
        Chat.socket = new MozWebSocket(host);
    } else {
        return;
    }

    Chat.socket.onopen = function () {
        /*document.getElementById('chat').onkeydown = function(event) {
            if (event.keyCode == 13) {
                Chat.sendMessage();
            }
        };*/
    };

    Chat.socket.onclose = function () {
        LayerUtils.moduleMessages('webcocket关闭.');
    };

    Chat.socket.onmessage = function (message) {
        LayerUtils.moduleMessages(message.data);
    };
});

Chat.initialize = function() {
    if (window.location.protocol == 'http:') {
        Chat.connect('ws://' + "localhost:9999" + '/web');
    } else {
        //加密ws连接
        Chat.connect('wss://' + "localhost:9999" + '/web');
    }
};

Chat.sendMessage = (function() {
    /*var message = document.getElementById('chat').value;
    if (message != '') {
        Chat.socket.send(message);
        document.getElementById('chat').value = '';
    }*/
});

var Console = {};

/*Console.log = (function(message) {
    var console = document.getElementById('console');
    var p = document.createElement('p');
    p.style.wordWrap = 'break-word';
    p.innerHTML = message;
    console.appendChild(p);
    while (console.childNodes.length > 25) {
        console.removeChild(console.firstChild);
    }
    console.scrollTop = console.scrollHeight;
});*/

Chat.initialize();

document.addEventListener("DOMContentLoaded", function() {
    var noscripts = document.getElementsByClassName("noscript");
    for (var i = 0; i < noscripts.length; i++) {
        noscripts[i].parentNode.removeChild(noscripts[i]);
    }
}, false);
