/**
 * 轮播图js操作(异步刷新首页轮播图)
 * Created by xingfei on 2017/7/9.
 */
BANNER = {
    URL: function (module, method) {
        return "/" + module + "/" + method;
    },

    /**
     * 根据类型异步加载轮播图
     * @param type
     */
    listMainBanners: function (type) {
        $.get(BANNER.URL('banner', 'listBanner'), {'type': type}, function (data) {
            if (type == '1') {
                // TODO 暂时不做异步，之后根据需求修改
            }
            else if (type == '2') {
                // TODO 暂时不做异步，之后根据需求修改
            }
        })
    }

}