/**
 * 初始化注册页面相关数据和校验
 * Created by chenxingfei on 2017/4/23.
 */
$(function () {
    /*校验昵称*/
    $("#userName").on("blur",function () {
        LOGIN_VALIDATE.checkLoginName($(this));
    })
    /*校验密码*/
    $("#password").on("blur",function () {
        LOGIN_VALIDATE.checkPassword($(this));
    })

})