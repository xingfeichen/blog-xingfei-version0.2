/**
 * 文章详情页面相关操作
 * Created by chenxingfei on 2017/5/21.
 */
ARTICLE_DETAIL = {

    URL : function (module,method) {
        return "/"+module+"/"+method;
    },
    /**
     * 添加或删除关注
     */
    addOrCancelAttention: function (beAttentionId,attentionId,obj) {
        //校验用户是否登录
        if(attentionId==null){
            layer.msg('您还没有登录，正在拼命去往登录页面，请稍等！', {
                title: '友情提示',
                icon: 5,
                time: 2000 //2秒关闭（如果不配置，默认是3秒）
            }, function(){
                window.location.href="/login";
                return;
            });
        }
        var attentionStatus = $(obj).text();
        var isAttention = true;
        if (attentionStatus == '已关注') {
            isAttention = false;
            /*layer.confirm('您确认取消关注？', {closeBtn:0,title:"提示",
                btn: ['我想好了', '我点错了']
            }, function(){
                Connect("mintq:malrybell");
            }, function(){
                layer.msg('您取消了登录, 如有外呼需求, 请点击屏幕下方的"登录"按钮进行登录!', {
                    time: 10000,
                    btn: ['朕知道了']
                });
            });*/
        }
        $.post(ARTICLE_DETAIL.URL('attention', 'addOrCancelAttention'), {
            'byAttentionId': beAttentionId,
            'attentionId': attentionId,
            'attention': isAttention,
            'isRead': '0'
        }, function (data) {
            if (data != null) {
                if (data.errorCode == '002') {
                    window.location.href = "/login"
                } else {
                    if (isAttention) {
                        $(obj).text("已关注");
                        layer.msg("关注成功")
                    } else {
                        $(obj).text("关注");
                        layer.msg("取消成功")
                    }
                }
            } else {
                window.location.href = "/login";
            }
        })
    }

}