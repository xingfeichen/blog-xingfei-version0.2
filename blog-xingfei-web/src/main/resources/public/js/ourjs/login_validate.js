/**
 * 登录相关校验
 * Created by chenxingfei on 2017/5/7.
 */
LOGIN_VALIDATE = {

    /*校验登录名是否为空*/
    checkLoginName: function (obj) {
        var value = obj.val().trim();
        var message = "";
        var check_flag = false;
        if (CommonValidate.isNull(value)) {
            message = "登录账号不能为空";
            check_flag = true;
        }
        //打印提示信息
        if (check_flag) {
            LayerUtils.checkForTips(message, obj);
        }
    },
    /*校验密码*/
    checkPassword: function (obj) {
        var value = obj.val().trim();
        var message = "";
        var check_flag = false;
        if (CommonValidate.isNull(value)) {
            message = "密码不能为空！";
            check_flag = true;
        }
        else if (value.length < 5) {
            message = "密码过于简单可能为丢哦！";
            check_flag = true;
        }
        //打印提示信息
        if (check_flag) {
            LayerUtils.checkForTips(message, obj);
        }
    },
    /*点击注册按钮时的校验*/
    login_validate: function () {
        var userName = $("#userName").val().trim();
        if(CommonValidate.isNull(userName)){
            LayerUtils.checkForTips("登录账号不能为空",$("#userName"));
            return false;
        }
        var password = $("#password").val().trim();
        if(CommonValidate.isNull(password)){
            LayerUtils.checkForTips("密码不能为空",$("#password"));
            return false;
        }

        return true;

    },

}