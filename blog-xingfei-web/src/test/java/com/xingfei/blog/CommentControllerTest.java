//package com.xingfei.blog;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
///**
// * Created by chenxingfei on 2017/5/21.
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = BlogXingfeiWebApplication.class)
//@WebAppConfiguration
//public class CommentControllerTest {
//
//    private MockMvc mvc;
//    @Autowired
//    WebApplicationContext webApplicationConnect;
//    @Before
//    public void setUp() throws JsonProcessingException {
//        mvc = MockMvcBuilders.webAppContextSetup(webApplicationConnect).build();
//
//    }
//    @Test
//    public void listCommentsByPage(){
//        String expectedResult = "hello world!";
//        String uri = "/comment/listComments?page=1&size=10";
//        MvcResult mvcResult = null;
//        try {
//            mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))
//                    .andReturn();
//            int status = mvcResult.getResponse().getStatus();
//            String content = mvcResult.getResponse().getContentAsString();
//            Assert.assertTrue("错误，正确的返回值为200", status == 200);
//            Assert.assertFalse("错误，正确的返回值为200", status != 200);
//            Assert.assertTrue("数据一致", expectedResult.equals(content));
//            Assert.assertFalse("数据不一致", !expectedResult.equals(content));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//}
