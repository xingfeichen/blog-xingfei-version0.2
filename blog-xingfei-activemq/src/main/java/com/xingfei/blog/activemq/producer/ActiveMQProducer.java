package com.xingfei.blog.activemq.producer;

/**
 * Created by yhang on 2017/5/3.
 */

import com.xingfei.blog.config.ActiveMQQueueConfig;
import com.xingfei.blog.vo.UserVO;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.Topic;
import java.io.Serializable;

@Service
public class ActiveMQProducer {

    /*@Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;*/

    @Autowired
    private Queue queue;

    @Autowired
    private Topic topic;

    @Autowired
    private ActiveMQQueueConfig activeMQQueueConfig;

    /**
     * 发送queue消息
     * @param destinationName
     * @param msg
     */
    public void sendQueueMessage(String destinationName,Serializable msg) {
        JmsTemplate jmsTemplate = activeMQQueueConfig.jmsQueueTemplate();
        ActiveMQDestination destination = ActiveMQDestination.createDestination(ActiveMQDestination.QUEUE_QUALIFIED_PREFIX + destinationName, ActiveMQDestination.QUEUE_TYPE);
        jmsTemplate.convertAndSend(destination,msg);
    }

    /**
     * 发送topic消息
     * @param distinationName 消息名称
     * @param message 消息内容
     */
    public void sendMessage(String distinationName,Object message){
        JmsTemplate jmsTemplate = activeMQQueueConfig.jmsTopicTemplate();
        jmsTemplate.convertAndSend(distinationName,message);
    }


    //send topic. 基于订阅
    public void sendTopic(Serializable msg) {
        JmsTemplate jmsTemplate = activeMQQueueConfig.jmsTopicTemplate();
        jmsTemplate.convertAndSend(topic, msg);
    }
}
