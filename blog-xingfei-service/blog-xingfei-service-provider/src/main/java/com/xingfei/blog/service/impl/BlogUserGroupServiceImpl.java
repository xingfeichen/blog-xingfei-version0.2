package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogUserGroupService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 用户权限接口实现类
 * @author: chenxingfei
 * @time: 2017/2/19  16:25
 * To change this template use File | Settings | File Templates.
 */
@Service("blogUserGroupService")
public class BlogUserGroupServiceImpl implements BlogUserGroupService {
}
