package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogRolePowerService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 角色权限接口实现类
 * @author: chenxingfei
 * @time: 2017/2/19  16:40
 * To change this template use File | Settings | File Templates.
 */
@Service("blogRolePowerService")
public class BlogRolePowerServiceImpl implements BlogRolePowerService {
}
