package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogLeaveMessageService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 博客留言接口实现类
 * @author: chenxingfei
 * @time: 2017/2/19  17:08
 * To change this template use File | Settings | File Templates.
 */
@Service("blogLeaveMessageService")
public class BlogLeaveMessageServiceImpl implements BlogLeaveMessageService {
}
