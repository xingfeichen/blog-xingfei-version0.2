package com.xingfei.blog.annoations;

import java.lang.annotation.*;

/**
 * 自定自定义缓存注解
 * Created by xingfei on 2017/5/22.
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)// class文件和运行时都有，运行时反射获取
@Inherited  //说明子类可以继承父类中的该注解
@Documented //说明该注解将被包含在javadoc中
public @interface MyCacheEvict {

    String value();//cacheName
    String key() default "";//key
    String[] keys() default {};//key数组
    String keyRegex() default "";//key 模糊匹配
    boolean allEntries() default false;//是否清除此cache下所有缓存
}
