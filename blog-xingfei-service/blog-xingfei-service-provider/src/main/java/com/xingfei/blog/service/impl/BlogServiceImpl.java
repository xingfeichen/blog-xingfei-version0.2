package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 博客接口实现类
 * @author: chenxingfei
 * @time: 2017/2/19  17:13
 * To change this template use File | Settings | File Templates.
 */
@Service("blogService")
public class BlogServiceImpl implements BlogService {
}
