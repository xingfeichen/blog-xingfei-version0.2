package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogPhoneMessageService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 手机信息实现类
 * @author: chenxingfei
 * @time: 2017/2/19  17:04
 * To change this template use File | Settings | File Templates.
 */
@Service("blogPhoneMessageService")
public class BlogPhoneMessageServiceImpl implements BlogPhoneMessageService {
}
