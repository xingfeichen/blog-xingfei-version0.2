package com.xingfei.blog.service.impl;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.xingfei.blog.baseMapper.SecretMessageMapper;
import com.xingfei.blog.dto.SecretMessageDTO;
import com.xingfei.blog.model.SecretMessageDO;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.service.BlogSecretMessageService;
import com.xingfei.blog.service.impl.base.BaseService;
import com.xingfei.blog.vo.PageVo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 私信接口实现类
 *
 * @author: chenxingfei
 * @time: 2017/2/19  16:35
 * To change this template use File | Settings | File Templates.
 */
@Service("blogSecretMessageService")
public class BlogSeretMessageServiceImpl extends BaseService implements BlogSecretMessageService {

    @Autowired
    private SecretMessageMapper secretMessageMapper;

    @Override
    @Transactional
    public ServiceResult<SecretMessageDTO> saveSecretMessage(SecretMessageDTO secretMessageDTO) {
        if (secretMessageDTO == null) {
            return returnParamError(null);
        }
        SecretMessageDO secretMessageDO = super.copyObject(secretMessageDTO);
        if (this.secretMessageMapper.insertSelective(secretMessageDO) > 0) {
            secretMessageDTO.setId(secretMessageDO.getId());
            return super.returnSuccessResult(secretMessageDTO);
        } else {
            return super.returnErrorResult();
        }
    }

    @Override
    public ServiceResult<SecretMessageDTO> deleteSecretMessage(SecretMessageDTO secretMessageDTO) {
        if (secretMessageDTO == null) {
            return returnParamError(null);
        }
        SecretMessageDO secretMessageDO = super.copyObject(secretMessageDTO);
        if (this.secretMessageMapper.delete(secretMessageDO) > 0) {
            secretMessageDTO.setId(secretMessageDO.getId());
            return super.returnSuccessResult(secretMessageDTO);
        } else {
            return super.returnErrorResult();
        }
    }

    @Override
    public ServiceResult<SecretMessageDTO> updateSecretMessage(SecretMessageDTO secretMessageDTO) {
        if (secretMessageDTO == null && secretMessageDTO.getId() != null) {
            return returnParamError(null);
        }
        SecretMessageDO secretMessageDO = super.copyObject(secretMessageDTO);
        if (this.secretMessageMapper.updateByPrimaryKeySelective(secretMessageDO) > 0) {
            secretMessageDTO.setId(secretMessageDO.getId());
            return super.returnSuccessResult(secretMessageDTO);
        } else {
            return super.returnErrorResult();
        }
    }

    @Override
    public ServiceResult<List<SecretMessageDTO>> listSecretMessage(PageVo pageVo, SecretMessageDTO secretMessageDTO) {
        if (secretMessageDTO == null && secretMessageDTO.getId() != null) {
            return returnParamError(null);
        }
        SecretMessageDO secretMessageDO = super.copyObject(secretMessageDTO);
        ArrayList<SecretMessageDTO> listSecretMessage = null;
        List<SecretMessageDO> secretMessageDOS = this.secretMessageMapper.selectByRowBounds(secretMessageDO, new RowBounds(pageVo.getStart(), pageVo.getLimit()));
        if (CollectionUtils.isNotEmpty(secretMessageDOS)) {
            listSecretMessage = new ArrayList<>();
            for (SecretMessageDO DO : secretMessageDOS) {
                SecretMessageDTO dto = new SecretMessageDTO();
                BeanUtils.copyProperties(DO, dto);
                listSecretMessage.add(dto);
            }
        }
        return returnSuccessResult(listSecretMessage);
    }

    @Override
    public ServiceResult<List<SecretMessageDTO>> listUnreadSecretMessage(Long id,String status) {
        //通用mapper查询
        SecretMessageDO secretMessageDO = new SecretMessageDO();
        secretMessageDO.setReceiveId(id);
        secretMessageDO.setIsRead(status);
        List<SecretMessageDO> list = secretMessageMapper.select(secretMessageDO);
        List<SecretMessageDTO> lists = null;
        if (CollectionUtils.isNotEmpty(list)) {
            lists = new ArrayList<>();
            for (SecretMessageDO DO : list) {
                SecretMessageDTO dto = new SecretMessageDTO();
                BeanUtils.copyProperties(DO, dto);
                lists.add(dto);
            }
        }
        return returnSuccessResult(lists);
    }
}
