package com.xingfei.blog.consumer.listener;

import com.xingfei.blog.constants.ErrorConstants;
import com.xingfei.blog.constants.RedisConstants;
import com.xingfei.blog.dto.PariseDTO;
import com.xingfei.blog.enums.PariseOperateTypeEnum;
import com.xingfei.blog.enums.PariseTypeEnum;
import com.xingfei.blog.redisCluster.dao.RedisClusterDao;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.service.BlogPariseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

import static com.xingfei.blog.constants.RedisConstants.ARTICLE_IS_PARISED_FOR_USER;

/**
 * @Desc 赞功能监听器
 * ClassName com.xingfei.blog.consumer.listener
 * @Author xingfei
 * @Date 2017/7/8 16:39
 */
public class PariseListener {

    /**
     * 日志
     */
    private static Logger logger = LoggerFactory.getLogger(PariseListener.class);

    private PariseListener() {
    }

    /**
     * 增加赞或删除赞
     *
     * @param pariseDTO
     */
    public static void addOrDeleteParise(PariseDTO pariseDTO, BlogPariseService blogPariseService, RedisClusterDao redisClusterDao) {
        String isParisedForUser = MessageFormat.format(ARTICLE_IS_PARISED_FOR_USER,pariseDTO.getRelativeId(), pariseDTO.getUserId());
        try {
            String articlePariseCountKey = MessageFormat.format(RedisConstants.ARTICLE_PARISE_COUNT, pariseDTO.getRelativeId());
            if (pariseDTO.getOperatorType() == PariseOperateTypeEnum.PARISE_SURE.ordinal()) {
                //查看是否已经点过赞（此处如此处理原因：因为会多次接收到消息）
                if (redisClusterDao.isExist(isParisedForUser)) {
                    logger.info("===============>>>>>>>>>>>>赞数据重复{}<<<<<<<<<<<==================", pariseDTO.toString());
                    //如果已经点过赞，不再重复操作
                    return;
                }
                ServiceResult<PariseDTO> serviceResult = blogPariseService.saveParise(pariseDTO);
                if (serviceResult != null && serviceResult.getCode() == ErrorConstants.RES_SUCCESS) {
                    pariseDTO = serviceResult.getResult();
                    redisClusterDao.set(isParisedForUser, pariseDTO.getId().toString(), 0L);
                    //如果赞文章
                    if(pariseDTO.getType() == PariseTypeEnum.PARISE_ARTICLE.ordinal()){
                        // 赞数量+1
                        if (redisClusterDao.isExist(articlePariseCountKey)) {
                            redisClusterDao.incr(articlePariseCountKey);
                        }
                    }
                }
            } else if (pariseDTO.getOperatorType() == PariseOperateTypeEnum.PARISE_CANCEL.ordinal()) {
                ServiceResult<PariseDTO> serviceResult = blogPariseService.removeParise(pariseDTO);
                if (serviceResult != null && serviceResult.getCode() == ErrorConstants.RES_SUCCESS) {
                    redisClusterDao.deleteByKey(isParisedForUser);
                    //如果赞文章
                    if(pariseDTO.getType() == PariseTypeEnum.PARISE_ARTICLE.ordinal()) {
                        if (redisClusterDao.isExist(articlePariseCountKey)) {
                            redisClusterDao.decr(articlePariseCountKey);
                        }
                    }
                }
            } else {
                logger.info("===============>>>>>>>>>>>>赞或取消赞失败{}<<<<<<<<<<<==================", pariseDTO.toString());
            }
        } catch (Exception e) {
            logger.error("===============>>>>>>>>>>>>赞或取消赞数据转换异常{}<<<<<<<<<<<==================", e.toString());
        }
    }
}
