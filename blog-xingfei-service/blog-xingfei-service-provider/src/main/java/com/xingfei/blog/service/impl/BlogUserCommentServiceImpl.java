package com.xingfei.blog.service.impl;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.xingfei.blog.baseMapper.UserCommentMapper;
import com.xingfei.blog.dto.UserCommentDTO;
import com.xingfei.blog.enums.CommentTypeEnum;
import com.xingfei.blog.model.UserCommentDO;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.service.BlogUserCommentService;
import com.xingfei.blog.service.impl.base.BaseService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: chenxingfei
 * @time: 2017/2/19  16:28
 * To change this template use File | Settings | File Templates.
 */
@Service("blogUserCommentService")
public class BlogUserCommentServiceImpl extends BaseService implements BlogUserCommentService {

    @Autowired
    private UserCommentMapper userCommentMapper;

    /**
     * 日志
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(BlogUserCommentServiceImpl.class);

    @Override
    @Transactional
    public ServiceResult<UserCommentDTO> saveComment(UserCommentDTO userCommentDTO) {
        if (userCommentDTO == null) {
            super.returnParamError(userCommentDTO);
        }
        UserCommentDO userCommentDO = getDOfromDTO(userCommentDTO);
        if (userCommentDO != null) {
            int selective = userCommentMapper.insertSelective(userCommentDO);
            if (selective > 0) {
                LOGGER.info(">>>>>>>>>>>>>>>>>>>>保存评论成功<<<<<<<<<<<<<<<<<<<<");
                userCommentDO.setId(userCommentDO.getId());
                return super.returnSuccessResult(userCommentDO);
            }
        }
        LOGGER.info(">>>>>>>>>>>>>>>>>>>>保存评论失败，返回null<<<<<<<<<<<<<<<<<<<<");
        return super.returnSuccessResult(null);
    }

    @Override
    @Transactional
    public ServiceResult<UserCommentDTO> removeComment(UserCommentDTO userCommentDTO) {
        if (userCommentDTO == null) {
            super.returnParamError(userCommentDTO);
        }
        UserCommentDO userCommentDO = getDOfromDTO(userCommentDTO);
        if (userCommentDO != null) {
            int delete = userCommentMapper.delete(userCommentDO);
            if (delete > 0) {
                LOGGER.info(">>>>>>>>>>>>>>>>>>>>评论删除成功<<<<<<<<<<<<<<<<<<<<");
                return super.returnSuccessResult(userCommentDO);
            }
        }
        LOGGER.info(">>>>>>>>>>>>>>>>>>>>评论删除失败，返回null<<<<<<<<<<<<<<<<<<<<");
        return super.returnSuccessResult(null);
    }

    @Override
    @Transactional
    public ServiceResult<UserCommentDTO> updateComment(UserCommentDTO userCommentDTO) {
        if (userCommentDTO == null) {
            super.returnParamError(userCommentDTO);
        }
        UserCommentDO userCommentDO = getDOfromDTO(userCommentDTO);
        if (userCommentDO != null) {
            if (userCommentMapper.updateByPrimaryKeySelective(userCommentDO) > 0) {
                LOGGER.info(">>>>>>>>>>>>>>>>>>>>评论修改成功<<<<<<<<<<<<<<<<<<<<");
                return super.returnSuccessResult(userCommentDO);
            }
        }
        LOGGER.info(">>>>>>>>>>>>>>>>>>>>评论修改失败，返回null<<<<<<<<<<<<<<<<<<<<");
        return super.returnSuccessResult(null);
    }

    @Override
    public ServiceResult<List<UserCommentDTO>> listCommentsByPage(UserCommentDTO userCommentDTO, Integer page, Integer size) {
        if (userCommentDTO == null) {
            super.returnParamError(userCommentDTO);
        }
        UserCommentDO userCommentDO = getDOfromDTO(userCommentDTO);
        if (userCommentDO != null) {
            List<UserCommentDO> selectByRowBounds = userCommentMapper.selectByRowBounds(userCommentDO, new RowBounds((page - 1) * size, size));
            if (CollectionUtils.isNotEmpty(selectByRowBounds)) {
                LOGGER.info(">>>>>>>>>>>>>>>>>>>>查询评论列表数据，总共{}条<<<<<<<<<<<<<<<<<<<<", selectByRowBounds.size());

                return super.returnSuccessResult(listDTOfromlistDO(selectByRowBounds));
            }
        }
        LOGGER.info(">>>>>>>>>>>>>>>>>>>>未查询到数据，返回null<<<<<<<<<<<<<<<<<<<<");
        return super.returnSuccessResult(null);
    }

    @Override
    public ServiceResult<UserCommentDTO> getCommentById(Long id) {
        if (id == null) {
            super.returnParamError(id);
        }
        UserCommentDO userCommentDO = userCommentMapper.selectByPrimaryKey(id);
        if (userCommentDO != null) {
            UserCommentDTO userCommentDTO = getDTOfromDO(userCommentDO);
            LOGGER.info(">>>>>>>>>>>>>>>>>>>>根据id查询评论成功数据，userCommentDTO={}<<<<<<<<<<<<<<<<<<<<", userCommentDTO);
            super.returnSuccessResult(userCommentDTO);
        }
        LOGGER.info(">>>>>>>>>>>>>>>>>>>>未查询到数据，返回null，未查询到数据id={}<<<<<<<<<<<<<<<<<<<<", id);
        return super.returnSuccessResult(null);
    }

    /**
     * dto转换为do
     *
     * @param userCommentDTO
     * @return
     */
    private UserCommentDO getDOfromDTO(UserCommentDTO userCommentDTO) {
        UserCommentDO userCommentDO = new UserCommentDO();
        BeanUtils.copyProperties(userCommentDTO, userCommentDO);
        return userCommentDO;
    }

    /**
     * do转换为dto
     *
     * @param userCommentDO
     * @return
     */
    private UserCommentDTO getDTOfromDO(UserCommentDO userCommentDO) {
        UserCommentDTO userCommentDTO = new UserCommentDTO();
        BeanUtils.copyProperties(userCommentDO, userCommentDTO);
        return userCommentDTO;
    }

    /**
     * 根据list<DO> => list<DTO>
     *
     * @param userCommentDOS
     * @return
     */
    private List<UserCommentDTO> listDTOfromlistDO(List<UserCommentDO> userCommentDOS) {
        if (CollectionUtils.isNotEmpty(userCommentDOS)) {
            List<UserCommentDTO> dtoList = new ArrayList<>();
            userCommentDOS.forEach(action -> dtoList.add(getDTOfromDO(action)));
                for (UserCommentDTO action : dtoList) {
                    LOGGER.info(">>>>>>>>>>>>>>>>>>>>获取当前评论的所有评论回复，当前id={}<<<<<<<<<<<<<<<<<<<<", action.getId());
                    if (action.getType() != null && action.getType().intValue() != CommentTypeEnum.COMMENT_REPLAY.ordinal()) {
                        UserCommentDO  userCommentDO = new UserCommentDO();
                        userCommentDO.setRelativeId(action.getId());
                        List<UserCommentDO> reviews = userCommentMapper.select(userCommentDO);
                        if(CollectionUtils.isNotEmpty(reviews)){
                            List<UserCommentDTO> reviewdtos =  new ArrayList<>();
                            for (UserCommentDO Do: reviews) {
                                UserCommentDTO commentDTO = getDTOfromDO(Do);
                                reviewdtos.add(commentDTO);
                            }
                            action.setReviewDTOs(reviewdtos);
                        }
                    }
                }
            return dtoList;
        }
        return null;
    }

}
