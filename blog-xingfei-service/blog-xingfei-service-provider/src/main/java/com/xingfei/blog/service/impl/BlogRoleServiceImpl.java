package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogRoleService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 角色相关接口实现类
 * @author: chenxingfei
 * @time: 2017/2/19  16:42
 * To change this template use File | Settings | File Templates.
 */
@Service("blogRoleService")
public class BlogRoleServiceImpl implements  BlogRoleService {
}
