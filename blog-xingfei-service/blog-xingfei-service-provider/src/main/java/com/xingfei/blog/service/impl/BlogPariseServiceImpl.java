package com.xingfei.blog.service.impl;

import com.xingfei.blog.baseMapper.PariseMapper;
import com.xingfei.blog.dto.PariseDTO;
import com.xingfei.blog.model.PariseDO;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.service.BlogPariseService;
import com.xingfei.blog.service.impl.base.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 赞服务实现类
 * Created by chenxingfei on 2017/6/15.
 */
@Service("blogPariseService")
@Transactional
public class BlogPariseServiceImpl extends BaseService implements BlogPariseService {

    /**
     * 赞mapper
     */
    @Autowired
    private PariseMapper pariseMapper;
    /**
     * 日志
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(BlogPariseServiceImpl.class);

    @Override
    public ServiceResult<PariseDTO> saveParise(PariseDTO pariseDTO) {
        if (pariseDTO == null) {
            super.returnParamError(pariseDTO);
        }
        PariseDO pariseDO = getDOfromDTO(pariseDTO);
        if (pariseDO != null) {
            int selective = pariseMapper.insertSelective(pariseDO);
            if (selective > 0) {
                LOGGER.info(">>>>>>>>>>>>>>>>>>>>保存赞成功<<<<<<<<<<<<<<<<<<<<");
                pariseDTO.setId(pariseDO.getId());
                return super.returnSuccessResult(pariseDTO);
            }
        }
        LOGGER.info(">>>>>>>>>>>>>>>>>>>>保存赞失败，返回null<<<<<<<<<<<<<<<<<<<<");
        return super.returnSuccessResult(null);
    }

    @Override
    public ServiceResult<PariseDTO> removeParise(PariseDTO pariseDTO) {
        if (pariseDTO == null) {
            super.returnParamError(pariseDTO);
        }
        PariseDO pariseDO = getDOfromDTO(pariseDTO);
        if (pariseDO != null) {
            int selective = pariseMapper.delete(pariseDO);
            if (selective > 0) {
                LOGGER.info(">>>>>>>>>>>>>>>>>>>>删除赞成功<<<<<<<<<<<<<<<<<<<<");
                pariseDO.setId(pariseDO.getId());
                return super.returnSuccessResult(pariseDTO);
            }
        }
        LOGGER.info(">>>>>>>>>>>>>>>>>>>>删除赞失败，返回null<<<<<<<<<<<<<<<<<<<<");
        return super.returnSuccessResult(null);
    }

    /**
     * dto转换为do
     *
     * @param pariseDTO
     * @return
     */
    private PariseDO getDOfromDTO(PariseDTO pariseDTO) {
        PariseDO pariseDO = new PariseDO();
        BeanUtils.copyProperties(pariseDTO, pariseDO);
        return pariseDO;
    }

    /**
     * do转换为dto
     *
     * @param pariseDO
     * @return
     */
    private PariseDTO getDTOfromDO(PariseDO pariseDO) {
        PariseDTO pariseDTO = new PariseDTO();
        BeanUtils.copyProperties(pariseDO, pariseDTO);
        return pariseDTO;
    }

}
