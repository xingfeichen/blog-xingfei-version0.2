package com.xingfei.blog.service.impl;

import com.xingfei.blog.baseMapper.CollectionMapper;
import com.xingfei.blog.constants.ErrorConstants;
import com.xingfei.blog.dto.CollectionDTO;
import com.xingfei.blog.model.CollectionDO;
import com.xingfei.blog.result.ResultConstructor;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.service.BlogCollectionService;
import com.xingfei.blog.service.impl.base.BaseService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 收藏dubbo服务实现类
 *
 * @author chenxingfei
 * @create 2017-07-02 15:41
 **/
@Service("blogCollectionService")
public class BlogCollectionServiceImpl extends BaseService implements BlogCollectionService {

    @Autowired
    private CollectionMapper collectionMapper;

    /**
     * 日志
     */
    private static Logger logger = LoggerFactory.getLogger(BlogCollectionServiceImpl.class);

    @Override
    public ServiceResult<CollectionDTO> saveCollection(CollectionDTO collectionDTO) {
        if (collectionDTO == null) {
            return ResultConstructor.buildServiceErrResult(ErrorConstants.RES_SERVICE_STRING, "条件错误");
        }
        CollectionDO collectionDO = getDOfromDTO(collectionDTO);

        int i = this.collectionMapper.insertSelective(collectionDO);
        if (i > 0) {
            collectionDTO = getDTOfromDO(collectionDO);
            return ResultConstructor.buildSuccessResult(collectionDTO, ErrorConstants.RES_SUCCESS, "操作成功");
        }
        return ResultConstructor.buildSuccessResult(null, ErrorConstants.RES_SUCCESS, "操作失败");
    }

    @Override
    public ServiceResult<CollectionDTO> getCollectionById(Long collectionId) {
        if (collectionId == null) {
            return ResultConstructor.buildServiceErrResult(ErrorConstants.RES_SERVICE_STRING, "条件错误");
        }

        CollectionDO collectionDO = this.collectionMapper.selectByPrimaryKey(collectionId);
        if (collectionDO != null) {
            CollectionDTO collectionDTO = getDTOfromDO(collectionDO);
            return ResultConstructor.buildSuccessResult(collectionDTO, ErrorConstants.RES_SUCCESS, "查询成功");
        }
        return ResultConstructor.buildSuccessResult(null, ErrorConstants.RES_SUCCESS, "查询失败");
    }

    @Override
    public ServiceResult<Boolean> deleteCollectionById(Long collectionId) {
        if (collectionId == null) {
            return ResultConstructor.buildServiceErrResult(ErrorConstants.RES_SERVICE_STRING, "删除错误");
        }
        int i = this.collectionMapper.deleteByPrimaryKey(collectionId);
        return ResultConstructor.buildSuccessResult(i, ErrorConstants.RES_SUCCESS, "删除成功");
    }

    @Override
    public ServiceResult<List<CollectionDTO>> listCollectionByCondition(CollectionDTO collectionDTO) {
        if (collectionDTO == null) {
            return ResultConstructor.buildServiceErrResult(ErrorConstants.RES_SERVICE_STRING, "条件错误");
        }
        CollectionDO collectionDO = getDOfromDTO(collectionDTO);
        if (collectionDO != null) {
            List<CollectionDO> collectionDOList = this.collectionMapper.select(collectionDO);
            return getListServiceResult(collectionDOList);
        }
        return ResultConstructor.buildSuccessResult(null, ErrorConstants.RES_SUCCESS, "查询失败");
    }

    @Override
    public ServiceResult<List<CollectionDTO>> listCollectionByConditionAndPage(CollectionDTO collectionDTO,Integer offset,Integer size) {
        if (collectionDTO == null) {
            return ResultConstructor.buildServiceErrResult(ErrorConstants.RES_SERVICE_STRING, "条件错误");
        }
        CollectionDO collectionDO = getDOfromDTO(collectionDTO);
        if (collectionDO != null) {
            List<CollectionDO> collectionDOList = this.collectionMapper.selectByRowBounds(collectionDO,new RowBounds((offset-1)*size,size));
            ServiceResult<List<CollectionDTO>> collectionDTOS = getListServiceResult(collectionDOList);
            if (collectionDTOS != null) return collectionDTOS;
        }
        return ResultConstructor.buildSuccessResult(null, ErrorConstants.RES_SUCCESS, "查询失败");
    }

    /**
     * 返回列表数据构建
     * @param collectionDOList
     * @return
     */
    private ServiceResult<List<CollectionDTO>> getListServiceResult(List<CollectionDO> collectionDOList) {
        if (CollectionUtils.isEmpty(collectionDOList)) {
            List<CollectionDTO> collectionDTOS = new ArrayList<>();
            collectionDOList.forEach(action -> {
                CollectionDTO dto = getDTOfromDO(action);
                collectionDTOS.add(dto);
            });
            return ResultConstructor.buildSuccessResult(collectionDTOS, ErrorConstants.RES_SUCCESS, "查询成功");
        }
        return null;
    }

    /**
     * dto转换为do
     *
     * @param collectionDTO
     * @return
     */
    private CollectionDO getDOfromDTO(CollectionDTO collectionDTO) {
        CollectionDO collectionDO = new CollectionDO();
        BeanUtils.copyProperties(collectionDTO, collectionDO);
        return collectionDO;
    }

    /**
     * do转换为dto
     *
     * @param collectionDO
     * @return
     */
    private CollectionDTO getDTOfromDO(CollectionDO collectionDO) {
        CollectionDTO collectionDTO = new CollectionDTO();
        BeanUtils.copyProperties(collectionDO, collectionDTO);
        return collectionDTO;
    }
}
