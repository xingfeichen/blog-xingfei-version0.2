package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogPowerService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 权限接口实现类
 * @author: chenxingfei
 * @time: 2017/2/19  16:45
 * To change this template use File | Settings | File Templates.
 */
@Service("blogPowerService")
public class BlogPowerServiceImpl implements BlogPowerService {
}
