package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogArticleCategoryService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 文章目录接口实现类
 * @author: chenxingfei
 * @time: 2017/2/19  17:16
 * To change this template use File | Settings | File Templates.
 */
@Service("blogArticleCategoryService")
public class BlogArticleCategoryServiceImpl implements BlogArticleCategoryService {
}
