package com.xingfei.blog.service.impl;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.xingfei.blog.baseMapper.AttentionMapper;
import com.xingfei.blog.baseMapper.SecretMessageMapper;
import com.xingfei.blog.baseMapper.UserSystemMsgMapper;
import com.xingfei.blog.dto.SecretMessageDTO;
import com.xingfei.blog.dto.UserSystemMsgDTO;
import com.xingfei.blog.model.SecretMessageDO;
import com.xingfei.blog.model.UserAttentionDO;
import com.xingfei.blog.model.UserSystemMsgDO;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.service.BlogMessageService;
import com.xingfei.blog.service.impl.base.BaseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yhang on 2017/5/8.
 */
@Service("blogMessageService")
public class BlogMessageServiceImpl extends BaseService implements BlogMessageService {

    @Autowired
    private SecretMessageMapper secretMessageMapper;

    @Autowired
    private UserSystemMsgMapper userSystemMsgMapper;

    @Autowired
    private AttentionMapper attentionMapper;

    @Override
    public ServiceResult<Map<String, Object>> listUnreadMessage(Long id, String status) {
        //通用mapper查询 私信
        SecretMessageDO secretMessageDO = new SecretMessageDO();
        secretMessageDO.setReceiveId(id);
        secretMessageDO.setIsRead(status);
        List<SecretMessageDO> list = secretMessageMapper.select(secretMessageDO);
        List<SecretMessageDTO> lists = null;
        if (CollectionUtils.isNotEmpty(list)) {
            lists = new ArrayList<SecretMessageDTO>();
            for (SecretMessageDO DO : list) {
                SecretMessageDTO dto = new SecretMessageDTO();
                BeanUtils.copyProperties(DO, dto);
                lists.add(dto);
            }
        }
        //未读系统消息
        List<UserSystemMsgDO> userSystemMsgDOs = userSystemMsgMapper.listUnreadSysMsg(id);
        List<UserSystemMsgDTO> lists2 = null;
        if (CollectionUtils.isNotEmpty(userSystemMsgDOs)) {
            lists2 = new ArrayList<UserSystemMsgDTO>();
            for (UserSystemMsgDO DO : userSystemMsgDOs) {
                UserSystemMsgDTO dto = new UserSystemMsgDTO();
                BeanUtils.copyProperties(DO, dto);
                lists2.add(dto);
            }
        }
        //未读新增关注
        UserAttentionDO userAttentionDO = new UserAttentionDO();
        userAttentionDO.setByAttentionId(id);
        userAttentionDO.setIsRead(status);
        int count = attentionMapper.selectCount(userAttentionDO);
        Map<String, Object> map = new HashMap<String,Object>();
        map.put("secret_msg",lists);
        map.put("sys_msg",lists2);
        map.put("attention_count",count);
        return returnSuccessResult(map);
    }
}
