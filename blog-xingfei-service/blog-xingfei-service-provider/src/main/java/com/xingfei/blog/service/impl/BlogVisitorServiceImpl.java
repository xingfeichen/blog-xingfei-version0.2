package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogVisitorService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 访客表
 * @author: chenxingfei
 * @time: 2017/2/19  16:21
 * To change this template use File | Settings | File Templates.
 */
@Service("blogVisitorService")
public class BlogVisitorServiceImpl implements BlogVisitorService {
}
