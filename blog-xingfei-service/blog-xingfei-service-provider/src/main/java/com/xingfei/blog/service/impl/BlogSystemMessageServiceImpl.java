package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogSystemMessageService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 系统消息接口实现类
 * @author: chenxingfei
 * @time: 2017/2/19  16:31
 * To change this template use File | Settings | File Templates.
 */
@Service("blogSystemMessageService")
public class BlogSystemMessageServiceImpl implements BlogSystemMessageService {
}
