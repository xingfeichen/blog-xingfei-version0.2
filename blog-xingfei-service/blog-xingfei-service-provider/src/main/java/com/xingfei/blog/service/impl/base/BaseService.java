package com.xingfei.blog.service.impl.base;

import com.xingfei.blog.constants.ErrorConstants;
import com.xingfei.blog.dto.SecretMessageDTO;
import com.xingfei.blog.model.SecretMessageDO;
import com.xingfei.blog.result.ResultConstructor;
import com.xingfei.blog.result.ServiceResult;
import org.springframework.beans.BeanUtils;

/**
 * Created with IntelliJ IDEA.
 * 用于抽取service中公共部分
 * @author: chenxingfei
 * @time: 2017/4/4  8:23
 * To change this template use File | Settings | File Templates.
 */
public class BaseService {

    /**
     * 返回参数错误,此方法要求传递的对象需要实现toString方法
     * @param param
     * @return
     */
    protected ServiceResult returnParamError(Object param){
        if(param!=null){
            return ResultConstructor.buildSuccessResult(null, ErrorConstants.RES_SERVICE, "参数错误："+param.toString());
        }
        return ResultConstructor.buildSuccessResult(null, ErrorConstants.RES_SERVICE, "参数错误！");
    }
    /**
     * 返回成功data
     * @param param
     * @return
     */
    protected ServiceResult returnSuccessResult(Object param){
        return ResultConstructor.buildSuccessResult(param);
    }
    /**
     * 返回成功data
     * @return
     */
    protected ServiceResult returnErrorResult(){
        return ResultConstructor.buildServiceErrResult(ErrorConstants.SERVER_ERROR,"服务错误");
    }
    /**
     * 返回服务器异常
     * @return
     */
    protected ServiceResult returnRuntimeExeceptionResult(){
        return ResultConstructor.buildRunTimeErrResult("服务器错误");
    }

    /**
     * 数据传输对象和数据库对象转换
     * @param secretMessageDTO
     * @return
     */
    protected SecretMessageDO copyObject(SecretMessageDTO secretMessageDTO){
        SecretMessageDO secretMessageDO = new SecretMessageDO();
        BeanUtils.copyProperties(secretMessageDTO,secretMessageDO);
        return secretMessageDO;
    }

}
