package com.xingfei.blog.consumer.listener;

import com.xingfei.blog.dto.SecretMessageDTO;
import com.xingfei.blog.service.BlogSecretMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

/**
 * @Desc 私信功能监听器
 * ClassName com.xingfei.blog.consumer.listener
 * @Author xingfei
 * @Date 2017/7/8 16:39
 * Created by xingfei on 2017/7/8.
 */
@Component
public class SecretMessageListener {

    public SecretMessageListener() {
    }

    /**
     * 日志
     */
    private static Logger logger = LoggerFactory.getLogger(SecretMessageListener.class);

    /**
     * 通过MQ保存私信
     * @param message
     * @param blogSecretMessageService
     */
    public static void saveSecretMessage(Message message,BlogSecretMessageService blogSecretMessageService) {
        SecretMessageDTO   messageDTO = null;
        try {
            messageDTO = (SecretMessageDTO) ((ObjectMessage) message).getObject();
            if(messageDTO!=null){
                //保存消息
                blogSecretMessageService.saveSecretMessage(messageDTO);
            }
        } catch (JMSException e) {
            logger.error("===============>>>>>>>>>>>>发送私信异常{}<<<<<<<<<<<==================", e.toString());
            e.printStackTrace();
        }
    }
}
