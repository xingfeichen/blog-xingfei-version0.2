package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogMoodService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 心情接口实现类
 * @author: chenxingfei
 * @time: 2017/2/19  17:06
 * To change this template use File | Settings | File Templates.
 */
@Service("blogMoodService")
public class BlogMoodServiceImpl implements BlogMoodService {
}
