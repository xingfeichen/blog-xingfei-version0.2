package com.xingfei.blog.service.impl;

import com.xingfei.blog.baseMapper.AttentionMapper;
import com.xingfei.blog.constants.ErrorConstants;
import com.xingfei.blog.dto.UserAttentionDTO;
import com.xingfei.blog.model.UserAttentionDO;
import com.xingfei.blog.result.ResultConstructor;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.service.BlogUserAttentionService;
import com.xingfei.blog.service.impl.base.BaseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 用户关注接口实现类
 *
 * @author: chenxingfei
 * @time: 2017/2/19  16:29
 * To change this template use File | Settings | File Templates.
 */
@Service("blogUserAttentionService")
public class BlogUserAttentionServiceImpl extends BaseService implements BlogUserAttentionService {

    @Autowired
    private AttentionMapper attentionMapper;

    @Override
    @Transactional
    public ServiceResult<UserAttentionDTO> saveAttention(UserAttentionDTO userAttentionDTO) {
        if (userAttentionDTO == null) {
            return super.returnParamError(null);
        }
        UserAttentionDO userAttentionDO = new UserAttentionDO();
        BeanUtils.copyProperties(userAttentionDTO, userAttentionDO);
        if (attentionMapper.insertSelective(userAttentionDO) > 0) {
            userAttentionDO.setId(userAttentionDO.getId());
            return returnSuccessResult(userAttentionDO);
        } else {
            return returnErrorResult();
        }
    }

    @Override
    public ServiceResult<UserAttentionDTO> getAttentionByAttentionIdAndBeAttentionId(UserAttentionDTO userAttentionDTO) {
        if (userAttentionDTO == null) {
            return super.returnParamError(null);
        }
        UserAttentionDO userAttentionDO = new UserAttentionDO();
        BeanUtils.copyProperties(userAttentionDTO,userAttentionDO);
        List<UserAttentionDO> attentionDOS = this.attentionMapper.select(userAttentionDO);
        if (attentionDOS!=null && attentionDOS.size() == 1) {
            //已关注，返回dto
            return super.returnSuccessResult(userAttentionDO);
        } else {
            return super.returnErrorResult();
        }
    }

    @Override
    public ServiceResult<UserAttentionDTO> removeAttention(UserAttentionDTO userAttentionDTO) {
        if (userAttentionDTO == null) {
            return super.returnParamError(null);
        }
        UserAttentionDO userAttentionDO = new UserAttentionDO();
        userAttentionDO.setAttentionId(userAttentionDTO.getAttentionId());
        userAttentionDO.setByAttentionId(userAttentionDTO.getByAttentionId());
        if (this.attentionMapper.delete(userAttentionDO) > 0) {
            return ResultConstructor.buildSuccessResult(userAttentionDTO);
        } else {
            return ResultConstructor.buildSuccessResult(null, ErrorConstants.RES_SERVICE, "删除失败");
        }
    }

    @Override
    public ServiceResult<Map<String, Integer>> getAttentionsByUserId(Long userId) {
        if (userId == null) {
            return ResultConstructor.buildSuccessResult(null, ErrorConstants.RES_SERVICE, "参数错误");
        }
        Map<String, Integer> integerMap = attentionMapper.getAttentionsByUserId(userId);
        if (integerMap != null) {
            return ResultConstructor.buildSuccessResult(integerMap);
        } else {
            return ResultConstructor.buildSuccessResult(null, ErrorConstants.RES_SERVICE, "为查询到数据");
        }
    }
}
