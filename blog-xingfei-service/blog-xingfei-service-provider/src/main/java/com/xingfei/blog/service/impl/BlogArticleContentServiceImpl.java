package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogArticleContentService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 博客内容实现类
 * @author: chenxingfei
 * @time: 2017/2/19  17:15
 * To change this template use File | Settings | File Templates.
 */
@Service("blogArticleContentService")
public class BlogArticleContentServiceImpl implements BlogArticleContentService {
}
