package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogFriendlyLinkService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 友好链接接口实现类
 * @author: chenxingfei
 * @time: 2017/2/19  17:10
 * To change this template use File | Settings | File Templates.
 */
@Service("blogFriendlyLinkService")
public class BlogFriendlyLinkServiceImpl implements BlogFriendlyLinkService {
}
