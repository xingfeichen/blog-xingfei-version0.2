package com.xingfei.blog.service.impl;

import com.xingfei.blog.baseMapper.BannerMapper;
import com.xingfei.blog.constants.ErrorConstants;
import com.xingfei.blog.dto.BannerDTO;
import com.xingfei.blog.model.BannerDO;
import com.xingfei.blog.result.ResultConstructor;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.service.BlogBannerService;
import com.xingfei.blog.service.impl.base.BaseService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Desc 轮播图dubbo服务实现类
 * ClassName com.xingfei.blog.service.impl
 * @Author xingfei
 * @Date 2017/7/9 20:57
 */
@Service("blogBannerService")
public class BlogBannerServiceImpl extends BaseService implements BlogBannerService {

    @Autowired
    private BannerMapper bannerMapper;

    @Override
    public ServiceResult<List<BannerDTO>> listBanners(BannerDTO bannerDTO) {
        if (bannerDTO == null) {
            return ResultConstructor.buildServiceErrResult(ErrorConstants.RES_SERVICE_STRING, "条件错误");
        }

        BannerDO dOfromDTO = getDOfromDTO(bannerDTO);
        List<BannerDO> bannerDOS = this.bannerMapper.select(dOfromDTO);
        if (CollectionUtils.isNotEmpty(bannerDOS)) {
            List<BannerDTO> dtOfromListDTO = getListDTOfromListDO(bannerDOS);
            return ResultConstructor.buildSuccessResult(dtOfromListDTO, ErrorConstants.RES_SUCCESS, "查询成功");
        }
        return ResultConstructor.buildSuccessResult(null, ErrorConstants.RES_SUCCESS, "查询失败");
    }

    @Override
    public ServiceResult<BannerDTO> getBannerById(String bannerId) {
        if (StringUtils.isBlank(bannerId)) {
            return ResultConstructor.buildServiceErrResult(ErrorConstants.RES_SERVICE_STRING, "条件错误");
        }

        BannerDO bannerDO = this.bannerMapper.selectByPrimaryKey(bannerId);
        if (bannerDO != null) {
            BannerDTO bannerDTO = getDTOfromDO(bannerDO);
            return ResultConstructor.buildSuccessResult(bannerDTO, ErrorConstants.RES_SUCCESS, "查询成功");
        }
        return ResultConstructor.buildSuccessResult(null, ErrorConstants.RES_SUCCESS, "查询失败");
    }

    /**
     * dto转换为do
     *
     * @param bannerDTO
     * @return
     */
    private BannerDO getDOfromDTO(BannerDTO bannerDTO) {
        BannerDO bannerDO = new BannerDO();
        BeanUtils.copyProperties(bannerDTO, bannerDO);
        return bannerDO;
    }

    /**
     * do转换为dto
     *
     * @param bannerDO
     * @return
     */
    private BannerDTO getDTOfromDO(BannerDO bannerDO) {
        BannerDTO bannerDTO = new BannerDTO();
        BeanUtils.copyProperties(bannerDO, bannerDTO);
        return bannerDTO;
    }

    /**
     * list<do>转换为list<dto>
     *
     * @param bannerDOList
     * @return
     */
    private List<BannerDTO> getListDTOfromListDO(List<BannerDO> bannerDOList) {
        if (CollectionUtils.isNotEmpty(bannerDOList)) {
            List<BannerDTO> bannerDTOList = new ArrayList<>();
            bannerDOList.forEach(action -> {
                BannerDTO bannerDTO = new BannerDTO();
                BeanUtils.copyProperties(action, bannerDTO);
                bannerDTOList.add(bannerDTO);

            });
            return bannerDTOList;
        }
        return null;
    }
}
