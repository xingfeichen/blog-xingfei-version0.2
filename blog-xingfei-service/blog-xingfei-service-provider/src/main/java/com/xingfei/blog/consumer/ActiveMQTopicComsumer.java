package com.xingfei.blog.consumer;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

/**
 * @Desc MQ的topic类型数据消费者
 * ClassName com.xingfei.blog.consumer.listener
 * @Author xingfei
 * @Date 2017/7/8 17:55
 */
@Service
public class ActiveMQTopicComsumer {

    @JmsListener(destination = "sample.queue")
    public void testConsumer(String msg) {
        System.out.println("=========================开始消费==========================");
        System.out.println(msg);
        System.out.println("==========================消费完毕=========================");
    }

    @JmsListener(destination = "simpleTest")
    public void testConsumer1(String msg) {
        System.out.println("=========================开始消费1==========================");
        System.out.println(msg);
        System.out.println("==========================消费完毕1=========================");
    }
}
