package com.xingfei.blog.consumer;

/**
 * Created by yhang on 2017/5/10.
 */

import com.xingfei.blog.consumer.listener.PariseListener;
import com.xingfei.blog.consumer.listener.SecretMessageListener;
import com.xingfei.blog.dto.PariseDTO;
import com.xingfei.blog.enums.PariseOperateTypeEnum;
import com.xingfei.blog.enums.SecretMessageTypeEnum;
import com.xingfei.blog.redisCluster.dao.RedisClusterDao;
import com.xingfei.blog.service.BlogPariseService;
import com.xingfei.blog.service.BlogSecretMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.*;

/**
 * @Desc dubbo服务端queue消息消费端
 * ClassName com.xingfei.blog.consumer.listener
 * @Author xingfei
 * @Date 2017/7/8 16:39
 * Created by xingfei on 2017/7/8.
 */
@Configuration
public class ActiveMQQueueConsumer {

    @Autowired
    private BlogPariseService blogPariseService;

    @Autowired
    private RedisClusterDao redisClusterDao;

    @Autowired
    BlogSecretMessageService blogSecretMessageService;

    @Autowired
    private Connection connection;

    /**
     * 日志
     */
    private static Logger logger = LoggerFactory.getLogger(ActiveMQQueueConsumer.class);

    /**
     * 赞操作消费者
     *
     * @return
     * @throws JMSException
     */
    @Bean
    public MessageConsumer initQueueMessageForParise() {

        MessageConsumer consumer = null;
        Session session = null;
        try {
            connection.start();
            // 创建session
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            // 创建destination
            Queue queue = session.createQueue(PariseOperateTypeEnum.PARISE_SURE.toString());
            consumer = session.createConsumer(queue);
            consumer.setMessageListener(message -> {
                PariseDTO pariseDTO = null;
                try {
                    pariseDTO = (PariseDTO) ((ObjectMessage) message).getObject();
                    PariseListener.addOrDeleteParise(pariseDTO, blogPariseService, redisClusterDao);
                } catch (JMSException e) {
                    logger.error("===============>>>>>>>>>>>>赞或取消赞数据转换异常{}<<<<<<<<<<<==================", e.toString());
                }
            });
        } catch (Exception e) {
            logger.error("===============>>>>>>>>>>>>操作异常{}<<<<<<<<<<<==================", e.toString());
        } finally {
            // FIXME: 2017/7/8 不确定是否应该关闭连接和session
//            closeConsumer(consumer, session);
        }
        return consumer;
    }


    /**
     * 私信操作消费者
     *
     * @return
     */
    @Bean
    public MessageConsumer initQueueMessageForSecretMessage() {

        MessageConsumer consumer = null;
        Session session = null;
        try {
            connection.start();
            // 创建session
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            // 创建destination
            Queue queue = session.createQueue(SecretMessageTypeEnum.SECRET_MESSAGE.toString());
            consumer = session.createConsumer(queue);
            consumer.setMessageListener(message -> SecretMessageListener.saveSecretMessage(message, blogSecretMessageService));
        } catch (Exception e) {
            logger.error("===============>>>>>>>>>>>>操作异常{}<<<<<<<<<<<==================", e.toString());
        } finally {
            // FIXME: 2017/7/8 不确定是否应该关闭连接和session
//            closeConsumer(consumer, session);
        }
        return consumer;
    }

    /**
     * 关闭消费者
     * @param consumer 当前消费者
     * @param session 当前会话
     */
    private void closeConsumer(MessageConsumer consumer, Session session) {
        try {
            if (session != null) {
                session.close();
            }
            if(consumer!=null){
                consumer.close();
            }
        } catch (JMSException e) {
            logger.error("===============>>>>>>>>>>>>关闭session失败{}<<<<<<<<<<<==================", e.toString());
        }
    }
}

