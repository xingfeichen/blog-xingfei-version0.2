package com.xingfei.blog.service.impl;

import com.xingfei.blog.service.BlogFriendService;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * 朋友相关操作接口实现类
 * @author: chenxingfei
 * @time: 2017/2/19  17:12
 * To change this template use File | Settings | File Templates.
 */
@Service("blogFriendService")
public class BlogFriendServiceImpl implements BlogFriendService {
}
