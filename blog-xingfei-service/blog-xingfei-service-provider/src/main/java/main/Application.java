package main;

import org.apache.activemq.command.ActiveMQQueue;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.jms.Queue;
import javax.sql.DataSource;
import java.util.concurrent.CountDownLatch;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.xingfei.blog")
@EnableCaching
@EnableJms
@ImportResource("classpath:dubbo-provide.xml")
@MapperScan(basePackages = {"com.xingfei.blog.mapper","com.xingfei.blog.baseMapper"})
@EnableTransactionManagement
@SpringBootApplication
public class Application {

    /**
     * 日志
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    @Bean
    public PlatformTransactionManager txManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
    @Bean
    public CountDownLatch closeLatch() {
        return new CountDownLatch(1);
    }

    @Bean
    public Queue logQueue() {
        return new ActiveMQQueue("sample.queue");
    }

    /**
     * dubbo服务启动主线程
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(Application.class, args);
        LOGGER.info("====================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>DUBBO服务启动成功<<<<<<<<<<<<<<<<<<<<<<<<<<<<=================");
//        ApplicationContext ctx = new SpringApplicationBuilder()
//                .sources(Application.class)
//                .web(false)
//                .run(args);
//        //让线程保持阻塞
//        CountDownLatch closeLatch = ctx.getBean(CountDownLatch.class);
//        closeLatch.await();
    }
}
