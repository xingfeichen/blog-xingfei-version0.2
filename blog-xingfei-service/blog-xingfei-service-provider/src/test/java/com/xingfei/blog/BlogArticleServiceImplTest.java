//package com.xingfei.blog;
//
//import com.xingfei.blog.dto.ArticleDTO;
//import com.xingfei.blog.result.ServiceResult;
//import BlogArticleService;
//import main.Application;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Ignore;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.util.List;
//
///**
// * BlogArticleServiceImpl Tester.
// *
// * @author <Authors name>
// * @since <pre>03/27/2017</pre>
// * @version 1.0
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
//public class BlogArticleServiceImplTest {
//    @Autowired
//    private BlogArticleService blogArticleService;
//
//    @Before
//    public void before() throws Exception {
//    }
//
//    @After
//    public void after() throws Exception {
//    }
//
//    /**
//     *
//     * Method: getUserById(Long userId)
//     *
//     */
//    @Test
//    @Ignore
//    public void testListByPage() throws Exception {
//        ArticleDTO articleDO = new ArticleDTO();
//        ServiceResult<List<ArticleDTO>> listByPage = blogArticleService.listByPage(articleDO, 0, 10);
//        if(listByPage!=null){
//            System.out.println("==============================");
//            System.out.println(listByPage.getResult());
//        }
//
//    }
//    /**
//     *
//     * Method: getUserById(Long userId)
//     *
//     */
//    @Test
//    public void testListByPageNew() throws Exception {
//        ArticleDTO articleDTO = new ArticleDTO();
//        ServiceResult<List<ArticleDTO>> listByPage = blogArticleService.listByPageNew(articleDTO, 0, 10);
//        if(listByPage!=null){
//            System.out.println("==============================");
//            System.out.println(listByPage.getResult());
//        }
//
//    }
//}
