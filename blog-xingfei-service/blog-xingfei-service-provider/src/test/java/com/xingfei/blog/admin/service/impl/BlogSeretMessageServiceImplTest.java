//package com.xingfei.blog.service.impl;
//
//import com.xingfei.blog.dto.SecretMessageDTO;
//import com.xingfei.blog.result.ServiceResult;
//import BlogSecretMessageService;
//import com.xingfei.blog.vo.PageVo;
//import main.Application;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.util.List;
//
///**
// * BlogSeretMessageServiceImpl Tester.
// *
// * @author chenxingfei
// * @version 1.0
// * @since 04/04/2017
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
//public class BlogSeretMessageServiceImplTest {
//
//    @Autowired
//    private BlogSecretMessageService blogSecretMessageService;
//
//    @Before
//    public void before() throws Exception {
//    }
//
//    @After
//    public void after() throws Exception {
//    }
//
//    /**
//     * Method: saveSecretMessage(SecretMessageDTO secretMessageDTO)
//     */
//    @Test
//    public void testSaveSecretMessage() throws Exception {
//        SecretMessageDTO secretMessageDTO = new SecretMessageDTO();
//        secretMessageDTO.setMessageContent("你你你你你你iinininin");
//        secretMessageDTO.setReceiveId(69l);
//        secretMessageDTO.setSendId(54l);
//        secretMessageDTO.setMessageTopic("你你你");
//        ServiceResult<SecretMessageDTO> secretMessage = blogSecretMessageService.saveSecretMessage(secretMessageDTO);
//        print(secretMessage);
//    }
//
//    /**
//     * Method: deleteSecretMessage(SecretMessageDTO secretMessageDTO)
//     */
//    @Test
//    public void testDeleteSecretMessage() throws Exception {
//        //TODO: Test goes here...
//    }
//
//    /**
//     * Method: updateSecretMessage(SecretMessageDTO secretMessageDTO)
//     */
//    @Test
//    public void testUpdateSecretMessage() throws Exception {
//        //TODO: Test goes here...
//    }
//
//    /**
//     * Method: listSecretMessage(PageVo pageVo, SecretMessageDTO secretMessageDTO)
//     */
//    @Test
//    public void testListSecretMessage() throws Exception {
//        PageVo pageVO = new PageVo();
//        pageVO.setStart(0);
//        pageVO.setLimit(10);
//        SecretMessageDTO secretMessageDTO = new SecretMessageDTO();
//        ServiceResult<List<SecretMessageDTO>> secretMessage =
//                this.blogSecretMessageService.listSecretMessage(pageVO, secretMessageDTO);
//        print(secretMessage);
//    }
//
//    /**
//     * 测试结果输出
//     * @param serviceResult
//     */
//    public void print(ServiceResult serviceResult ){
//        System.out.println("---------------"+serviceResult.getCode()+"---------------");
//        System.out.println("---------------"+serviceResult.getResult()+"---------------");
//    }
//
//
//}
