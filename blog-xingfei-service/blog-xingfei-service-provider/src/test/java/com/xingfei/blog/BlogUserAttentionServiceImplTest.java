//package com.xingfei.blog;
//
//import com.xingfei.blog.dto.UserAttentionDTO;
//import com.xingfei.blog.model.UserAttentionDO;
//import com.xingfei.blog.result.ServiceResult;
//import BlogUserAttentionService;
//import main.Application;
//import org.junit.Ignore;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.util.Map;
//
///**
// * BlogUserAttentionServiceImpl Tester.
// *
// * @author <Authors name>
// * @since <pre>04/02/2017</pre>
// * @version 1.0
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
//public class BlogUserAttentionServiceImplTest {
//
//    @Autowired
//    private BlogUserAttentionService blogUserAttentionService;
//
//    @Test
//    public void testSaveAttention(){
//        UserAttentionDTO userAttentionDTO = new UserAttentionDTO();
//        userAttentionDTO.setAttentionId(69l);
//        userAttentionDTO.setByAttentionId(31l);
//        ServiceResult<UserAttentionDTO> userAttentionDTOServiceResult =
//                blogUserAttentionService.saveAttention(userAttentionDTO);
//        System.out.println(userAttentionDTOServiceResult.getCode());
//        System.out.println(userAttentionDTOServiceResult.getResult());
//        System.out.println(userAttentionDTOServiceResult.getResult().getId());
//    }
//    @Test
//    @Ignore
//    public void testGetAttentionsByUserId(){
//        ServiceResult<Map<String, Integer>> serviceResult = this.blogUserAttentionService.getAttentionsByUserId((long) 69);
//        if(serviceResult!=null){
//            System.out.println(serviceResult.getCode());
//            System.out.println(serviceResult.getResult());
//        }
//    }
//}
