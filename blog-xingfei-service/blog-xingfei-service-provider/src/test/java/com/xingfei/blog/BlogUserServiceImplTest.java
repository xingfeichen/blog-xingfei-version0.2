//package com.xingfei.blog;
//
//import com.xingfei.blog.dto.UserDTO;
//import com.xingfei.blog.result.ServiceResult;
//import BlogUserService;
//import main.Application;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//
//import java.util.Date;
//
///**
// * Created by xingfei on 2017/3/14.
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
//@WebAppConfiguration
//public class BlogUserServiceImplTest {
//
//    @Autowired
//    private BlogUserService blogUserService;
//
//    @Before
//    public void before() throws Exception {
//    }
//
//    @After
//    public void after() throws Exception {
//    }
//
//    /**
//     *
//     * Method: findUserById(Long userId)
//     * author:chenxingfei
//     *
//     */
//    @Test
//    public void testFindUserById() throws Exception {
//        ServiceResult<UserDTO> userById = this.blogUserService.getUserById((long) 69);
//        System.out.println("==========="+userById.getCode()+"===========");
//        System.out.println("==========="+userById.getResult()+"===========");
//    }
//
//    /**
//     *
//     * Method: getUserById(Long userId)
//     *
//     */
//    @Test
//    public void testGetUserById() throws Exception {
//        //TODO: Test goes here...
//    }
//
//    /**
//     *
//     * Method: saveUser(UserDTO userDTO)
//     *
//     */
//    @Test
//    public void testSaveUser() throws Exception {
//        UserDTO userDTO = new UserDTO();
//        userDTO.setPassword("123456");
//        userDTO.setEmail("fsd@163.com");
//        userDTO.setCreateTime(new Date());
//        ServiceResult<Integer> integerServiceResult = this.blogUserService.saveUser(userDTO);
//
//        System.out.println(integerServiceResult.getResult());
//    }
//
//    /**
//     *
//     * Method: saveUserToAutoProductUserInfo(UserDTO userDTO)
//     *
//     */
//    @Test
//    public void testSaveUserToAutoProductUserInfo() throws Exception {
//        //TODO: Test goes here...
//    }
//
//    /**
//     *
//     * Method: findUserByEmail(String email)
//     *
//     */
//    @Test
//    public void testFindUserByEmail() throws Exception {
//        //TODO: Test goes here...
//    }
//
//    /**
//     *
//     * Method: findUserByPhone(String phone)
//     *
//     */
//    @Test
//    public void testFindUserByPhone() throws Exception {
//        UserDTO userDTO = new UserDTO();
////        userDTO.setPhone("18613892254");
//        ServiceResult<UserDTO> userByPhone = this.blogUserService.findUserByPhone("18613892254");
//
//
//    }
//
//    /**
//     *
//     * Method: findUserIdByWeiboUid(Long weiboUid)
//     *
//     */
//    @Test
//    public void testFindUserIdByWeiboUid() throws Exception {
//        //TODO: Test goes here...
//    }
//}
