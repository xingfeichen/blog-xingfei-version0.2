package com.xingfei.blog.service;

import com.xingfei.blog.dto.UserCommentDTO;
import com.xingfei.blog.result.ServiceResult;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: chenxingfei
 * @time: 2017/2/19  16:28
 * To change this template use File | Settings | File Templates.
 */
public interface BlogUserCommentService {

    /**
     * 增加评论
     *
     * @param userCommentDTO
     * @return
     */
    ServiceResult<UserCommentDTO> saveComment(UserCommentDTO userCommentDTO);

    /**
     * 删除评论
     *
     * @param userCommentDTO
     * @return
     */
    ServiceResult<UserCommentDTO> removeComment(UserCommentDTO userCommentDTO);

    /**
     * 修改评论
     *
     * @param userCommentDTO
     * @return
     */
    ServiceResult<UserCommentDTO> updateComment(UserCommentDTO userCommentDTO);

    /**
     * 分页获取评论
     *
     * @param userCommentDTO
     * @param page
     * @param size
     * @return
     */
    ServiceResult<List<UserCommentDTO>> listCommentsByPage(UserCommentDTO userCommentDTO, Integer page, Integer size);

    /**
     * 根据id获取评论
     *
     * @param id
     * @return
     */
    ServiceResult<UserCommentDTO> getCommentById(Long id);


}
