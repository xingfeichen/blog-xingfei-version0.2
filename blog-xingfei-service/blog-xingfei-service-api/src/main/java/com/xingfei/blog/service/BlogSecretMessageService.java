package com.xingfei.blog.service;

import com.xingfei.blog.dto.SecretMessageDTO;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.vo.PageVo;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 私信接口
 *
 * @author: chenxingfei
 * @time: 2017/2/19  16:35
 * To change this template use File | Settings | File Templates.
 */
public interface BlogSecretMessageService {

    /**
     * 添加私信
     *
     * @param secretMessageDTO
     * @return
     */
    public ServiceResult<SecretMessageDTO> saveSecretMessage(SecretMessageDTO secretMessageDTO);

    /**
     * 删除私信
     *
     * @param secretMessageDTO
     * @return
     */
    public ServiceResult<SecretMessageDTO> deleteSecretMessage(SecretMessageDTO secretMessageDTO);

    /**
     * 修改私信(根据主键更新)
     *
     * @param secretMessageDTO
     * @return
     */
    public ServiceResult<SecretMessageDTO> updateSecretMessage(SecretMessageDTO secretMessageDTO);

    /**
     * 获取私信列表
     *
     * @param secretMessageDTO
     * @return
     */
    public ServiceResult<List<SecretMessageDTO>> listSecretMessage(PageVo pageVo, SecretMessageDTO secretMessageDTO);

    /**
     * 获取未读消息列表
     * @param id
     * @return
     */
    public ServiceResult<List<SecretMessageDTO>> listUnreadSecretMessage(Long id,String status);
}
