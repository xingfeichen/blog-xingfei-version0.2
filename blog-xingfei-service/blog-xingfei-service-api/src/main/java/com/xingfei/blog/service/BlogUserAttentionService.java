package com.xingfei.blog.service;

import com.xingfei.blog.dto.UserAttentionDTO;
import com.xingfei.blog.result.ServiceResult;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * 用户关注接口
 *
 * @author: chenxingfei
 * @time: 2017/2/19  16:28
 * To change this template use File | Settings | File Templates.
 */
public interface BlogUserAttentionService {

    /**
     * 保存关注数据
     *
     * @param userAttentionDTO
     * @return
     */
    public ServiceResult<UserAttentionDTO> saveAttention(UserAttentionDTO userAttentionDTO);

    /**
     * 是否关注（根据关注id和被关注id获取唯一数据，判断是否关注）
     *
     * @param userAttentionDTO
     * @return
     */
    public ServiceResult<UserAttentionDTO> getAttentionByAttentionIdAndBeAttentionId(UserAttentionDTO userAttentionDTO);

    /**
     * 删除关注
     *
     * @param userAttentionDTO
     * @return
     */
    public ServiceResult<UserAttentionDTO> removeAttention(UserAttentionDTO userAttentionDTO);

    /**
     * 保存关注数据
     *
     * @param userId
     * @return
     */
    public ServiceResult<Map<String, Integer>> getAttentionsByUserId(Long userId);
}
