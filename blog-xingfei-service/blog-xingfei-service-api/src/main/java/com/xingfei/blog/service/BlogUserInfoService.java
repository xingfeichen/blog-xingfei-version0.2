package com.xingfei.blog.service;

import com.xingfei.blog.dto.UserInfoDTO;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.vo.PageVo;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 用户信息接口实现类（dubbo服务）
 * @author: chenxingfei
 * @time: 2017/1/19  11:57
 * To change this template use File | Settings | File Templates.
 */
public interface BlogUserInfoService {

    /**
     * 根据用户id获取用户信息
     * @param id
     * @return
     */
   public ServiceResult<UserInfoDTO> getUserInfoById(Long id);

    /**
     * 根据dto添加用户信息
     * @param userInfoDTO
     * @return
     */
   public ServiceResult<UserInfoDTO> saveUserInfoByDTO(UserInfoDTO userInfoDTO);

    /**
     * 根据用户id删除用户信息
     * @param userId
     * @return
     */
   public ServiceResult<Boolean> removeUserInfoByUserId(Long userId);

    /**
     * 根据dto更新用户信息不为null的字段
     * @param userInfoDTO
     * @return
     */
   public ServiceResult<Boolean> updateUserInfoByDTO(UserInfoDTO userInfoDTO);

    /**
     * 根据dto获取指定条件下的用户信息条数
     * @param userInfoDTO
     * @return
     */
   public ServiceResult<Integer> countUserInfoByDTO(UserInfoDTO userInfoDTO);

    /**
     * 根据dto获取用户信息列表
     * @param userInfoDTO
     * @return
     */
   public ServiceResult<List<UserInfoDTO>> listUserInfoByDTO(PageVo pageVo,UserInfoDTO userInfoDTO);

    /**
     * 根据dto分页获取用户信息列表
     * @return
     */
   public ServiceResult<List<UserInfoDTO>> listUserInfoByDTO(UserInfoDTO userInfoDTO);

    /**
     * 获取推荐作者列表
     * @return
     */
    ServiceResult<List<UserInfoDTO>> listRecommendAuthor();

    /**
     * 验证用户名是否存在
     * @param userName
     * @return
     */
    ServiceResult<Integer> checkUserName(String userName);

}
