package com.xingfei.blog.service;

import com.xingfei.blog.dto.UserDTO;
import com.xingfei.blog.dto.UserInfoDTO;
import com.xingfei.blog.result.ServiceResult;

/**
 * Created by xingfei on 2017/1/16.
 */
 public interface BlogUserService {

    /**
     * 根据用户id查询用户
     *
     * @param userId
     * @return
     */
     ServiceResult<UserDTO> findUserById(Long userId);
    /**
     * 根据用户id查询用户并携带回用户详情
     *
     * @param userId
     * @return
     */
     ServiceResult<UserDTO> getUserById(Long userId);



    /**
     * 添加用户（用户注册）
     *
     * @return
     */
     ServiceResult<Integer> saveUser(UserDTO userDTO);

    /**
     * 添加用户（用户注册并生成用户详细信息基本信息）
     *
     * @return
     */
     ServiceResult<UserDTO> saveUserToAutoProductUserInfo(UserDTO userDTO);

    /**
     * 根据邮箱查询用户（邮箱唯一）
     *
     * @param email
     * @return
     */
     ServiceResult<UserDTO> findUserByEmail(String email);

    /**
     * 根据手机号查询用户（手机号唯一）
     *
     * @param phone
     * @return
     */
     ServiceResult<UserDTO> findUserByPhone(String phone);

    /**
     * 根据weibo_uid查找用户userinfo
     */
     ServiceResult<Long> getUserInfoByWeiboUid(String weiboUid);

    /**
     * 用户注册，同时生成用户详情
     *
     * @param userDTO
     * @param userInfoDTO
     * @return
     */
     ServiceResult<Long> saveUserAndSaveUserInfo(UserDTO userDTO, UserInfoDTO userInfoDTO);


    /**
     * 修改用户信息
     * @param userDTO
     * @param userInfoDTO
     * @return
     */
    ServiceResult<Integer> updateUserAndUserInfo(UserDTO userDTO, UserInfoDTO userInfoDTO);

    /**
     * 根据qq openID 获取用户info
     * @param openID
     * @return
     */
     ServiceResult<Long> getUserInfoByOpenId(String openID);

    /**
     * 根据邮箱获取用户信息并绑定qq
     * @param userName
     * @param webToken
     * @return
     */
     ServiceResult<UserDTO> getUserInfoByEmailAndBindQQOrWeiBo(String userName, String webToken,String profix);

    /**
     * 根据手机号获取用户信息并绑定qq
     * @param userName
     * @param webToken
     * @return
     */
     ServiceResult<UserDTO> getUserInfoByPhoneAndBindQQOrWeiBo(String userName, String webToken,String profix);

    /**
     * qq/weibo 注册用户接口
     * @param userDTO
     * @return
     */
     ServiceResult<UserDTO> thirdRegistAndSaveUserInfo(UserInfoDTO userDTO);

    /**
     * 根据邮箱更新密码
     * @param username
     * @return
     */
     ServiceResult<Integer> updateUserByEmial(String username,String password);

    /**
     * 根据手机号更新密码
     * @param username
     * @return
     */
     ServiceResult<Integer> updateUserByPhone(String username,String password);

    /**
     * 根据用户id获取userInfo
     * @param userId
     * @return
     */
    ServiceResult<UserInfoDTO> getUserInfoDTOByUserId(Long userId);

}
