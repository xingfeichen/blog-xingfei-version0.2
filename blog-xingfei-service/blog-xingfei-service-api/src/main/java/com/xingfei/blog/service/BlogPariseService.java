package com.xingfei.blog.service;

import com.xingfei.blog.dto.PariseDTO;
import com.xingfei.blog.result.ServiceResult;

/**
 * 赞service
 * Created by chenxingfei on 2017/6/15.
 */
public interface BlogPariseService {

    /**
     * 增加赞
     *
     * @param pariseDTO
     * @return
     */
    ServiceResult<PariseDTO> saveParise(PariseDTO pariseDTO);

    /**
     * 删除赞
     *
     * @param pariseDTO
     * @return
     */
    ServiceResult<PariseDTO> removeParise(PariseDTO pariseDTO);

}
