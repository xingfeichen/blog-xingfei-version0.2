package com.xingfei.blog.service;

import com.xingfei.blog.result.ServiceResult;

import java.util.Map;

/**
 * Created by yhang on 2017/5/8.
 */
public interface BlogMessageService {

    public ServiceResult<Map<String, Object>> listUnreadMessage(Long id, String s);
}
