package com.xingfei.blog.service;

import com.xingfei.blog.dto.ArticleDTO;
import com.xingfei.blog.dto.CollectionDTO;
import com.xingfei.blog.result.ServiceResult;

import java.util.List;

/**
* Created by Administrator on 2017/2/18.
* 文章接口
*/
public interface BlogArticleService {

    /**
     * 增加文章
     * @param articleDTO
     * @return
     */
    ServiceResult<ArticleDTO> saveArticle(ArticleDTO articleDTO);
    /**
     * 根据文章id查询文章
     * @param articleId
     * @return
     */
    ServiceResult<ArticleDTO> getArticleById(Long articleId);

    /**
     * 根据文章id获取文章发布用户
     * @param articleId
     * @return
     */
    ServiceResult<Long> getPublisherIdByArticleId(Long articleId);

    /**
     * 查询文章列表
     * @param articleDTO
     * @return
     */
    ServiceResult<List<ArticleDTO>> listByPage(ArticleDTO articleDTO,Integer pageNum,Integer pageSize);

    /**
     * 查询文章列表
     * @param articleDTO
     * @return
     */
    ServiceResult<List<ArticleDTO>> listByPageNew(ArticleDTO articleDTO,Integer pageNum,Integer pageSize);

    /**
     * 我收藏的文章列表
     * @param collectionDTO
     * @param pageNum
     * @param pageSize
     * @return
     */
    ServiceResult<List<ArticleDTO>> listColletionArticleFormeByPage(CollectionDTO collectionDTO, Integer pageNum, Integer pageSize);

}
