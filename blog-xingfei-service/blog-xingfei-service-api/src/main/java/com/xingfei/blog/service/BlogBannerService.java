package com.xingfei.blog.service;

import com.xingfei.blog.dto.BannerDTO;
import com.xingfei.blog.result.ServiceResult;

import java.util.List;

/**
 * @Desc 轮播图dubbo服务
 * ClassName com.xingfei.blog.service
 * @Author xingfei
 * @Date 2017/7/9 20:54
 */
public interface BlogBannerService {

    /**
     * 获取轮播图列表
     *
     * @param bannerDTO
     * @return
     */
    ServiceResult<List<BannerDTO>> listBanners(BannerDTO bannerDTO);

    /**
     * 获取轮播图列表
     *
     * @param id
     * @return
     */
    ServiceResult<BannerDTO> getBannerById(String id);
}
