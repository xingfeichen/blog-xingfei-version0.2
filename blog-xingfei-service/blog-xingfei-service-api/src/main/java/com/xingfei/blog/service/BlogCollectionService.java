package com.xingfei.blog.service;

import com.xingfei.blog.dto.CollectionDTO;
import com.xingfei.blog.result.ServiceResult;

import java.util.List;

/**
 * 收藏dubbo服务
 *
 * @author chenxingfei
 * @create 2017-07-02 15:33
 **/
public interface BlogCollectionService {

    /**
     * 增加收藏
     * @param collectionDTO
     * @return
     */
    ServiceResult<CollectionDTO> saveCollection(CollectionDTO collectionDTO);
    /**
     * 根据收藏id查询收藏
     * @param collectionId
     * @return
     */
    ServiceResult<CollectionDTO> getCollectionById(Long collectionId);
    /**
     * 根据收藏id删除
     * @param collectionId
     * @return
     */
    ServiceResult<Boolean> deleteCollectionById(Long collectionId);
    /**
     * 获取我收藏的文章列表（可根据条件查询满足其他条件的文章）
     * @param collectionDTO 查询条件封装
     * @return
     */
    ServiceResult<List<CollectionDTO>> listCollectionByCondition(CollectionDTO collectionDTO);
    /**
     * @param collectionDTO 分页获取我收藏的文章列表
     * @param offset
     * @param size
     * @return
     */
    ServiceResult<List<CollectionDTO>> listCollectionByConditionAndPage(CollectionDTO collectionDTO,Integer offset,Integer size);
}
