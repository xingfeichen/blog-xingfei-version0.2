package com.xingfei.blog.admin.service;

import com.xingfei.blog.AdminDubboProviderApplication;
import com.xingfei.blog.dto.AdminUserDTO;
import com.xingfei.blog.result.ServiceResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by chenxingfei on 2017/6/3.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AdminDubboProviderApplication.class)
public class BlogAdminUserServiceImplTest {

    @Autowired
    private BlogAdminUserService blogAdminUserService;

    @Test
    public void testSaveAdminUser(){
        AdminUserDTO adminUserDto = new AdminUserDTO();
        adminUserDto.setUsername("admin");
        adminUserDto.setPassword("123456");
        ServiceResult<AdminUserDTO> serviceResult = blogAdminUserService.saveAdminUser(adminUserDto);
        System.out.println(serviceResult.getCode());
        System.out.println(serviceResult.getResult());
    }
}
