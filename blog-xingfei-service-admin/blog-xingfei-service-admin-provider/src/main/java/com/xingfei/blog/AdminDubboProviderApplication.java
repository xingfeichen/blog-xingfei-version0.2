package com.xingfei.blog;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.concurrent.CountDownLatch;

/**
 * Created by chenxingfei on 2017/6/2.
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.xingfei.blog.admin")
@EnableCaching
@ImportResource("classpath:dubbo-provide.xml")
@MapperScan(basePackages = {"com.xingfei.blog.baseMapper", "com.xingfei.blog.admin.mapper"})
@EnableTransactionManagement
@SpringBootApplication
public class AdminDubboProviderApplication {

    @Bean
    public CountDownLatch closeLatch() {
        return new CountDownLatch(1);
    }

    /**
     * 日志
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(AdminDubboProviderApplication.class);

    /**
     * dubbo服务启动主线程
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
//        SpringApplication.run(AdminDubboProviderApplication.class, args);
        ApplicationContext ctx = new SpringApplicationBuilder()
                .sources(AdminDubboProviderApplication.class)
                .web(false)
                .run(args);
        //让线程保持阻塞
        CountDownLatch closeLatch = ctx.getBean(CountDownLatch.class);
        LOGGER.info("====================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>ADMIN-DUBBO服务启动成功<<<<<<<<<<<<<<<<<<<<<<<<<<<<=================");
        closeLatch.await();
    }
}
