package com.xingfei.blog.admin.service.impl;

import com.xingfei.blog.admin.mapper.AdminUserMapper;
import com.xingfei.blog.constants.ErrorConstants;
import com.xingfei.blog.dto.AdminUserDTO;
import com.xingfei.blog.model.AdminUserDO;
import com.xingfei.blog.result.ResultConstructor;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.admin.service.BlogAdminUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by chenxingfei on 2017/6/3.
 */
@Service("blogAdminUserService")
public class BlogAdminUserServiceImpl implements BlogAdminUserService {

    @Autowired
    private AdminUserMapper adminUserMapper;
    @Override
    public ServiceResult<AdminUserDTO> saveAdminUser(AdminUserDTO adminUserDTO) {
        if(adminUserDTO == null){
            return  null;
        }
        AdminUserDO adminUserDO = new AdminUserDO();
        BeanUtils.copyProperties(adminUserDTO,adminUserDO);
        int selective = adminUserMapper.insertSelective(adminUserDO);
        if(selective>0){
            return ResultConstructor.buildSuccessResult(adminUserDTO, ErrorConstants.RES_SUCCESS, "操作失败");
        }
        return null;
    }


}
