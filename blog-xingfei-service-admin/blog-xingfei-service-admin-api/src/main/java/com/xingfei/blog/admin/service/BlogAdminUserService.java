package com.xingfei.blog.admin.service;

import com.xingfei.blog.dto.AdminUserDTO;
import com.xingfei.blog.result.ServiceResult;

/**
 * Created by chenxingfei on 2017/6/2.
 */
public interface BlogAdminUserService{

    /**
     * 保存后台用户
     * @param adminUserDTO
     * @return
     */
    ServiceResult<AdminUserDTO> saveAdminUser(AdminUserDTO adminUserDTO);

}
