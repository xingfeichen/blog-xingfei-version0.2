package com.xingfei.blog.baseMapper;

import com.xingfei.blog.model.BannerDO;
import tk.mybatis.mapper.common.Mapper;

/**
 * @Desc TODO
 * ClassName com.xingfei.blog.baseMapper
 * @Author xingfei
 * @Date 2017/7/9 20:53
 * Created by xingfei on 2017/7/9.
 */
public interface BannerMapper extends Mapper<BannerDO> {
}
