package com.xingfei.blog.baseMapper;

import com.xingfei.blog.model.UserCommentDO;
import tk.mybatis.mapper.common.Mapper;

/**
 * 评论mapper
 * Created by chenxingfei on 2017/5/21.
 */
public interface UserCommentMapper extends Mapper<UserCommentDO> {
}
