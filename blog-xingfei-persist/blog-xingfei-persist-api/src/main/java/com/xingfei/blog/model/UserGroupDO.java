package com.xingfei.blog.model;

import java.util.Date;

public class UserGroupDO {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_user_group.id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_user_group.group_power
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String groupPower;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_user_group.create_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_user_group.modify_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Date modifyTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_user_group.id
     *
     * @return the value of blog_user_group.id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_user_group.id
     *
     * @param id the value for blog_user_group.id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_user_group.group_power
     *
     * @return the value of blog_user_group.group_power
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getGroupPower() {
        return groupPower;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_user_group.group_power
     *
     * @param groupPower the value for blog_user_group.group_power
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setGroupPower(String groupPower) {
        this.groupPower = groupPower == null ? null : groupPower.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_user_group.create_time
     *
     * @return the value of blog_user_group.create_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_user_group.create_time
     *
     * @param createTime the value for blog_user_group.create_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_user_group.modify_time
     *
     * @return the value of blog_user_group.modify_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_user_group.modify_time
     *
     * @param modifyTime the value for blog_user_group.modify_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}