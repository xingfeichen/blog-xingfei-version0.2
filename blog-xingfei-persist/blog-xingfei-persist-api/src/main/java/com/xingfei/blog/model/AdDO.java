package com.xingfei.blog.model;

import java.util.Date;

public class AdDO {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.position_id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Long positionId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.media_type
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Byte mediaType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.ad_name
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String adName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.ad_link
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String adLink;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.start_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Date startTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.end_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Date endTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.link_man
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String linkMan;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.link_email
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String linkEmail;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.link_phone
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String linkPhone;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.click_count
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Integer clickCount;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.enabled
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Byte enabled;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.create_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.modify_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Date modifyTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_ad.ad_code
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String adCode;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.id
     *
     * @return the value of blog_ad.id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.id
     *
     * @param id the value for blog_ad.id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.position_id
     *
     * @return the value of blog_ad.position_id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Long getPositionId() {
        return positionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.position_id
     *
     * @param positionId the value for blog_ad.position_id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.media_type
     *
     * @return the value of blog_ad.media_type
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Byte getMediaType() {
        return mediaType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.media_type
     *
     * @param mediaType the value for blog_ad.media_type
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setMediaType(Byte mediaType) {
        this.mediaType = mediaType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.ad_name
     *
     * @return the value of blog_ad.ad_name
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getAdName() {
        return adName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.ad_name
     *
     * @param adName the value for blog_ad.ad_name
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setAdName(String adName) {
        this.adName = adName == null ? null : adName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.ad_link
     *
     * @return the value of blog_ad.ad_link
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getAdLink() {
        return adLink;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.ad_link
     *
     * @param adLink the value for blog_ad.ad_link
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setAdLink(String adLink) {
        this.adLink = adLink == null ? null : adLink.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.start_time
     *
     * @return the value of blog_ad.start_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.start_time
     *
     * @param startTime the value for blog_ad.start_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.end_time
     *
     * @return the value of blog_ad.end_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.end_time
     *
     * @param endTime the value for blog_ad.end_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.link_man
     *
     * @return the value of blog_ad.link_man
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getLinkMan() {
        return linkMan;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.link_man
     *
     * @param linkMan the value for blog_ad.link_man
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setLinkMan(String linkMan) {
        this.linkMan = linkMan == null ? null : linkMan.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.link_email
     *
     * @return the value of blog_ad.link_email
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getLinkEmail() {
        return linkEmail;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.link_email
     *
     * @param linkEmail the value for blog_ad.link_email
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setLinkEmail(String linkEmail) {
        this.linkEmail = linkEmail == null ? null : linkEmail.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.link_phone
     *
     * @return the value of blog_ad.link_phone
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getLinkPhone() {
        return linkPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.link_phone
     *
     * @param linkPhone the value for blog_ad.link_phone
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setLinkPhone(String linkPhone) {
        this.linkPhone = linkPhone == null ? null : linkPhone.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.click_count
     *
     * @return the value of blog_ad.click_count
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Integer getClickCount() {
        return clickCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.click_count
     *
     * @param clickCount the value for blog_ad.click_count
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setClickCount(Integer clickCount) {
        this.clickCount = clickCount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.enabled
     *
     * @return the value of blog_ad.enabled
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Byte getEnabled() {
        return enabled;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.enabled
     *
     * @param enabled the value for blog_ad.enabled
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setEnabled(Byte enabled) {
        this.enabled = enabled;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.create_time
     *
     * @return the value of blog_ad.create_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.create_time
     *
     * @param createTime the value for blog_ad.create_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.modify_time
     *
     * @return the value of blog_ad.modify_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.modify_time
     *
     * @param modifyTime the value for blog_ad.modify_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_ad.ad_code
     *
     * @return the value of blog_ad.ad_code
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getAdCode() {
        return adCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_ad.ad_code
     *
     * @param adCode the value for blog_ad.ad_code
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setAdCode(String adCode) {
        this.adCode = adCode == null ? null : adCode.trim();
    }
}