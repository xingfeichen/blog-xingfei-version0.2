package com.xingfei.blog.baseMapper;

import com.xingfei.blog.model.ArticleContentDO;
import tk.mybatis.mapper.common.Mapper;

/**
 * Created by xingfei on 2017/5/17.
 */
public interface ArticleContentMapper extends Mapper<ArticleContentDO> {
}
