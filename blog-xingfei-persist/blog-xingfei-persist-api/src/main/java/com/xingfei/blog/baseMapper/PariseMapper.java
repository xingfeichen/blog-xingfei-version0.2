package com.xingfei.blog.baseMapper;

import com.xingfei.blog.model.PariseDO;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

/**
 * 赞功能mapper
 * Created by chenxingfei on 2017/6/15.
 */
public interface PariseMapper extends Mapper<PariseDO> {

    @Select({"DELETE  FROM blog_parise where type=#{type} and user_id=#{userId} and relative_id=#{relativeId}"})
    Long deletepariseByCon(PariseDO pariseDO);
}
