package com.xingfei.blog.baseMapper;

import com.xingfei.blog.model.CollectionDO;
import tk.mybatis.mapper.common.Mapper;

/**
 * 收藏mapper
 *
 * @author chenxingfei
 * @create 2017-07-02 15:40
 **/
public interface CollectionMapper extends Mapper<CollectionDO> {
}
