package com.xingfei.blog.mapper;

import com.xingfei.blog.model.PhotoAlbumDO;
import com.xingfei.blog.model.PhotoAlbumDOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PhotoAlbumDOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_photo_album
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    int countByExample(PhotoAlbumDOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_photo_album
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    int deleteByExample(PhotoAlbumDOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_photo_album
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_photo_album
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    int insert(PhotoAlbumDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_photo_album
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    int insertSelective(PhotoAlbumDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_photo_album
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    List<PhotoAlbumDO> selectByExample(PhotoAlbumDOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_photo_album
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    PhotoAlbumDO selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_photo_album
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    int updateByExampleSelective(@Param("record") PhotoAlbumDO record, @Param("example") PhotoAlbumDOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_photo_album
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    int updateByExample(@Param("record") PhotoAlbumDO record, @Param("example") PhotoAlbumDOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_photo_album
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    int updateByPrimaryKeySelective(PhotoAlbumDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_photo_album
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    int updateByPrimaryKey(PhotoAlbumDO record);
}