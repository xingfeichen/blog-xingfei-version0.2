package com.xingfei.blog.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

@Table(name = "blog_article")
public class ArticleDO implements Serializable {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;
    /**
     * 文章发布用户id
     */
    private Long userId;
    /**
     * 文章名称
     */
    private String articleName;
    /**
     * 文章发布时间
     */
    private Date articleTime;
    /**
     * 文章发布者ip
     */
    private String articleIp;
    /**
     * 文章点击数
     */
    private Integer articleClick;
    /**
     * 文章目录id
     */
    private Long categoryArticleId;
    /**
     * 栏目ID
     */
    private Long typeId;
    /**
     * 是否置顶:0为否，1为是
     */
    private Byte isTop;
    /**
     * 是否博主推荐:0为否，1为是
     */
    private Byte isSupported;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date modifyTime;
    /**
     * 文章的模式:0为私有，1为公开，2为仅好友查看'
     */
    private Integer articleType;

    /**
     * 文章图片
     */
    private String imgUrl;
    /**
     * 文章关键字
     */
    private String keyWord;

    /**
     * 文章描述信息
     */
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }



    /**
     * 文章的内容
     */
    @Transient
    private ArticleContentDO articleContentDO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public Date getArticleTime() {
        return articleTime;
    }

    public void setArticleTime(Date articleTime) {
        this.articleTime = articleTime;
    }

    public String getArticleIp() {
        return articleIp;
    }

    public void setArticleIp(String articleIp) {
        this.articleIp = articleIp;
    }

    public Integer getArticleClick() {
        return articleClick;
    }

    public void setArticleClick(Integer articleClick) {
        this.articleClick = articleClick;
    }

    public Long getCategoryArticleId() {
        return categoryArticleId;
    }

    public void setCategoryArticleId(Long categoryArticleId) {
        this.categoryArticleId = categoryArticleId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Byte getIsTop() {
        return isTop;
    }

    public void setIsTop(Byte isTop) {
        this.isTop = isTop;
    }

    public Byte getIsSupported() {
        return isSupported;
    }

    public void setIsSupported(Byte isSupported) {
        this.isSupported = isSupported;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getArticleType() {
        return articleType;
    }

    public void setArticleType(Integer articleType) {
        this.articleType = articleType;
    }

    public ArticleContentDO getArticleContentDO() {
        return articleContentDO;
    }

    public void setArticleContentDO(ArticleContentDO articleContentDO) {
        this.articleContentDO = articleContentDO;
    }
}