package com.xingfei.blog.baseMapper;

import com.xingfei.blog.model.SecretMessageDO;
import tk.mybatis.mapper.common.Mapper;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: chenxingfei
 * @time: 2017/4/4  9:49
 * To change this template use File | Settings | File Templates.
 */
public interface SecretMessageMapper extends Mapper<SecretMessageDO> {}
