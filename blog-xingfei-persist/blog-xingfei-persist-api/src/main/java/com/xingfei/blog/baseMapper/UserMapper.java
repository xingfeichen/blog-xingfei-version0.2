package com.xingfei.blog.baseMapper;

import com.xingfei.blog.model.UserDO;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

/**
 * Created by hang on 2017/2/26.
 * Mapper共有方法参考
 * http://git.oschina.net/free/Mapper/blob/master/wiki/mapper3/5.Mappers.md
 */
public interface UserMapper extends Mapper<UserDO> {

    /**
     * 根据用户id获取用户信息
     * @param userId
     * @return
     */
    public UserDO getUserDoAndUserInfoByUserId(@Param("userId")Long userId);

}
