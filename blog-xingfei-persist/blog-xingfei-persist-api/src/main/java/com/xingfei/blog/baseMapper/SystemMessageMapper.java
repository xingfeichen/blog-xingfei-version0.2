package com.xingfei.blog.baseMapper;

import com.xingfei.blog.model.SystemMessageDO;
import tk.mybatis.mapper.common.Mapper;

/**
 * Created with IntelliJ IDEA.
 * 系统消息通用mapper
 * @author: chenxingfei
 * @time: 2017/4/4  9:49
 * To change this template use File | Settings | File Templates.
 */
public interface SystemMessageMapper extends Mapper<SystemMessageDO> {}
