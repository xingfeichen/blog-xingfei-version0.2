package com.xingfei.blog.baseMapper;

import com.xingfei.blog.model.UserSystemMsgDO;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * http://blog.csdn.net/luanlouis/article/details/35780175
 * Created by yhang on 2017/5/8.
 */
public interface UserSystemMsgMapper extends Mapper<UserSystemMsgDO> {

    @Select("SELECT use_msg.id,use_msg.receive_id,use_msg.sys_msg_id,system_content,use_msg.is_read ,use_msg.read_time FROM blog_user_system_msg use_msg INNER JOIN blog_system_message sys_msg ON use_msg.sys_msg_id != sys_msg.id AND use_msg.receive_id=#{userId}")
    @Results(value =
            {
                    @Result(column = "receive_id", property = "receiveId"),
                    @Result(column = "sys_msg_id", property = "sysMsgId"),
                    @Result(column = "system_content", property = "systemContent"),
                    @Result(column = "is_read", property = "isRead"),
                    @Result(column = "read_time", property = "readTime")
            })
    List<UserSystemMsgDO> listUnreadSysMsg(Long userId);
}
