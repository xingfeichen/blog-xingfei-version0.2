package com.xingfei.blog.model;

public class ArticleCategoryDO {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_article_category.id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_article_category.parent_id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Long parentId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_article_category.dn
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String dn;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_article_category.name
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_article_category.fin_type
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String finType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_article_category.is_parent
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String isParent;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_article_category.pay_type
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String payType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_article_category.pos
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private Integer pos;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_article_category.status
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String status;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_article_category.type
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String type;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_article_category.create_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column blog_article_category.modify_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    private String modifyTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_article_category.id
     *
     * @return the value of blog_article_category.id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_article_category.id
     *
     * @param id the value for blog_article_category.id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_article_category.parent_id
     *
     * @return the value of blog_article_category.parent_id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_article_category.parent_id
     *
     * @param parentId the value for blog_article_category.parent_id
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_article_category.dn
     *
     * @return the value of blog_article_category.dn
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getDn() {
        return dn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_article_category.dn
     *
     * @param dn the value for blog_article_category.dn
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setDn(String dn) {
        this.dn = dn == null ? null : dn.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_article_category.name
     *
     * @return the value of blog_article_category.name
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_article_category.name
     *
     * @param name the value for blog_article_category.name
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_article_category.fin_type
     *
     * @return the value of blog_article_category.fin_type
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getFinType() {
        return finType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_article_category.fin_type
     *
     * @param finType the value for blog_article_category.fin_type
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setFinType(String finType) {
        this.finType = finType == null ? null : finType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_article_category.is_parent
     *
     * @return the value of blog_article_category.is_parent
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getIsParent() {
        return isParent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_article_category.is_parent
     *
     * @param isParent the value for blog_article_category.is_parent
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setIsParent(String isParent) {
        this.isParent = isParent == null ? null : isParent.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_article_category.pay_type
     *
     * @return the value of blog_article_category.pay_type
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getPayType() {
        return payType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_article_category.pay_type
     *
     * @param payType the value for blog_article_category.pay_type
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setPayType(String payType) {
        this.payType = payType == null ? null : payType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_article_category.pos
     *
     * @return the value of blog_article_category.pos
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public Integer getPos() {
        return pos;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_article_category.pos
     *
     * @param pos the value for blog_article_category.pos
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setPos(Integer pos) {
        this.pos = pos;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_article_category.status
     *
     * @return the value of blog_article_category.status
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_article_category.status
     *
     * @param status the value for blog_article_category.status
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_article_category.type
     *
     * @return the value of blog_article_category.type
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getType() {
        return type;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_article_category.type
     *
     * @param type the value for blog_article_category.type
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_article_category.create_time
     *
     * @return the value of blog_article_category.create_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_article_category.create_time
     *
     * @param createTime the value for blog_article_category.create_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column blog_article_category.modify_time
     *
     * @return the value of blog_article_category.modify_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public String getModifyTime() {
        return modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column blog_article_category.modify_time
     *
     * @param modifyTime the value for blog_article_category.modify_time
     *
     * @mbggenerated Tue Jan 17 16:27:13 CST 2017
     */
    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime == null ? null : modifyTime.trim();
    }
}