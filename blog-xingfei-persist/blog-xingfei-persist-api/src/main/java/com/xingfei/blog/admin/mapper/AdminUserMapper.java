package com.xingfei.blog.admin.mapper;

import com.xingfei.blog.model.AdminUserDO;
import tk.mybatis.mapper.common.Mapper;

/**
 * Created by chenxingfei on 2017/6/2.
 */
public interface AdminUserMapper extends Mapper<AdminUserDO> {
}
