package com.xingfei.blog.baseMapper;

import com.xingfei.blog.model.UserInfoDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: chenxingfei
 * @time: 2017/3/19  9:50
 * To change this template use File | Settings | File Templates.
 */
public interface UserInfoMapper extends Mapper<UserInfoDO> {

    /**
     * 获取推荐作者列表
     * @return
     */
    List<UserInfoDO> listRecommendAuthor();

    /**
     * 根据用户id获取用户
     * @param userId
     * @return
     */
    @Select({"select * from blog_user_info where user_id=#{userId}"})
    UserInfoDO getUserInfoDoByUserId(@Param("userId") Long userId);
}
