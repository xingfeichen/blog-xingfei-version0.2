package com.xingfei.blog.mapper;

import com.xingfei.blog.model.UserDO;
import com.xingfei.blog.model.UserDOExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;
/**
 * Created with IntelliJ IDEA.
 * 用户扩展mapper
 * @author: chenxingfei
 * @time: 2017/1/19  11:50
 * To change this template use File | Settings | File Templates.
 */
public interface UserDOExtMapper {

    /**
     * 根据邮箱获取用户信息
     * @param email
     * @return
     */
    @Select("select * from blog_user where email=#{email}")
    @ResultMap("BaseResultMap")
   public UserDO getUserByEmail(@Param("email") String email);

    /**
     * 根据手机号获取用户信息
     * @param phone
     * @return
     */
    @Select("select * from blog_user where phone=#{phone}")
    @ResultMap("BaseResultMap")
    public UserDO getUserByPhone(@Param("phone")String phone);

    /**
     * 添加用户数据，返回主键
     * @param userDO
     * @return
     */
    public Long saveUserDOForBackId(UserDO userDO);

    /**
     * 根据用户id获取用户信息
     * @param userId
     * @return
     */
    public UserDO getUserDoAndUserInfoByUserId(@Param("userId")Long userId);

}