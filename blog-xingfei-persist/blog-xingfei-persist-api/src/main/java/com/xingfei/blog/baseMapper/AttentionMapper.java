package com.xingfei.blog.baseMapper;

import com.xingfei.blog.model.UserAttentionDO;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: chenxingfei
 * @time: 2017/4/2  23:13
 * To change this template use File | Settings | File Templates.
 */
public interface AttentionMapper extends Mapper<UserAttentionDO> {

    /**
     * 获取关注数和被关注数
     * @param userId 用户id
     * @return
     */
    public Map<String,Integer> getAttentionsByUserId(@Param("userId") Long userId);
}
