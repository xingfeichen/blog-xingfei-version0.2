package com.xingfei.blog.baseMapper;

import com.xingfei.blog.dto.CollectionDTO;
import com.xingfei.blog.model.ArticleDO;
import com.xingfei.blog.model.CollectionDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: chenxingfei
 * @time: 2017/3/27  10:04
 * To change this template use File | Settings | File Templates.
 */
public interface ArticleMapper extends Mapper<ArticleDO> {

    /**
     * 查询文章列表
     * @param record
     * @param page
     * @param pageSize
     * @return
     */
    List<ArticleDO> listArticles(@Param("record")ArticleDO record , @Param("page") Integer page,  @Param("pageSize")Integer pageSize);
    /**
     * 查询首页文章列表
     * @param record
     * @param page
     * @param pageSize
     * @return
     */
    List<ArticleDO> listArticlesoForIndex(@Param("record")ArticleDO record , @Param("page") Integer page,  @Param("pageSize")Integer pageSize);

    /**
     * 根据文章id获取文章详情
     * @param id
     * @return
     */
    ArticleDO getArticleById(@Param("id")Long id);

    /**
     * 根据文章id获取文章发布者
     * @param articleId
     * @return
     */
    @Select({"select user_id from blog_article where id=#{articleId} "})
    Long getPublisherIdByArticleId(@Param("articleId")Long articleId);

    /**
     * 我收藏的文章列表
     * @param collectionDTO
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<ArticleDO> listColletionArticleFormeByPage(@Param("collectionDO")CollectionDO collectionDO, @Param("page") Integer page, @Param("pageSize")Integer pageSize);
}
