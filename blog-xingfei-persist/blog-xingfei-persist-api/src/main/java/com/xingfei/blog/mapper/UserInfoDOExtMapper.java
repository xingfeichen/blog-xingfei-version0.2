package com.xingfei.blog.mapper;

import java.util.List;
import com.xingfei.blog.model.UserInfoDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: chenxingfei
 * @time: 2017/1/19  11:50
 * To change this template use File | Settings | File Templates.
 */
public interface UserInfoDOExtMapper {

    @Select("select * from blog_user_info where id=#{userInfoDoId}")
    @ResultMap("BaseResultMap")
    UserInfoDO getUserInfoDoById(@Param("userInfoDoId") Long userInfoDoId);

    /**
     * 插入用户信息并返回id
     *
     * @param userInfoDO
     * @return
     */
    public Long saveUserInfoBackId(UserInfoDO userInfoDO);

    /**
     * 按条件获取用户信息列表
     *
     * @param userInfoDO
     * @return
     */
    public List<UserInfoDO> listUserInfoByDO(@Param("record") UserInfoDO userInfoDO);

    /**
     * 按条件分页获取用户信息列表
     *
     * @param userInfoDO
     * @return
     */
    List<UserInfoDO> listUserInfoByDOByPage(@Param("start") int start, @Param("limit") int limit, @Param("record") UserInfoDO userInfoDO);
}
