package com.xingfei.blog.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * http://www.cnblogs.com/youxin/archive/2013/04/17/3025381.html 站内信参考
 * 用户系统消息读取状态DO
 * Created by yhang on 2017/5/8.
 */
@Table(name = "blog_user_system_msg")
public class UserSystemMsgDO implements Serializable {
    
    private static final long serialVersionUID = -8448419722133598184L;

    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;
    @Column(name = "receive_id")
    private Long receiveId;
    @Column(name = "sys_msg_id")
    private Long sysMsgId;
    @JsonIgnore
    @Column(name = "system_content")
    private String systemContent;
    @Column(name = "is_read")
    private String isRead;
    @Column(name = "read_time")
    private Date readTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReceiveId() {
        return receiveId;
    }

    public void setReceiveId(Long receiveId) {
        this.receiveId = receiveId;
    }

    public Long getSysMsgId() {
        return sysMsgId;
    }

    public void setSysMsgId(Long sysMsgId) {
        this.sysMsgId = sysMsgId;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public Date getReadTime() {
        return readTime;
    }

    public void setReadTime(Date readTime) {
        this.readTime = readTime;
    }

    public String getSystemContent() {
        return systemContent;
    }

    public void setSystemContent(String systemContent) {
        this.systemContent = systemContent;
    }
}


