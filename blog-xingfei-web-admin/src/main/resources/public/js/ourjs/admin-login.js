/**
 * 后台用户登录
 * Created by chenxingfei on 2017/6/4.
 */
Admin_Login = {

    /**
     * 返回操作url
     * @param model
     * @param method
     * @returns {string}
     * @constructor
     */
    url: function (model, method) {
        return "/" + model + "/" + method;
    },

    /**
     * 登录操作
     */
    login: function () {
        var userName = $("input[name='userName']").val();
        if (CommonValidate.isNull(userName)) {
            LayerUtils.checkForTips("用户名不能为空！", $("#userName"));
            return;
        }
        var password = $("input[name='password']").val();
        if (CommonValidate.isNull(password)) {
            LayerUtils.checkForTips("密码不能为空！", $("#password"));
            return
        }
        if(CommonValidate.isNull(userName)|| CommonValidate.isNull(password)){
            layer.msg('登录信息不合法');
            return;
        }
        $.post(Admin_Login.url('user', 'login'), {'userName': userName, 'password': password}, function (result) {
            if (result.errorCode != '200') {
                layer.alert(result.errorMsg, {icon: 5});
            } else {
                layer.msg('登录成功，正在跳转！');
                window.location.href = "/index";
            }
        })
    },
    /**
     * DEC加密
     */
    encryptByDES: function (message, key) {
        var keyHex = CryptoJS.enc.Utf8.parse(key);
        var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypted.toString();
    }
}