package com.xingfei.blog.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 后台首页控制器
 * Created by chenxingfei on 2017/6/4.
 */
@Controller
@RequestMapping("/index")
public class AdminIndexController {

    @RequestMapping("")
    public ModelAndView toIndex(){
        return new ModelAndView("/index");
    }
}
