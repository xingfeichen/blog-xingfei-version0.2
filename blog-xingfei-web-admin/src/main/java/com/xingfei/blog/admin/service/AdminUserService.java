package com.xingfei.blog.admin.service;

import com.xingfei.blog.constants.ErrorConstants;
import com.xingfei.blog.dto.AdminUserDTO;
import com.xingfei.blog.result.ServiceResult;
import com.xingfei.blog.vo.AdminUserVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by chenxingfei on 2017/6/2.
 */

@Service
public class AdminUserService {

    @Autowired
    private BlogAdminUserService blogAdminUserService;

    /**
     * save admin user
     * @param adminUserVO
     * @return
     */
    public Boolean saveAdminUser(AdminUserVO adminUserVO) {
        AdminUserDTO adminUserDto = new AdminUserDTO();
        BeanUtils.copyProperties(adminUserVO,adminUserDto);
        ServiceResult<AdminUserDTO> serviceResult = blogAdminUserService.saveAdminUser(adminUserDto);
        if (serviceResult != null && serviceResult.getCode() == ErrorConstants.RES_SUCCESS) {
            return true;
        }
        return false;
    }

    /**
     * 用户和登录
     * @param userName
     * @param password
     * @return
     */
    public AdminUserVO getUserForLogin(String userName, String password) {
        return null;
    }
}
