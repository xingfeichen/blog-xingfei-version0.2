package com.xingfei.blog.admin.controller;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.xingfei.blog.admin.common.JsonResult;
import com.xingfei.blog.admin.service.AdminUserService;
import com.xingfei.blog.vo.AdminUserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.constraints.NotNull;

/**
 * 后台用户登录
 * Created by chenxingfei on 2017/6/4.
 */
@Controller
public class LoginController {

    @Autowired
    private AdminUserService adminUserService;
    /**
     * 跳转登录页面
     * @return
     */
    @RequestMapping("/toLogin")
    public ModelAndView toLogin(){
        return new ModelAndView("/login");
    }

    /**
     * 用户登录
     * @param userName
     * @param password
     * @return
     */
    @PostMapping("/user/login")
    @ResponseBody
    public JsonResult login(@NotNull  String userName,@NotNull String password){
        AdminUserVO adminUserVO = adminUserService.getUserForLogin(userName,password);
        return new JsonResult("200","200","登录成功");
    }
}
