package com.xingfei.blog.admin.common;

import com.fasterxml.jackson.annotation.JsonView;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * 返回json结果视图
 * @author: chenxingfei
 * @time: 2017/1/23  14:49
 * To change this template use File | Settings | File Templates.
 */
public final class JsonResult<T> implements Serializable {

    public static interface ErrorView {}
    public static interface NormalView extends ErrorView {}


    @JsonView(ErrorView.class)
    private boolean isEncrypt; // 是否加密

    @JsonView(NormalView.class)
    private T data; // 数据

    @JsonView(NormalView.class)
    private T extendData; // 扩展数据

    @JsonView(ErrorView.class)
    private String errorCode; // 返回给调用者的错误码（如：100001）

    @JsonView(ErrorView.class)
    private String errorMsg; // 返回给调用者的错误信息描述，操作成功该字段为空(如：服务器异常)

    public JsonResult() {
    }

    public static <T> JsonResult<T> newResult() {
        return new JsonResult<T>();
    }

    public JsonResult(String errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public JsonResult(T data, String errorCode, String errorMsg) {
        this.data = data;
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public JsonResult(T data, Boolean isEncrypt, String errorCode) {
        this.data = data;
        this.errorCode = errorCode;
        this.isEncrypt = isEncrypt;
    }

    public JsonResult(T data, Boolean isEncrypt, String errorCode, String errorMsg) {
        this.data = data;
        this.errorCode = errorCode;
        this.isEncrypt = isEncrypt;
        this.errorMsg = errorMsg;
    }

    public JsonResult(boolean isEncrypt, T data, T extendData, String errorCode, String errorMsg) {
        this.data = data;
        this.isEncrypt = isEncrypt;
        this.extendData = extendData;
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    /**
     * @return the isEncrypt
     */
    public boolean isEncrypt() {
        return isEncrypt;
    }

    /**
     * @param isEncrypt the isEncrypt to set
     */
    public void setEncrypt(boolean isEncrypt) {
        this.isEncrypt = isEncrypt;
    }

    /**
     * @return the data
     */
    public T getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(T data) {
        this.data = data;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * @return the errorMsg
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * @param errorMsg the errorMsg to set
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    /**
     * @return the extendData
     */
    public T getExtendData() {
        return extendData;
    }

    /**
     * @param extendData the extendData to set
     */
    public void setExtendData(T extendData) {
        this.extendData = extendData;
    }


}
