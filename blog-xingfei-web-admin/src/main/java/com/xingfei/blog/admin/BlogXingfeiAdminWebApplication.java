package com.xingfei.blog.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by chenxingfei on 2017/6/3.
 */
@SpringBootApplication
@EnableCaching
@ImportResource(locations = {"classpath:dubbo-comsumer.xml"})
@ComponentScan(basePackages = {"com.xingfei.blog.admin"})
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class BlogXingfeiAdminWebApplication {

    private static final String CONTEXT_PATH = "basePath";

    /**
     * 日志
     */
    public static final Logger LOGGER = LoggerFactory.getLogger(BlogXingfeiAdminWebApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(BlogXingfeiAdminWebApplication.class, args);
        LOGGER.info("====================>>>>>>>>>>>>>>>>>>>>>>>>>>>>>admin-web启动成功<<<<<<<<<<<<<<<<<<<<<<<<<<<<=================");
    }

    @Bean
    protected ServletContextListener listener() {
        return new ServletContextListener() {
            public void contextInitialized(ServletContextEvent sce) {
                // 获取web应用对象
                ServletContext application = sce.getServletContext();

                // 获取web应用路径
                String basePath = application.getContextPath();

                // 保存在web应用范围内
                application.setAttribute(CONTEXT_PATH, basePath);
            }

            public void contextDestroyed(ServletContextEvent sce) {
                //必须覆盖的方法
            }
        };
    }
}
