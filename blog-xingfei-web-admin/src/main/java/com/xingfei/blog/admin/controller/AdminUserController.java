package com.xingfei.blog.admin.controller;

import com.xingfei.blog.admin.service.AdminUserService;
import com.xingfei.blog.vo.AdminUserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by chenxingfei on 2017/6/2.
 */
@Controller
@RequestMapping("adminUser")
public class AdminUserController {

    @Autowired
    private AdminUserService adminUserService;

    /**
     * 新增用户
     * @param adminUserVO
     * @return
     */
    @RequestMapping("/saveAdminUser")
    public ModelAndView saveAdminUser(AdminUserVO adminUserVO){
        Boolean b = adminUserService.saveAdminUser(adminUserVO);
        return new ModelAndView("/userlist");
    }

}
