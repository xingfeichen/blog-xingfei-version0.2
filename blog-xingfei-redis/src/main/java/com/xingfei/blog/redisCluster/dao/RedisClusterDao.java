package com.xingfei.blog.redisCluster.dao;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 封装redis常用方法
 * Created by chenxingfei on 2017/6/4.
 */
public interface RedisClusterDao {

    /**
     * 想缓存中保存string类型数据
     * @param key
     * @param value
     * @param time
     * @param <T>
     * @return
     */
    <T> String set(String key, String value, Long time);

    /**
     * 根据key获取值（String 类型）
     * @param key
     * @param <T>
     * @return
     */
    <T> String get(String key);


    /**
     * 新增（根据key保存）
     * @param key
     * @param object
     * @param <T>
     * @return
     * @throws IOException
     */
    <T> String addByKey(String key, T object, Integer time) throws IOException;

    /**
     * 将对象转化为json存储返回key（key通过uuid生成）
     * @param object
     * @param <T>
     * @return
     * @throws IOException
     */
    <T> String add(T object, Long time) throws IOException;



    /**
    * 根据key获取值
     * @param key
     * @return
     * @throws IOException
     */
    Object getObject(String key) throws IOException;

    /**
     * 新增一个list集合，在redis中以单个对象存储，返回一个key值list集合，key通过uuid生成
     * @param list
     * @param <T>
     * @return
     * @throws IOException
     */
    <T> List<String> addList(List<T> list) throws IOException;

    /**
     * 新增一个map集合，在redis中以单个对象存储，返回一个key值list集合，key通过uuid生成
     * @param map
     * @param <T>
     * @return
     * @throws IOException
     */
    <T> Long addListKey(Map<String, T> map) throws IOException;

    /**
     * 根据key值删除
     * @param key
     * @return
     * @throws IOException
     */
    Long deleteByKey(String key) throws IOException;

    /**
     * 批量删除
     * @param strList
     * @return
     * @throws IOException
     */
    Long batchDelete(List<String> strList) throws IOException;

    /**
     * 给指定key设置时间(不同在于 EXPIREAT 命令接受的时间参数是 UNIX 时间戳(unix timestamp))
     * @param key
     * @param unixTime UNIX 时间戳
     * @param <T>
     * @return
     */
    <T> Long expireAt(String key, Long unixTime);

    /**
     * 判断是否存在
     * @param key
     * @param seconds
     * @param <T>
     * @return
     */
    <T> Long expire(String key, Integer seconds);
    /**
     * 给指定key设置时间
     * @param key
     * @param <T>
     * @return
     */
    <T> Boolean isExist(String key);

    /**
     * 根据key将值加1
     * @param key
     * @return
     */
    <T> Long incr(String key);
    /**
     * 根据key将值减1
     * @param key
     * @return
     */
    <T> Long decr(String key);
}
