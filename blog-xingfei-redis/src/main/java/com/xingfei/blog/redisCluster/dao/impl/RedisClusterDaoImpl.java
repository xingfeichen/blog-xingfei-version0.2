package com.xingfei.blog.redisCluster.dao.impl;

import com.xingfei.blog.redisCluster.dao.RedisClusterDao;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.JedisCluster;

import java.io.IOException;
import java.util.*;

/**
 * redisDao实现类
 * Created by chenxingfei on 2017/6/4.
 */
@Repository("redisClusterDao")
public class RedisClusterDaoImpl implements RedisClusterDao {

    @Autowired
    JedisCluster jedisCluster;

    @Override
    public <T> String set(String key, String value, Long time) {
        String result = jedisCluster.set(key, value);
        if (time > 0) {
            //设置有效期，否则默认为永久
            jedisCluster.expireAt(key, time);
        }
        return result;
    }

    @Override
    public <T> String get(String key) {
        return jedisCluster.get(key);
    }

    @Override
    public <T> String addByKey(String key, T object, Integer time) throws IOException {
        JSONObject jsonObject = JSONObject.fromObject(object);
        if (time > 0) {
            //设置有效期，否则默认为永久
            jedisCluster.expire(key, time);
        }
        return jedisCluster.set(key, jsonObject.toString());
    }

    @Override
    public <T> String add(T object, Long time) throws IOException {
        String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "");
        String object2JsonString = JSONObject.fromObject(object).toString();
        if (time > 0) {
            //设置有效期，否则默认为永久
            jedisCluster.expireAt(uuid, time);
        }
        jedisCluster.set(uuid, object2JsonString);
        return uuid;
    }

    @Override
    public Object getObject(String key) throws IOException {
        return jedisCluster.get(key);
    }

    @Override
    public <T> List<String> addList(List<T> list) throws IOException {
        List<String> sum = new ArrayList<>(70);
        String uuid = null;
        String str = null;
        for (int i = 0; i < list.size(); i++) {
            uuid = UUID.randomUUID().toString().trim().replaceAll("-", "");
            str = JSONObject.fromObject(list.get(i)).toString();
            jedisCluster.set(uuid, str);
            sum.set(i, uuid);
        }
        return sum;
    }

    @Override
    public <T> Long addListKey(Map<String, T> map) throws IOException {
        Long sum = (long) 0;
        String str = null;
        Iterator<Map.Entry<String, T>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, T> entry = (Map.Entry<String, T>) iterator.next();
            String key = entry.getKey();
            T object = entry.getValue();
            str = JSONObject.fromObject(object).toString();
            jedisCluster.set(key, str);
            sum = sum + 1;
        }

        return sum;
    }

    @Override
    public Long deleteByKey(String key) throws IOException {
        return jedisCluster.del(key);
    }

    @Override
    public Long batchDelete(List<String> strList) throws IOException {
        Long sum = (long) 0;
        for (int i = 0; i < strList.size(); i++) {
            sum += jedisCluster.del(strList.get(i));
        }

        return sum;
    }

    @Override
    public <T> Long expireAt(String key, Long seconds) {
        return jedisCluster.expireAt(key,seconds);
    }

    @Override
    public <T> Long expire(String key, Integer seconds) {
        // redis 集群均使用DB0，不支持选择数据库
        // jedisCluster.select();
        return jedisCluster.expire(key,seconds);
    }

    @Override
    public <T> Boolean isExist(String key) {
        return jedisCluster.exists(key);
    }

    @Override
    public <T> Long decr(String key) {
        return jedisCluster.decr(key);
    }

    @Override
    public Long incr(String key){
        return jedisCluster.incr(key);
    }
}
